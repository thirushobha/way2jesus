<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('instamojo');
		$this->load->helper('url');
		$this->load->model("user_model");
		$this->load->model("home_model");
		$this->load->model("page_model");
		$this->load->model("feed_model");
		$this->load->model("payment_model");
		$this->load->helper('date');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$this->template->set_layout("client/themes/best.php");
	}

	public function checkout($amount='',$planid='')
	{
		$userid = $this->user->info->ID;

		if($planid==""){
			redirect('payment/payment_problem','refresh');
		}
		$plans = $this->db->get_where('payment_plans',array('ID'=>$planid))->row();	
		 $this->template->loadContent("funds/checkout", array(
			"plans" => $plans,
			"userid" => $userid,
			
			)
		);
	}

	public function payment_problem($userid='',$planid='')
	{
		
		$userid = $this->user->info->ID;

		if($planid==""){
			redirect('funds/payment_problem','refresh');
		}
		$plans = $this->db->get_where('payment_plans',array('ID'=>$planid))->row();	
		 $this->template->loadContent("funds/payment_failure", array(
			"plans" => $plans,
			"userid" => $userid,
			
			)
		);
		
        
        
	}
	public function payment_success($userid='',$planid='')
	{
		$userid = $this->user->info->ID;

		if($planid==""){
			redirect('funds/payment_success','refresh');
		}
		$plans = $this->db->get_where('payment_plans',array('ID'=>$planid))->row();	
		$userinfo = $this->db->get_where('users',array('ID'=>$userid))->row();
		 $this->template->loadContent("funds/payment_success", array(
			"plans" => $plans,
			"userinfo" => $userinfo,
			
			)
		);
        
	}


	public function index($amount='',$planid='')
	{
		$userid = $this->user->info->ID;

		if($planid==""){
			redirect('payment/payment_problem','refresh');
		}
		$insert_payment_data = array(
			'user_id_payment'=>$userid,
			'planid_payment'=>$planid,
			'amount_payment'=>$amount,
		);

		$this->session->set_userdata($insert_payment_data);
		
		$pay = $this->instamojo->pay_request( 
						$amount = $amount , 
						$purpose = "Purchase of Church Subscription" , 
						$buyer_name = $this->user->info->username , 
						$email = $this->user->info->email , 
						$phone = '9291642620' ,
		     			$send_email = 'FALSE' , 
		     			$send_sms = 'FALSE' , 
		     			$repeated = 'FALSE'
		     		);

		$redirect_url = $pay['longurl'];
		redirect($redirect_url,'refresh') ;

	}

		public function status()
	{
		$payment_id = $this->input->get('payment_id');
		$payment_status = $this->input->get('payment_status');
		$payment_request_id = $this->input->get('payment_request_id');

		$status     = $this->instamojo->status($payment_request_id);

		if($status['status']=="Completed")
		{
			if($status['payments'][0]['status']=='Credit')
			{
				$user_id_payment = $this->session->userdata('user_id_payment');
				$planid_payment = $this->session->userdata('planid_payment');
				$amount_payment = $this->session->userdata('amount_payment');


				

				$plans = $this->db->get_where('payment_plans',array('ID'=>$planid_payment))->row();

				//payment log table(userid,amount,timestamp,email,processor,hash);
				$extend_years = $plans->days;
				$purchase_date =  date('Y-m-d');
				$expire_date = date('Y-m-d', strtotime('+'.$extend_years.' years'));
				$timestamp_to = strtotime($expire_date);



				$payment_log_insert = array(
					'userid'=>$user_id_payment,
					'amount'=>$amount_payment,
					'timestamp'=>time(),
					'email'=>$this->user->info->email,
					'payment_id'=>$payment_id,
					'payment_request_id'=>$payment_request_id,
					'payment_status'=>$payment_status
					
				);
				$this->db->insert('payment_logs',$payment_log_insert);
				$insert_id = $this->db->insert_id();


				//in users table update(premium_planid,premium_time)
				$user_table_update = array(
					'premium_time'=>$timestamp_to,
					'premium_planid'=>$planid_payment,
				);
				$user_where = array('ID'=>$user_id_payment);
				$this->db->update('users', $user_table_update,$user_where);


				$plan_sales = $plans->sales;
				$plan_sales++;
				$plans_table_data = array('sales'=>$plan_sales);
				$this->db->update('payment_plans',$plans_table_data,array('ID'=>$planid_payment));
				//update payment_plans table increment sales of plan



				$insert_payment_data = array(
			'user_id'=>$user_id_payment,
			'planid'=>$planid_payment,
			'amount'=>$amount_payment,
			'payment_id'=>$payment_id,
			'payment_request_id'=>$payment_request_id,
			'payment_status'=>$payment_status
		);
			
				if($insert_id>0)
				{
					redirect('payment/payment_success/'.$user_id_payment.'/'.$planid_payment,'refresh');

				}else{
					redirect('payment/payment_problem','refresh');
				}

			}else{
				redirect('payment/payment_problem','refresh');
			}

		}else{
			redirect('payment/payment_problem','refresh');

		}



		print_r($status);
	}



}

/* End of file example.php */
/* Location: ./application/controllers/example.php */