<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funds extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("admin_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$this->template->set_layout("client/themes/best.php");
	}

	public function plans()
	{
		$plans = $this->admin_model->get_payment_plans();
		  $this->template->loadContent("funds/plans", array(
			"plans" => $plans,
			
			)
		);
	}

	public function subscription()
	{
		$userinfo = $this->user->info;

		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}

		$plan = $this->admin_model->get_payment_plan($this->user->info->premium_planid)->row();
		  $this->template->loadContent("funds/subscription", array(
			"plan" => $plan,
			"userinfo"=>$userinfo,			
			)
		);
	}
}