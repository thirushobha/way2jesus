<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller 
{

	public function __construct() 
	{
		
		parent::__construct();
		$this->load->model('welcome_model');
		if ($this->user->loggedin) {
			redirect(base_url().'home');
		}
	
	}

	public function index()
	{
		

		$data['pastors'] = $this->welcome_model->get_pastors();
		$data['churches'] = $this->welcome_model->get_churches();
		$data['loggedin'] = $this->user->loggedin;
		
		
		$this->load->view('frontpages/home', $data, FALSE);
	}

	public function home()
	{
		$data= "";
		$this->load->view('frontpages/index', $data, FALSE);
	}


}