<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pastors extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		// If the user does not have premium. 
		// -1 means they have unlimited premium
		// if($this->settings->info->global_premium && 
		// 	($this->user->info->premium_time != -1 && 
		// 		$this->user->info->premium_time < time()) ) {
		// 	$this->session->set_flashdata("globalmsg", lang("success_29"));
		// 	redirect(site_url("funds/plans"));
		// }

		$this->template->set_layout("client/themes/best.php");
	}

	public function index() 
	{
		// $this->template->loadContent("pastors/index.php", array(
			
		// 	)
		// );

		 //pagination settings
        $config['base_url'] = site_url('pastors/index');
        $config['total_rows'] = $this->page_model->get_total_pastors_count();
        $config['per_page'] = "16";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"]/$config["per_page"];
        $config["num_links"] = floor($choice);

        // integrate bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_url'] = '0';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li >';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" class="active page-link">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);

        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        // get books list
        $data['images'] = $this->page_model->get_all_pastors_data_nodatatable($config["per_page"], $data['page'], NULL);


        $data['pagination'] = $this->pagination->create_links();

        // load view
        // $this->load->view('pages/view_pages',$data);


        $this->template->loadContent("pastors/view", array(
			"pagination" => $this->pagination->create_links(),
			"images" => $data['images'],
			"page" => $data['page'],
			
			)
		);

	}


		public function your_pastors($type=2) 
	{
		$type = intval($type);
		$this->load->library("datatables");

		$this->datatables->set_default_order("Users.ID", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				 0 => array(
				 	"pages.name" => 0
				 ),
				 1 => array(
				 	"pages.pageviews" => 0
				 ),
				 2 => array(
				 	"pages.members" => 0
				 ),
				 3 => array(
				 	"page_categories.name" => 0
				 ),

			)
		);

		
			
			$this->datatables->set_total_rows(
				$this->page_model
					->get_total_pastors_count()
			);
			$pages = $this->page_model->get_all_pastors_data($this->datatables);
		



		foreach($pages->result() as $r) {


				$options = '<a href="' . site_url("profile/" . $r->username) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'">View</a> ';
				if($r->friends !="")
				{
					$friends_count = count(unserialize($r->friends));

				}else{
					$friends_count = 0;
				}
			
			 
			$this->datatables->data[] = array(
				'<a href="'.site_url("profile/" . $r->username).'">' . $r->first_name.' '.$r->last_name . '</a>',
				$r->profile_views,
				$r->post_count,
				$friends_count ,
				$options
			);
		}
		echo json_encode($this->datatables->process());
	}

}