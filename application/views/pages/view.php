  <script type="text/javascript">
$(document).ready(function() {
  load_posts(<?php echo $page->ID ?>);
});

function load_posts_wrapper() 
{
  load_posts(<?php echo $page->ID ?>);
}

function load_posts(pageid) 
{
  $.ajax({
    url: global_base_url + 'feed/load_page_posts/' + pageid,
    type: 'GET',
    data: {

    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
          nextSelector : '.load_next'
      });
    }
  })
}
 </script>

<section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $page->profile_header ?>" alt="">
      <!-- <a href="#" title=""><i class="fa fa-camera"></i> Change Image</a> -->

       <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 

<a data-toggle="modal" data-target="#bannermodal" style="cursor: pointer;" class="btn btn-warning btn-xs mr-5"><span class="fa fa-pencil"></span></a> 

<a href="<?php echo site_url("pages/delete_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php echo lang("ctn_551") ?>')" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
<?php endif; ?>


    </section>


    <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" alt="" width="150px">
                      
                       <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
                       <a data-toggle="modal" data-target="#myModal" class="text-white" style="cursor: pointer;"><i class="fa fa-camera"></i></a>
                       <?php endif; ?>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        
                        <?php if($member == null) : ?>
   <li> <a href="<?php echo site_url("pages/join_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_554") ?></a>
   </li>
  <?php else : ?>
    <li>
    <a href="<?php echo site_url("pages/leave_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="fa fa-check"></span> <?php echo lang("ctn_34") ?></a> 
  </li>
  <?php endif; ?>
  <li><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="fa fa-flag"></span></button>
  </li>

                      </ul>
                      <!-- <ul class="flw-status">
                        <li>
                          <span>Members</span>
                          <b>34</b>
                        </li>
                        <li>
                          <span>Followers</span>
                          <b>155</b>
                        </li>
                      </ul> -->
                    </div><!--user_pro_status end-->
                     <ul class="social_links">

                        <li class="page-block-intro">
                        <?php echo $page->description ?>
                        </li>
                        <hr>
                       <?php if(isset($page->location) && !empty($page->location)) : ?>
                          <li class="page-block-tidbit">
                          <span class="fa fa-map-marker"></span> <?php echo $page->location ?>
                          </li>
                        <?php endif; ?>
                         <?php if(isset($page->email) && !empty($page->email)) : ?>
                          <li class="page-block-tidbit">
                          <span class="fa fa-envelope"></span> <?php echo $page->email ?>
                          </li>
                        <?php endif; ?>
                        <?php if(isset($page->phone) && !empty($page->phone)) : ?>
                          <li class="page-block-tidbit">
                          <span class="fa fa-phone"></span> <?php echo $page->phone ?>
                          </li>
                        <?php endif; ?>
                        <?php if(isset($page->website) && !empty($page->website)) : ?>
                          <li class="page-block-tidbit">
                          <span class="fa fa-link"></span> <a href="<?php echo $page->website ?>"><?php echo $page->website ?></a>
                          </li>
                        <?php endif; ?>


                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>
                  </div><!--user_profile end-->
                  <div class="suggestions full-width">
                    <div class="sd-title">
                      <h3>Church Members</h3>
                      <i class="la la-ellipsis-v"></i>
                    </div><!--sd-title end-->
                    <div class="suggestions-list">

                      <?php foreach($users->result() as $r) : ?>
          
          <div class="suggestion-usd">
                        <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>" width="40">
                        <div class="sgt-text">
                          <h4><?php echo $r->first_name ?> <?php echo $r->last_name ?></h4>
                          <!-- <span>PHP Developer</span> -->
                        </div>
                        <span><a href="<?php echo site_url("profile/" . $r->username) ?>"><i class="la la-plus"></i></a></span>
                      </div>
        <?php endforeach; ?>



                      
                      


                      <div class="view-more">
                        <!-- <a href="#" title="">View More</a> -->
                        <a href="<?php echo site_url("pages/members/" . $slug) ?>">View More</a>
                      </div>
                    </div><!--suggestions-list end-->
                  </div><!--suggestions end-->
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-6">
                <div class="main-ws-sec">
                  <div class="user-tab-sec">
                    <h3><?php echo $page->name ?></h3>
                    <div class="star-descp">
                      <!-- <span>Graphic Designer at Self Employed</span> -->
                      <!-- <ul>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-half-o"></i></li>
                      </ul> -->
                      <!-- <a href="#" title="">Status</a> -->
                    </div><!--star-descp end-->
                    
                    <div class="st2">
                      <ul class="job-dt">
                            <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li>
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current" id="feed-dd">
                      <?php if($member != null && $member->roleid == 1) : ?>
                          <?php
                          $postAsDefault = "page";
                          $postAs = $page->name;
                          $postAsImg = $page->profile_avatar;

                           ?>
                        <?php endif; ?>
                        <?php 
                        // Page defaults
                        $editor_placeholder = lang("ctn_579") . " " . $page->name . "'s ".lang("ctn_552")." ...";
                        $target_type = "page_profile";
                        $targetid = $page->ID;
                        ?>

                        <?php if( ($page->posting_status == 0 && $member != null && $member->roleid) || ($page->posting_status == 1 && $member != null) || ($page->posting_status == 2) || ( $this->common->has_permissions(array("admin", "page_admin"), $this->user) ) ) : ?>
                        <?php // only show editor if [admins can only post || members can only post || anyone can post || Is page admin/admin role] ?>
                        <?php if($this->common->has_permissions(array("admin", "page_creator", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
                        <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval):
                    ?>

                     <?php
                if($this->settings->info->global_premium && 
                  ($this->user->info->premium_time != -1 && 
                    $this->user->info->premium_time < time()) ) {

                  //$this->session->set_flashdata("globalmsg", lang("success_29"));
                  //redirect(site_url("funds/plans"));
                      ?>
                      <div class="alert alert-danger">
                        <strong>Alert!</strong> Please make payment to Continues process.
                        <br><br>
                        <a href="<?php echo base_url();?>funds/plans" class="btn btn-primary">Make Payment</a>
                      </div>

                      <?php
                }else{

    
                ?>
                       <?php include(APPPATH . "views/feed/editor.php"); ?>

                     <?php } ?>
                       <?php endif; ?>
                       <?php endif; ?>
                      <?php endif; ?>
                    <div class="posts-section" id="home_posts">
                     
                    </div><!--posts-section end-->
                  </div><!--product-feed-tab end-->
                  
                 
                  
                 
                  
                </div><!--main-ws-sec end-->
              </div>
              <div class="col-lg-3">
                <div class="right-sidebar">
                  <div class="message-btn">
                    <a href="#" title=""><i class="fa fa-envelope"></i> Message</a>
                  </div>



                  <div class="widget widget-portfolio">
                    <div class="wd-heady">
                      <h3><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></h3>
                      <img src="<?php echo base_url();?>assets/images/photo-icon.png" alt="">
                    </div>
                    <div class="pf-gallery">
                      <ul>


                        <?php foreach($albums->result() as $r) : ?>
            <li >
            <?php if(isset($r->file_name)) : ?>
              <a href="<?php echo site_url("pages/view_album/" . $r->ID) ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>"></a>
            <?php else : ?>
              <a href="<?php echo site_url("pages/view_album/" . $r->ID) ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/default_album.png"></a>
            <?php endif; ?>
            <!-- <p><a href="<?php echo site_url("pages/view_album/" . $r->ID) ?>"><?php echo $r->name ?></a></p> -->
            </li>
          <?php endforeach; ?>



                        
                      </ul>
                    </div><!--pf-gallery end-->
                  </div><!--widget-portfolio end-->






                  <div class="widget widget-portfolio">
                    <div class="wd-heady">
                      <h3><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></h3>
                      
                    </div>
                    <div >
                <ul class="social_links">
                      <?php foreach($events->result() as $r) : ?>
          <li >
            <p class="page-event-title"><a href="<?php echo site_url("pages/view_event/" . $r->ID) ?>"><?php echo $r->title ?></a></p>
            <p><span class="glyphicon glyphicon-calendar"></span> <?php echo $r->start ?> ~ <?php echo $r->end ?> </p>
          </li>

        <?php endforeach; ?>
  </ul>
                      
                    </div><!--pf-gallery end-->
                  </div><!--widget-portfolio end-->




                </div><!--right-sidebar end-->
              </div>
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>











<!-- Button to Open the Modal -->
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Open modal
  </button>
 -->
  <!-- The Modal -->



    <div class="modal" id="bannermodal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Banner Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <?php echo form_open_multipart(site_url("pages/update_pic/".$page->ID), array("class" => "form-horizontal")) ?>

          <div class="cp-field">
        <h5 for="inputEmail3">Banner Image</h5>
         
        <div class="cpp-fiel">
        <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_header ?>" width="100%" />
        <?php if($this->settings->info->avatar_upload) : ?>
            <input type="file" name="userfile_profile" /> 
         <?php endif; ?>
        </div>
    

         <input type="submit" name="s" value="Submit" class="btn btn-primary" />
    </div>
    <br>

         
<?php echo form_close() ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>


  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Profile Pic</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <?php echo form_open_multipart(site_url("pages/update_pic/".$page->ID), array("class" => "form-horizontal")) ?>

          <div class="cp-field">
        <h5 for="inputEmail3">Profile Pic</h5>
        <div class="cpp-fiel">
        <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" width="75px"/>
        <?php if($this->settings->info->avatar_upload) : ?>
            <input type="file" name="userfile" class="form-control" /> 
            <br>
         <?php endif; ?>
        </div>

         <input type="submit" name="s" value="Submit" class="btn btn-primary" />
    </div>
    <br>

         
<?php echo form_close() ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>




   <?php echo form_open(site_url("pages/report_page/" . $page->ID)) ?>
 <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-flag"></span> <?php echo lang("ctn_578") ?> <?php echo $page->name ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_580") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="reason">
                    </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_578") ?>">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?>