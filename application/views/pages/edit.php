<div class="container">
        <div class="add-billing-method">
                                            <h3>Update Church</h3>
                                            
                                            <div class="payment_methods">
                                                <h4>Update Church Details</h4>
                                                                 <?php echo form_open_multipart(site_url("pages/edit_page_pro/" . $page->ID), array("class" => "form-horizontal")) ?>
                                                    <div class="row">


















                                                        <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 >Church Name</h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" name="name" class="form-control" value="<?php echo $page->name ?>">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 >Church Description</h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" class="form-control" name="description" value="<?php echo $page->description ?>">
                    </div>
                </div>
                <?php if(!$this->settings->info->page_slugs) : ?>
                    <div class="col-lg-6">
                        <div class="cc-head">
                        <h5 ><?php echo lang("ctn_535") ?></h5>
                        </div>
                        <div class="col-sm-7">
                            <input type="text" name="slug" class="form-control" id="slug-check" value="<?php echo $page->slug ?>">
                            <span class="help-block"><?php echo lang("ctn_536") ?>: <?php echo site_url("pages/view/") ?><strong>my-unique-slug</strong></span>
                        </div>
                        <div class="col-sm-3" id="slug-msg">

                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 >Church Category</h5>
                    </div>
                    <div class="inpt-field">
                        <select name="categoryid" class="form-control">
                           <?php foreach($categories->result() as $r) : ?>
                                <option value="<?php echo $r->ID ?>" <?php if($page->categoryid == $r->ID) echo "selected" ?>><?php echo $r->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 >Church Type</h5>
                    </div>
                    <div class="inpt-field">
                        <select name="type" class="form-control">
                            <option value="0"><?php echo lang("ctn_539") ?></option>
                            <option value="1" <?php if($page->type == 1) echo "selected" ?>><?php echo lang("ctn_540") ?></option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 >Church Profile Pic</h5>
                    </div>
                    <div class="inpt-field">
                    <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" width="100px" height="100px"/>
                    
                    <input type="file" name="userfile" /> 
                    
                    </div>
                </div>
                <div class="col-lg-6 clearfix">
                    <div class="cc-head">
                    <h5 >Church Banner</h5>
                    </div>
                    <div class="inpt-field">
                        <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_header ?>" width="100px" height="100px"/>
                   
                        <input type="file" name="userfile_profile" /> 
                    </div>
                </div>
                

                <div class="col-lg-6 ">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_497") ?></h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" name="location" class="form-control map_name" value="<?php echo $page->location ?>">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_24") ?></h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" name="email" class="form-control" value="<?php echo $page->email ?>">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_544") ?></h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" name="phone" class="form-control" value="<?php echo $page->phone ?>">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_545") ?></h5>
                    </div>
                    <div class="inpt-field">
                        <input type="text" name="website" class="form-control" value="<?php echo $page->website ?>">
                    </div>
                </div>
               
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_546") ?></h5>
                    </div>
                    <div class="inpt-field">
                    <select name="posting_status" class="form-control">
                        <option value="0"><?php echo lang("ctn_547") ?></option>
                        <option value="1" <?php if($page->posting_status == 1) echo "selected" ?>><?php echo lang("ctn_548") ?></option>
                        <option value="2" <?php if($page->posting_status == 2) echo "selected" ?>><?php echo lang("ctn_549") ?></option>
                    </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cc-head">
                    <h5 ><?php echo lang("ctn_550") ?></h5>
                    </div>
                    <div class="inpt-field">
                    <select name="nonmembers_view" class="form-control">
                        <option value="0"><?php echo lang("ctn_53") ?></option>
                        <option value="1" <?php if($page->nonmembers_view) echo "selected" ?>><?php echo lang("ctn_54") ?></option>
                    </select>
                    </div>
                </div>
                                                        
                                                       <!--  <div class="col-lg-6">
                                                           <div class="cc-head">
                                                               <h5>First Name</h5>
                                                           </div>
                                                           <div class="inpt-field">
                                                               <input type="text" name="f-name" placeholder="">
                                                           </div>inpt-field end
                                                       </div> -->
                                                        
                                                       
                                                        <div class="col-lg-12">
                                                            <button type="submit" class="btn btn-success " value="<?php echo lang("ctn_531") ?>">Update</button>
                                                        </div>
                                                    </div>
                                                <?php echo form_close() ?>
                                               

                                            </div>
                                        </div><!--add-billing-method end-->

                                        </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#slug-check').on("change", function() {
                var slug = $('#slug-check').val();
                $.ajax({
                    url: global_base_url + 'pages/check_slug',
                    type: 'GET',
                    data: {
                        slug : slug
                    },
                    dataType : 'json',
                    success: function(msg) {
                        if(msg.error) {
                            $('#slug-msg').html(msg.error_msg);
                            return;
                        }
                        if(msg.status == 0) {
                            $('#slug-msg').html(msg.status_msg);
                        } else if(msg.status == 1) {
                            $('#slug-msg').html(msg.status_msg);
                        }
                        return;
                    }
                })
            });
        });
    </script>