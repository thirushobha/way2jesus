
<section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $page->profile_header ?>" alt="">
      <!-- <a href="#" title=""><i class="fa fa-camera"></i> Change Image</a> -->

       <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<a href="<?php echo site_url("pages/edit_page/" . $page->ID) ?>" class="btn btn-warning btn-xs mr-5"><span class="fa fa-pencil"></span></a> <a href="<?php echo site_url("pages/delete_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php echo lang("ctn_551") ?>')" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
<?php endif; ?>


    </section>


    <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" alt="">
                      <a href="#" title=""><i class="fa fa-camera"></i></a>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        
                        <?php if($member == null) : ?>
   <li> <a href="<?php echo site_url("pages/join_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_554") ?></a>
   </li>
  <?php else : ?>
    <li>
    <a href="<?php echo site_url("pages/leave_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="fa fa-check"></span> <?php echo lang("ctn_34") ?></a> 
  </li>
  <?php endif; ?>
  <li><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="fa fa-flag"></span></button>
  </li>

                      </ul>
                      <!-- <ul class="flw-status">
                        <li>
                          <span>Members</span>
                          <b>34</b>
                        </li>
                        <li>
                          <span>Followers</span>
                          <b>155</b>
                        </li>
                      </ul> -->
                    </div><!--user_pro_status end-->
                     <ul class="user-fw-status">

                        
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>


                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>
                  </div><!--user_profile end-->
                
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-9 widget">
                <div class="main-ws-sec">
                  <div class="user-tab-sec">
                    <h3><?php echo $page->name ?></h3>
                    <div class="star-descp">
                      <!-- <span>Graphic Designer at Self Employed</span> -->
                      <!-- <ul>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-half-o"></i></li>
                      </ul> -->
                      <!-- <a href="#" title="">Status</a> -->
                    </div><!--star-descp end-->
                    
                    <div class="st2">
                      <ul class="job-dt">
                            <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li>
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current" id="feed-dd">



                      
      <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3 col-md-4 pd-left-none no-pd">
                <div class="main-left-sidebar no-margin">
                  <div class="user-data full-width">
                    <div class="user-profile">
                      <div class="username-dt">
                        <div class="usr-pic">
                          <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" alt="">
                        </div>
                      </div><!--username-dt end-->
                      <div class="user-specs">
                        <h3><?php echo $page->name ?></h3>
                        <!-- <span>Graphic Designer at Self Employed</span> -->
                      </div>
                    </div><!--user-profile end-->
                    <ul class="user-fw-status">
                      <li>
                        <h4>Who's going? </h4>
                        <span>  <?php echo $attending_count ?></span>
                      </li>
                      
                      <li>
                        <input type="button" class="btn btn-primary form-control btn-sm" value="<?php echo lang("ctn_591") ?>" onclick="view_list(<?php echo $event->ID ?>,1)">

                      </li>
                      <li>
                        <input type="button" class="btn btn-info form-control btn-sm" value="<?php echo lang("ctn_592") ?>" onclick="view_list(<?php echo $event->ID ?>,0)">

                      </li>
                    </ul>
                  </div><!--user-data end-->
                  
                 
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-9 col-md-8 no-pd">
                <div class="main-ws-sec">
                  
                  <div class="posts-section">
                    <div class="post-bar">
                      <div class="post_topbar">
                        
                        
                      </div>
                      <div class="epi-sec">
                        
                        <ul class="bk-links">

       <?php if($attending == null) : ?>
    <li><a href="<?php echo site_url("pages/join_event/" . $event->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_593") ?></a></li>
  <?php else : ?>
   <li> <a href="<?php echo site_url("pages/leave_event/" . $event->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> <?php echo lang("ctn_594") ?></a> </li>
  <?php endif; ?>

                          
                        </ul>
                      </div>
                      <div class="job_descp">
                        <h3><i class="fa fa-user"></i>  <?php echo $event->title ?></h3>
                        
                        <p><?php echo $event->description ?>
                        </p>
                        <p><span class="fa fa-clock-o"></span> <?php echo $event->start ?> ~ <?php echo $event->end ?></p>
                        <?php if(!empty($event->location)) : ?>
                        <p><span class="fa fa-map-marker"></span> <?php echo $event->location ?></p>
                        <?php endif; ?>
                      </div>
                      
                    </div><!--post-bar end-->
                    
                    
                    
                  </div><!--posts-section end-->
                </div><!--main-ws-sec end-->
              </div>
              
            </div>
          </div><!-- main-section-data end-->
                      
                    




                  </div><!--product-feed-tab end-->
                  
                 
                  
                 
                  
                </div><!--main-ws-sec end-->
              </div>
              
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>












 <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="view-area">
     
    </div>
  </div>
</div>
<script type="text/javascript">
function view_list(eventid, type) 
{
  $.ajax({
    url: global_base_url + 'pages/view_event_users/' + eventid,
    type: 'GET',
    data: {
      type : type
    },
    success: function(msg) {
      $('#viewModal').modal('show');
      $('#view-area').html(msg);
    }
  })
}
</script>