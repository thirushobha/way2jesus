
<section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $page->profile_header ?>" alt="">
      <!-- <a href="#" title=""><i class="fa fa-camera"></i> Change Image</a> -->

       <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<a href="<?php echo site_url("pages/edit_page/" . $page->ID) ?>" class="btn btn-warning btn-xs mr-5"><span class="fa fa-pencil"></span></a> <a href="<?php echo site_url("pages/delete_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php echo lang("ctn_551") ?>')" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
<?php endif; ?>


    </section>


    <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" alt="" width="150px">
                      <a href="#" title=""><i class="fa fa-camera"></i></a>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        
                        <?php if($member == null) : ?>
   <li> <a href="<?php echo site_url("pages/join_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_554") ?></a>
   </li>
  <?php else : ?>
    <li>
    <a href="<?php echo site_url("pages/leave_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="fa fa-check"></span> <?php echo lang("ctn_34") ?></a> 
  </li>
  <?php endif; ?>
  <li><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="fa fa-flag"></span></button>
  </li>

                      </ul>
                      <!-- <ul class="flw-status">
                        <li>
                          <span>Members</span>
                          <b>34</b>
                        </li>
                        <li>
                          <span>Followers</span>
                          <b>155</b>
                        </li>
                      </ul> -->
                    </div><!--user_pro_status end-->
                     <ul class="user-fw-status">

                        
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>


                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>
                  </div><!--user_profile end-->
                
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-9 widget">
                <div class="main-ws-sec">
                  <div class="user-tab-sec">
                    <h3><?php echo $page->name ?></h3>
                    <div class="star-descp">
                      <!-- <span>Graphic Designer at Self Employed</span> -->
                      <!-- <ul>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-half-o"></i></li>
                      </ul> -->
                      <!-- <a href="#" title="">Status</a> -->
                    </div><!--star-descp end-->
                    
                    <div class="st2">
                      <ul class="job-dt">
                            <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li>
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current" id="feed-dd">



                      <h4 class="page-header-title"> <span class="glyphicon glyphicon-file"></span>Members List</h4>
         <br>

          <div class="db-header clearfix">
            
            <div class="db-header-extra form-inline"> 

                         <div class="form-group has-feedback no-margin">
<div class="input-group">
<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="fa fa-search" aria-hidden="true"></span></button>
        <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="fa fa-check" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
          <li><a href="#" onclick="change_search(1)"><span class="fa fa-check nodisplay" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
          <li><a href="#" onclick="change_search(2)"><span class="fa fa-check nodisplay" id="username-exact"></span> <?php echo lang("ctn_77") ?></a></li>
          <li><a href="#" onclick="change_search(3)"><span class="fa fa-check nodisplay" id="firstname-exact"></span> <?php echo lang("ctn_79") ?></a></li>
          <li><a href="#" onclick="change_search(4)"><span class="fa fa-check nodisplay" id="lastname-exact"></span> <?php echo lang("ctn_80") ?></a></li>
        </ul>
      </div><!-- /btn-group -->
</div>
</div>

<?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_469") ?>" data-toggle="modal" data-target="#addModal">
<?php endif; ?>

                         
        </div>
        </div>
        <br>
        <table id="friends-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header"><td><?php echo lang("ctn_347") ?></td><td><?php echo lang("ctn_25") ?></td><td><?php echo lang("ctn_29") ?></td><td><?php echo lang("ctn_30") ?></td><td><?php echo lang("ctn_568") ?></td><td><?php echo lang("ctn_52") ?></td></tr>
</thead>
<tbody>
</tbody>
</table>
                      
                    




                  </div><!--product-feed-tab end-->
                  
                 
                  
                 
                  
                </div><!--main-ws-sec end-->
              </div>
              
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>
























<?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
  <?php echo form_open(site_url("pages/invite_user/" . $page->ID)) ?>
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-person"></span> <?php echo lang("ctn_469") ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_504") ?></label>
                    <div class="col-md-8">
                        <select class="js-example-basic-multiple" style="width: 100%;" name="with_users[]" id="with_users" multiple="multiple">
                        
                        </select>
                    </div>
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_469") ?>">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?>


 <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="edit-member">
     
    </div>
  </div>
</div>
<?php endif; ?>
 <script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#friends-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        { "orderable" : false },
        null,
        null,
        null,
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("pages/members_page/" . $page->ID) ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
      "search-like", 
      "search-exact",
      "username-exact",
      "firstname-exact",
      "lastname-exact",

      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }

    function edit_member(userid) 
    {
      console.log("D");
      $.ajax({
        url: global_base_url + 'pages/edit_member/' + userid,
        type: 'GET',
        data: {
        },
        success: function(msg) {
          $('#editModal').modal('show');
          $('#edit-member').html(msg);
        }
      });
    }
</script>