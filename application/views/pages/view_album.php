
<style type="text/css">
  .company-up-info img{
    border-radius: 0px;
  }
</style>





<section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $page->profile_header ?>" alt="">
      <!-- <a href="#" title=""><i class="fa fa-camera"></i> Change Image</a> -->

       <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<a href="<?php echo site_url("pages/edit_page/" . $page->ID) ?>" class="btn btn-warning btn-xs mr-5"><span class="fa fa-pencil"></span></a> <a href="<?php echo site_url("pages/delete_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php echo lang("ctn_551") ?>')" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
<?php endif; ?>


    </section>


    <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" alt="" width="150px">
                      <a href="#" title=""><i class="fa fa-camera"></i></a>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        
                        <?php if($member == null) : ?>
   <li> <a href="<?php echo site_url("pages/join_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_554") ?></a>
   </li>
  <?php else : ?>
    <li>
    <a href="<?php echo site_url("pages/leave_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="fa fa-check"></span> <?php echo lang("ctn_34") ?></a> 
  </li>
  <?php endif; ?>
  <li><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="fa fa-flag"></span></button>
  </li>

                      </ul>
                      <!-- <ul class="flw-status">
                        <li>
                          <span>Members</span>
                          <b>34</b>
                        </li>
                        <li>
                          <span>Followers</span>
                          <b>155</b>
                        </li>
                      </ul> -->
                    </div><!--user_pro_status end-->
                     <ul class="user-fw-status">

                        
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>


                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>
                  </div><!--user_profile end-->
                
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-9 widget">
                <div class="main-ws-sec">
                  <div class="user-tab-sec">
                    <h3><?php echo $page->name ?></h3>
                    <div class="star-descp">
                      <!-- <span>Graphic Designer at Self Employed</span> -->
                      <!-- <ul>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-half-o"></i></li>
                      </ul> -->
                      <!-- <a href="#" title="">Status</a> -->
                    </div><!--star-descp end-->
                    
                    <div class="st2">
                      <ul class="job-dt">
                            <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li>
                            <li class="active"><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current" id="feed-dd">



                      
        <div class="company-title">
          <h3><?php echo $album->name ?>
           <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 

<input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_581") ?>" data-toggle="modal" data-target="#addModal"> <input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_582") ?>" data-toggle="modal" data-target="#addMultiModal">

<?php endif; ?>

          </h3>
          <p><?php echo $album->description ?></p>
          



        </div><!--company-title end-->
        <div class="companies-list">
          <div class="row album-images">


            

            <?php if($images->num_rows() == 0) : ?>
<p><?php echo lang("ctn_583") ?> <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"><?php echo lang("ctn_584") ?></a> <?php echo lang("ctn_585") ?></p>
<?php else : ?>

   



<?php foreach($images->result() as $r) : ?>

   <div class="col-lg-3 col-md-4 col-sm-6 col-12">
              <div class="company_profile_info">
                <div class="company-up-info">
                  
                  <?php if(isset($r->file_name)) : ?>
    <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>" width="140" alt="<?php echo $r->name . "<br>" . $r->description ?>" class="album-image" style="cursor: pointer">
  <?php else : ?>
    <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/default_album.png" class="album-image" width="140" alt="<?php echo $r->name . "<br>" . $r->description ?>" style="cursor: pointer">
  <?php endif; ?>

                  <h3><?php echo $r->name ?></h3>
                  <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?>
                  <ul>
                    <li><a href="javascript:void(0)" onclick="edit_image(<?php echo $r->ID ?>)" class="btn btn-warning btn-xs"><span class="fa fa-edit"></span></a></li>
                    <li> <a href="<?php echo site_url("pages/delete_image/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a></li>
                    
                  </ul>
                  <?php endif; ?>

                </div>
                <!-- <a href="#" title="" class="view-more-pro">View Profile</a> -->
              </div><!--company_profile_info end-->
            </div>





<?php endforeach; ?>

<?php endif; ?>


          
            
          </div>
        </div><!--companies-list end-->
        <div class="process-comm">
          <?php echo $this->pagination->create_links() ?>
          <!-- <a href="#" title=""><img src="<?php echo base_url();?>assets/images/process-icon.png" alt=""></a> -->
        </div>
                      
                    




                  </div><!--product-feed-tab end-->
                  
                 
                  
                 
                  
                </div><!--main-ws-sec end-->
              </div>
              
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>





























<?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
 <?php echo form_open_multipart(site_url("pages/add_photo/" . $album->ID)) ?>
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-picture"></span> <?php echo lang("ctn_586") ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_81") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_271") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="description">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_556") ?></label>
                    <div class="col-md-8">
                        <?php echo $album->name ?>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_499") ?></label>
                    <div class="col-md-8">
                        <input type="file" class="form-control" name="image_file">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_500") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="image_url" placeholder="http://www ...">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_574") ?></label>
                    <div class="col-md-8">
                        <input type="checkbox" class="form-control" name="feed_post" value="1" checked>
                        <span class="help-area"><?php echo lang("ctn_587") ?></span>
                    </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_584") ?>">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?>


 <?php echo form_open_multipart(site_url("pages/add_multi_photo/" . $album->ID)) ?>
 <div class="modal fade" id="addMultiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-picture"></span> <?php echo lang("ctn_588") ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_556") ?></label>
                    <div class="col-md-8">
                        <?php echo $album->name ?>
                    </div>
            </div>
            <div id="multi">
	            <div class="form-group">
	                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_499") ?></label>
	                    <div class="col-md-8">
	                        <input type="file" class="form-control" name="image_file_1">
	                    </div>
	            </div>
	            <div class="form-group">
	                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_500") ?></label>
	                    <div class="col-md-8">
	                        <input type="text" class="form-control" name="image_url_1" placeholder="http://www ...">
	                    </div>
	            </div>
	        </div>
	        <input type="hidden" id="amount" name="amount" value="1">
	        <input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_589") ?>" onclick="add_photo()">

          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_574") ?></label>
                    <div class="col-md-8">
                        <input type="checkbox" class="form-control" name="feed_post" value="1" checked>
                        <span class="help-area"><?php echo lang("ctn_587") ?></span>
                    </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_584") ?>">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?>


 <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="edit-album">
     
    </div>
  </div>
</div>

<?php endif; ?>


<script type="text/javascript">
function add_photo() 
{
	var id = parseInt($('#amount').val());
	id = id + 1;
	$('#amount').val(id);

	var html = '<div class="form-group">'
	                    +'<label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_499") ?> '+id+'</label>'
	                    +'<div class="col-md-8">'
	                        +'<input type="file" class="form-control" name="image_file_'+id+'">'
	                    +'</div>'
	            +'</div>'
	            +'<div class="form-group">'
	                    +'<label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_500") ?> '+id+'</label>'
	                    +'<div class="col-md-8">'
	                        +'<input type="text" class="form-control" name="image_url_'+id+'" placeholder="http://www ...">'
	                    +'</div>'
	            +'</div>';
	$('#multi').append(html);
}

function edit_image(id) 
{
	$.ajax({
		url: global_base_url + 'pages/edit_image/' + id,
		type: 'GET',
		data: {
		},
		success: function(msg) {
			$('#editModal').modal('show');
			$('#edit-album').html(msg);
		}
	})
}
</script>