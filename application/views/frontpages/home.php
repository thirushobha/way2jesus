<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.themesindustry.com/html/xeone/index-modern.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Mar 2019 06:07:28 GMT -->
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> - <?php endif; ?><?php echo $this->settings->info->site_name ?></title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/cubeportfolio.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/jquery.fancybox.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/jquery.background-video.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/settings.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/layers.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/navigation.css">
<link rel="stylesheet" href="<?php echo base_url();?>front/css/style.css">


<!-- SCRIPTS -->
        <script type="text/javascript">
        var global_base_url = "<?php echo site_url('/') ?>";
        </script>

<!-- Favicon: http://realfavicongenerator.net -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url();?>images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url();?>images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<style type="text/css">
  .rev_slider .slotholder:after {
    width: 100%;
    height: 100%;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    pointer-events: none;

    /* black overlay with 50% transparency */
    background: rgba(0, 0, 0, 0.5);
}
</style>
</head>
<body  data-spy="scroll" data-target=".navbar" data-offset="90">

<!--PreLoader-->
<div class="loader">
   <div class="loader-inner">
      <div class="loader-blocks">
         <span class="block-1"></span>
         <span class="block-2"></span>
         <span class="block-3"></span>
         <span class="block-4"></span>
         <span class="block-5"></span>
         <span class="block-6"></span>
         <span class="block-7"></span>
         <span class="block-8"></span>
         <span class="block-9"></span>
         <span class="block-10"></span>
         <span class="block-11"></span>
         <span class="block-12"></span>
         <span class="block-13"></span>
         <span class="block-14"></span>
         <span class="block-15"></span>
         <span class="block-16"></span>
      </div>
   </div>
</div>
<!--PreLoader Ends-->

<!-- header -->
<header class="site-header transparent-sidemenu">
   <nav class="navbar navbar-expand-lg transparent-bg static-nav">
      <div class="container">
         <a class="navbar-brand" href="<?php echo base_url();?>">
         <!-- <img src="<?php echo base_url();?>front/images/logo.png" alt="logo"> -->
         <img src="<?php echo base_url();?>front/images/logo2.png" alt=""/>
         </a>
         <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#xenav">
            <span> </span>
            <span> </span>
            <span> </span>
         </button>
         <div class="collapse navbar-collapse" id="xenav">
            <ul class="navbar-nav ml-auto">
               <li class="nav-item active">
                  <a class="nav-link pagescroll" href="#revo_main_wrapper">Home</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-feature">About Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-team">Pastors</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-churches">Churches</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#portfolio-xe">Slogans</a>
               </li>
               <!-- <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-pricings">Packages</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-testimonial">Clients</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-blog">Blog</a>
               </li> -->

               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#contactus">contact</a>
               </li>
               <?php
               
               if($loggedin){ ?>
                <li class="nav-item">
                  <a class="nav-link " href="<?php echo base_url();?>home"><i class="fa fa-user-o"></i> My Profile</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link " href="<?php echo site_url("login/logout/" . $this->security->get_csrf_hash()) ?>"><i class="fa fa-sign-out"></i> Logout</a>
               </li>

               

               <?php }else{
                ?>
                 <li class="nav-item">
                  <a class="nav-link " href="<?php echo base_url();?>login"><i class="fa fa-sign-in"></i> Login</a>
               </li>

               <li class="nav-item">
                  <a class="nav-link " href="<?php echo base_url();?>register"><i class="fa fa-user-o"></i> Register</a>
               </li>

               <?php
             }
               ?>
            </ul>
         </div>
      </div>

      <!--side menu open button-->
      <!-- <a href="javascript:void(0)" class="d-none d-lg-inline-block sidemenu_btn" id="sidemenu_toggle">
          <span></span> <span></span> <span></span>
       </a> -->
   </nav>

   <!-- side menu -->
   <!-- <div class="side-menu">
      <div class="inner-wrapper">
         <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
         <nav class="side-nav">
            <ul class="navbar-nav w-100">
               <li class="nav-item active">
                  <a class="nav-link pagescroll" href="#revo_main_wrapper">Home</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-feature">About Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-team">Team</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#portfolio-xe">Slogans</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#contactus">contact</a>
               </li>

               <?php
               
               if($loggedin){ ?>
                <li class="nav-item">
                  <a class="nav-link pagescroll" href="<?php echo base_url();?>home"><i class="fa fa-sign-in"></i> My Profile</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="<?php echo site_url("login/logout/" . $this->security->get_csrf_hash()) ?>"><i class="fa fa-user-o"></i> Logout</a>
               </li>

               

               <?php }else{
                ?>
                 <li class="nav-item">
                  <a class="nav-link pagescroll" href="#portfolio-xe"><i class="fa fa-sign-in"></i> Login</a>
               </li>

               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#portfolio-xe"><i class="fa fa-user-o"></i> Register</a>
               </li>

               <?php
             }
               ?>

              
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-pricings">Packages</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-testimonial">Clients</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link pagescroll" href="#our-blog">Blog</a>
               </li>
               
            </ul>
         </nav>

         <div class="side-footer w-100">
            <ul class="social-icons-simple white top40">
            <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
            <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>
            <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
         </ul>
         <p class="whitecolor">&copy; 2019 Way2Jesus. Made With Love by UBN QTech</p>
         </div>
      </div>
   </div> -->



   <a id="close_side_menu" href="javascript:void(0);"></a>
   <!-- End side menu -->
</header>
<!-- header -->




<!--Main Slider-->
<div id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
<div id="rev_arrows" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
   <ul> <!-- SLIDE  -->

          <!-- SLIDE  -->
      <li data-index="rs-third" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?php echo base_url();?>front/images/sliders/slider9.jpg">
      <!-- MAIN IMAGE -->
      <img src="<?php echo base_url();?>front/images/sliders/slider9.jpg" alt="" data-bgposition="center center" data-ease="Linear.easeNone" data-rotatestart="0"  data-bgparallax="10" class="rev-slidebg" data-no-retina>

      <!-- LAYER NR. 1 -->  
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-50']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize font-xlight whitecolor text-center">Way2Jesus</h1> </div>

      <!-- LAYER NR. 2 -->
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1500"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize fontbold whitecolor text-center">In One Platform</h1> </div>

      <!-- LAYER NR. 3 -->
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']" 
      data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
      data-textAlign="['center','center','center','center']"  
      data-responsive_offset="on" data-start="2000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h4 class="whitecolor font-light text-center">Connecting People with God’s love</h4> </div>
      </li>
      
      <li data-index="rs-first" data-transition="fadeout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?php echo base_url();?>front/images/sliders/slider4.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off">
      <!-- MAIN IMAGE -->
      <img src="<?php echo base_url();?>front/images/sliders/slider4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

      <!-- LAYER NR. 1 -->  
      <div class="tp-caption tp-resizeme" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-50']" 
       data-width="none" data-height="none" data-type="text" 
       data-textAlign="['center','center','center','center']" 
       data-responsive_offset="on" data-start="1000"
       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize font-xlight whitecolor text-center">Way2Jesus</h1> </div>

      <!-- LAYER NR. 2 -->
      <div class="tp-caption tp-resizeme" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
       data-width="none" data-height="none" data-type="text" 
       data-textAlign="['center','center','center','center']" 
       data-responsive_offset="on" data-start="1500"
       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize fontbold whitecolor text-center">In One Platform</h1> </div>

      <!-- LAYER NR. 3 -->
      <div class="tp-caption tp-resizeme" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']" 
       data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
       data-textAlign="['center','center','center','center']"  
       data-responsive_offset="on" data-start="2000"
       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
       <h4 class="whitecolor font-light text-center">Connecting People with God’s love</h4> </div>
      </li>

      <!-- SLIDE  -->
      <li data-index="rs-second" data-transition="fadetotopfadefrombottom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="<?php echo base_url();?>front/images/sliders/slider5.jpg">
      <!-- MAIN IMAGE -->
      <img src="<?php echo base_url();?>front/images/sliders/slider5.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

      <!-- LAYER NR. 1 -->  
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-50']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize font-xlight whitecolor text-center">Way2Jesus</h1> </div>

      <!-- LAYER NR. 2 -->
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1500"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize fontbold whitecolor text-center">In One Platform</h1> </div>

      <!-- LAYER NR. 3 -->
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']" 
      data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
      data-textAlign="['center','center','center','center']"  
      data-responsive_offset="on" data-start="2000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h4 class="whitecolor font-light text-center">Connecting People with God’s love</h4> </div>
      </li>



     
      <!-- <li data-index="rs-third" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?php echo base_url();?>front/images/sliders/slider10.jpg">
      
      <img src="<?php echo base_url();?>front/images/sliders/slider10.jpg" alt="" data-bgposition="center center" data-ease="Linear.easeNone" data-rotatestart="0"  data-bgparallax="10" class="rev-slidebg" data-no-retina>

       
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-50']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize font-xlight whitecolor text-center">Next Big Thing</h1> </div>

     
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
      data-width="none" data-height="none" data-type="text" 
      data-textAlign="['center','center','center','center']" 
      data-responsive_offset="on" data-start="1500"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h1 class="text-capitalize fontbold whitecolor text-center">In One Page</h1> </div>

 
      <div class="tp-caption tp-resizeme" 
      data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
      data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']" 
      data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
      data-textAlign="['center','center','center','center']"  
      data-responsive_offset="on" data-start="2000"
      data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'>
      <h4 class="whitecolor font-light text-center">The Best Multipurpose One Page Template in Market</h4> </div>
      </li> -->

   </ul>
</div>
</div>
<!--Main Slider ends -->

<!--Some Services-->  
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div id="services-slider" class="owl-carousel">
            <div class="item">
               <div class="service-box">
                  <span class="bottom25"><i class="fa fa-mobile-phone"></i></span>
                  <h4 class="bottom10"><a href="javascript:void(0)">Mobile App</a></h4>
                  <p>We have our own mobile app to connect to mobile platform.</p>
               </div>        
            </div>
            <div class="item">
               <div class="service-box">
                  <span class="bottom25"><i class="fa fa-laptop"></i></span>
                  <h4 class="bottom10"><a href="javascript:void(0)">Fully Responsive</a></h4>
                     <p>Our application easily adjustable to all devices and all browsers.</p>
               </div>        
            </div>
            <div class="item">
               <div class="service-box">
                  <span class="bottom25"><i class="fa fa-support"></i></span>
                  <h4 class="bottom10"><a href="javascript:void(0)">Full Support</a></h4>
                     <p>We Provide our support for users and pastors with 24/7.</p>
               </div>        
            </div>
            <div class="item">
               <div class="service-box">
                  <span class="bottom25"><i class="fa fa-edit"></i></span>
                  <h4 class="bottom10"><a href="javascript:void(0)">Secure</a></h4>
                     <p>We provide our security to user authentication to restrict others.</p>
               </div>        
            </div>
            <div class="item">
               <div class="service-box">
                  <span class="bottom25"><i class="fa fa-globe"></i></span>
                  <h4 class="bottom10"><a href="javascript:void(0)">Password Protected</a></h4>
                  <p>we have authenticate users to easily access to their profiles and their churches.</p>
               </div>        
            </div>
         </div>
      </div>
   </div>
</div>
<!--Some Services ends-->
       
<!--Some Feature -->  
<section id="our-feature" class="padding_bottom padding_top_half single-feature">
    <div class="container">
      <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <!-- <span>About us</span> -->
               <h2 class="darkcolor bottom10">About Us</h2>
               
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-7 col-sm-7 wow fadeInLeft" data-wow-delay="300ms">
            <div class="heading-title text-md-left text-center padding_bottom">
               <span>Connecting People with God’s love</span>
              
               

          <p>Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. When a user sign-ups as the pastor he can create and post and share all the updates about his work, teachings etc in his pastor profile.
          </p>
          <p>

          So our way2jesus platform is giving an opportunity to the pastors and churches to promote and share their works on a dedicated platform for Christianity where a normal user can view all the information about the churches and pastors around him based on his or her search.
        </p>
        <p>

          Way2jesus brings and unites all the churches and pastors at a single platform where they can share their prayers, announcements and other information about them with the users. . So we can say our Way2jesus is the complete dedicated platform to spread the word of Christianity where pastors and churches can connect with the people.
        </p>

               <a href="<?php echo base_url();?>register" class="button btnprimary top20 pagescroll">Register Now</a>
            </div>
         </div>
         <div class="col-md-5 col-sm-5 padding_bottom wow fadeInRight" data-wow-delay="350ms">
            <div class="image hover-effect">
               <img alt="video img" src="<?php echo base_url();?>frontassets/images/about-img.jpg">
               <!-- <a data-fancybox href="https://www.youtube.com/watch?v=GhvD7NtUT-Q&amp;autoplay=1&amp;rel=0&amp;controls=1&amp;showinfo=0" class="button-play fontmedium"><i class="fa fa-play"></i></a> -->
               <a data-fancybox href="<?php echo base_url();?>frontassets/images/about-img.jpg" class=" fontmedium"></a>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Some Feature ends-->              
        
   
<!-- WOrk Process-->  
<!-- <section id="our-process" class="padding gradient_bg_default">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <h2 class="whitecolor">Work <span class="fontregular">Process</span> </h2>
            </div>
         </div>
      </div>
      <div class="row">
         <ul class="process-wrapp">
            <li class="whitecolor wow fadeIn" data-wow-delay="300ms">
               <span class="pro-step bottom20">01</span>
               <p class="fontbold bottom25">Concept</p>
               <p>Quisque tellus risus, adipisci viverra bibendum urna.</p>
            </li>
            <li class="whitecolor wow fadeIn" data-wow-delay="400ms">
               <span class="pro-step bottom20">02</span>
               <p class="fontbold bottom25">Plan</p>
               <p>Quisque tellus risus, adipisci viverra bibendum urna.</p>
            </li>
            <li class="whitecolor wow fadeIn" data-wow-delay="500ms">
               <span class="pro-step bottom20">03</span>
               <p class="fontbold bottom25">Design</p>
               <p>Quisque tellus risus, adipisci viverra bibendum urna.</p>
            </li>
            <li class="whitecolor wow fadeIn" data-wow-delay="600ms">
               <span class="pro-step bottom20">04</span>
               <p class="fontbold bottom25">Development</p>
               <p>Quisque tellus risus, adipisci viverra bibendum urna.</p>
            </li>
            <li class="whitecolor wow fadeIn" data-wow-delay="700ms">
               <span class="pro-step bottom20">05</span>
               <p class="fontbold bottom25">Quality Check</p>
               <p>Quisque tellus risus, adipisci viverra bibendum urna.</p>
            </li>
         </ul>
      </div>
   </div>
</section> -->
<!--WOrk Process ends--> 

<!-- Our Team-->    
<section id="our-team" class="padding bglight">
   <div class="container">
      <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <!-- <span>Heros Behind the Company</span> -->
               <h2 class="darkcolor bottom20">Pastors</h2>
               <p class="heading_space">A pastor is an ordained leader of a Christian congregation. A pastor also gives advice and counsel to people from the community or congregation. </p>
            </div>
         </div>
      </div>
      <div class="row">
        <div class="col-md-12">
           <div id="ourteam-slider" class="owl-carousel">


            <?php

            foreach($pastors as $pastor):

            ?>


           <div class="item">
              <div class="team-box">
               <div class="image">
                  <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $pastor['avatar']; ?>" alt="">
               </div>
               <div class="team-content gradient_bg whitecolor">
                  <h3><a href="<?php echo base_url();?>profile/<?php echo $pastor['username'];?>" target="_blank"><?php echo $pastor['first_name'].' '.$pastor['last_name']?></a></h3>
                  <p class="bottom40"><a href="<?php echo base_url();?>profile/<?php echo $pastor['username'];?>" target="_blank">@<?php echo $pastor['username'];?></a></p>
                  <div class="progress-bars">
                    <p><?php echo $pastor['aboutme'];?></p>
                     <!-- <div class="progress">
                        <p><?php echo $pastor['location_live'];?></p>
                        <div class="progress-bar" data-value="90"><span>90%</span></div>
                     </div>
                     <div class="progress">
                        <p>Web Designing</p>
                        <div class="progress-bar" data-value="75"><span>75%</span></div>
                     </div> -->
                  </div>
               </div>
            </div>
           </div>



         <?php endforeach;?>



          
        </div>
        </div>   
      </div>
   </div>
   <div class="container mt-4">
  <div class="row">
    <div class="col text-center">
      <a href="<?php echo base_url();?>pastors" class="button btnsecondary wow fadeInUp " data-wow-delay="450ms">See More Pastors</a>
    </div>
  </div>
</div>
    
</section>

<!-- Our Team ends--> 








<!-- Our church-->    
<section id="our-churches" class="padding bglight">
   <div class="container">
      <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <!-- <span>Heros Behind the Company</span> -->
               <h2 class="darkcolor bottom20">Churches</h2>
               <p class="heading_space">A church building or church house, often simply called a church, is a building used for Christian religious activities, particularly for Christian worship services. </p>
            </div>
         </div>
      </div>
      <div class="row">
        <div class="col-md-12">
           <div id="ourchurch-slider" class="owl-carousel">


            <?php

            foreach($churches as $church):

            ?>


           <div class="item">
              <div class="team-box">
               <div class="image">
                  <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" .$church['profile_avatar'] ?>" alt="">
               </div>
               <div class="team-content gradient_bg whitecolor">
                  <h3><a href="<?php echo base_url();?>pages/view/<?php echo $church['ID'];?>" target="_blank"><?php echo $church['name'];?></a></h3>
                  <!-- <p class="bottom40"><a href="<?php echo base_url();?>profile/<?php echo $pastor['username'];?>" target="_blank">@<?php echo $pastor['username'];?></a></p> -->
                  <div class="progress-bars">
                    <p><?php echo $church['description'];?></p>
                     <!-- <div class="progress">
                        <p><?php echo $pastor['location_live'];?></p>
                        <div class="progress-bar" data-value="90"><span>90%</span></div>
                     </div>
                     <div class="progress">
                        <p>Web Designing</p>
                        <div class="progress-bar" data-value="75"><span>75%</span></div>
                     </div> -->
                  </div>
               </div>
            </div>
           </div>

           

         <?php endforeach;?>



          
        </div>
        </div>   
      </div>
   </div>
   <div class="container mt-4">
  <div class="row">
    <div class="col text-center">
      <a href="<?php echo base_url();?>pages" class="button btnsecondary wow fadeInUp " data-wow-delay="450ms">See More Churches</a>
    </div>
  </div>
</div>
    
</section>

<!-- Our Team ends--> 



<!-- Video Click -->  
<!-- <section id="video-bg" class="video-parallax padding_top">
   <div class="container">
      <div class="row">
         <div class="col-md-7 col-sm-7 wow fadeInLeft" data-wow-delay="300ms">
            <div class="heading-title text-md-left text-center padding_bottom">
               <span>Great platform to connect all</span>
               <h2 class="fontregular bottom20 darkcolor"> Connecting People with God’s love</h2>
               <p>Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. </p>
               <a href="<?php echo base_url();?>register" class="button btnprimary top20 pagescroll">Register Now</a>
            </div>
         </div>
         <div class="col-md-5 col-sm-5 padding_bottom wow fadeInRight" data-wow-delay="350ms">
            <div class="image hover-effect">
               <img alt="video img" src="<?php echo base_url();?>frontassets/images/about-img.jpg">
               <a data-fancybox href="https://www.youtube.com/watch?v=GhvD7NtUT-Q&amp;autoplay=1&amp;rel=0&amp;controls=1&amp;showinfo=0" class="button-play fontmedium"><i class="fa fa-play"></i></a>
               <a data-fancybox href="<?php echo base_url();?>frontassets/images/about-img.jpg" class=" fontmedium"></a>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Video Click-->

 <!--Gallery-->
<section id="portfolio-xe" class="bglight padding">
   <div class="container">
    <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <span>Gallery</span>
               <h2 class="darkcolor bottom10">JESUS SLOGANS</h2>
               <!-- <p class="heading_space">Curabitur mollis bibendum luctus. Duis suscipit vitae dui sed suscipit. Vestibulum auctor nunc vitae diam eleifend, in maximus metus sollicitudin. Quisque vitae sodales lectus. Nam porttitor justo sed mi finibus, vel tristique risus faucibus. </p> -->
              <!--  <div id="flat-filters" class="cbp-l-filters dark bottom30">
               <div data-filter="*" class="cbp-filter-item">
                  <span>All</span>
               </div>
               <div data-filter=".web" class="cbp-filter-item">
                  <span>web</span>
               </div>
               <div data-filter=".print" class="cbp-filter-item">
                  <span>Print</span>
               </div>
               <div data-filter=".graphic" class="cbp-filter-item">
                  <span>Graphic</span>
               </div>
               <div data-filter=".logo" class="cbp-filter-item">
                  <span>Logo</span>
               </div>
            </div> -->
            </div>
         </div>
      </div>
      <div id="flat-gallery" class="cbp">
         <div class="cbp-item web logo">
            <img src="<?php echo base_url();?>frontassets/slogans/7.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/7.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
         <div class="cbp-item graphic print">
            <img src="<?php echo base_url();?>frontassets/slogans/2.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/2.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
         <div class="cbp-item logo graphic">
            <img src="<?php echo base_url();?>frontassets/slogans/3.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/3.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
         <div class="cbp-item print">
            <img src="<?php echo base_url();?>frontassets/slogans/4.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/4.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
         <div class="cbp-item logo">
            <img src="<?php echo base_url();?>frontassets/slogans/5.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/5.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
         <div class="cbp-item web print graphic">
            <img src="<?php echo base_url();?>frontassets/slogans/6.jpg" alt="">
            <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="<?php echo base_url();?>frontassets/slogans/6.jpg"></a>
               <!-- <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p> -->
            </div>
         </div>
      </div>
   </div>
</section>
<!--Gallery ends -->         
    
<!-- Mobile Apps -->  
<section id="our-apps" class="padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <span>Yes We Have Mobile App also</span>
               <h2 class="darkcolor heading_space">Our Mobile App Features</h2>
            </div>
         </div>
      </div>
      <div class="row" id="app-feature">
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-left clearfix">
               <div class="feature-item left top30 bottom30 wow fadeInUp" data-wow-delay="300ms">
                  <span class="icon"><i class="fa fa-mobile-phone"></i></span>
                  <div class="text">
                     <h4>Material Design</h4>
                     <p>In app we used material design for good interactive user interface.</p>
                  </div>
               </div>
               <div class="feature-item left top30 bottom30 wow fadeInUp" data-wow-delay="350ms">
                  <span class="icon"><i class="fa fa-cog"></i></span>
                  <div class="text">
                     <h4>Amazing User Settings</h4>
                     <p>User can like,comment and share of each post in the application.</p>
                  </div>
               </div>
               <div class="feature-item left top30 bottom30 wow fadeInUp" data-wow-delay="400ms">
                  <span class="icon"><i class="fa fa-edit"></i></span>
                  <div class="text">
                     <h4>Easy to Customize</h4>
                     <p>User have profile editiong and change password options also.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="image feature-item text-center wow fadeIn" data-wow-delay="500ms">
               <img src="http://www.themesindustry.com/html/xeone/images/responsive.png" alt="">
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-right clearfix">
               <div class="feature-item right top30 bottom30 wow fadeInUp" data-wow-delay="300ms">
                  <span class="icon"><i class="fa fa-code"></i></span>
                  <div class="text">
                     <h4>Powerful BackEnd</h4>
                     <p>We used More Secured Backend technology to secure user and pastor profiles.</p>
                  </div>
               </div>
               <div class="feature-item right top30 bottom30 wow fadeInUp" data-wow-delay="350ms">
                  <span class="icon"><i class="fa fa-folder-o"></i></span>
                  <div class="text">
                     <h4>Anywhere To connect</h4>
                     <p>We have web and android application so you use  anyone.</p>
                  </div>
               </div>
               <div class="feature-item right top30 bottom30 wow fadeInUp" data-wow-delay="400ms">
                  <span class="icon"><i class="fa fa-support"></i></span>
                  <div class="text">
                     <h4>Privacy</h4>
                     <p>We dont share your data with anyone it's our responsibility. </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>                                                                                                                             <!--Mobile Apps ends-->  
        
<!-- Counters -->  
<section id="funfacts" class="padding_top fact-iconic gradient_bg">
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-12 margin_bottom whitecolor text-md-left text-center wow fadeInUp" data-wow-delay="300ms">
            <h3 class="bottom25">Use Way2jesus to make communities</h3>
            <p class="title">Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. </p>
         </div>
         <div class="col-md-7 col-sm-12 text-center">
            <div class="row">
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom wow fadeInUp" data-wow-delay="350ms">
                  <div class="img-icon bottom15">
                     <i class="fa fa-smile-o"></i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="1500" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Satisfied Pastors</p>
               </div>
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom wow fadeInUp" data-wow-delay="400ms">
                  <div class="img-icon bottom15">
                     <i class="fa fa-language"> </i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="1500" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Created Churches</p>
               </div>
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom wow fadeInUp" data-wow-delay="500ms">
                  <div class="img-icon bottom15">
                     <i class="fa fa-desktop"></i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="6000" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Carried out Users</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Counters ends--> 
  
<!-- Pricing Tables -->  
<!-- <section id="our-pricings" class="padding bglight">
   <div class="container">
      <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <span>Choose The Best One</span>
               <h2 class="darkcolor bottom30">Our Packages</h2>
               <p>Curabitur mollis bibendum luctus. Duis suscipit vitae dui sed suscipit. Vestibulum auctor nunc vitae diam eleifend, in maximus metus sollicitudin. Quisque vitae sodales lectus. </p>
            </div>
         </div>
      </div>
      <div class="row three-col-pricing">
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60 wow fadeIn" data-wow-delay="350ms">
               <h3 class="bottom20 darkcolor">Start plan</h3>
               <p>If you are a small business and you are interested in small rebranding then this is a perfect plan for you</p>
               <div class="ammount">
                  <h2 class="defaultcolor"><i class="fa fa-dollar"></i> 9.99 <span class="dur">/ month</span></h2>
               </div>
               <ul class="top20">
                  <li><span>Designing a small brand</span></li>
                  <li><span>Redesign the company logo</span></li>
                  <li><span>Deploying a website</span></li>
                  <li class="not-support"><span>Studio and product photography</span></li>
                  <li class="not-support"><span>Professional project support</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="javascript:void(0)" class="button btnprimary top50">Get Started </a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table active top60 wow fadeIn" data-wow-delay="400ms">
               <h3 class="bottom20 darkcolor">Business plan</h3>
               <p>If you are a small business and you are interested in small rebranding then this is a perfect plan for you</p>
               <div class="ammount">
                  <h2 class="defaultcolor"><i class="fa fa-dollar"></i> 29.99 <span class="dur">/ month</span></h2>
               </div>
               <ul class="top20">
                  <li><span>Designing a small brand</span></li>
                  <li><span>Redesign the company logo</span></li>
                  <li><span>New visual design of the website</span></li>
                  <li><span>Deploying a website</span></li>
                  <li><span>Studio and product photography</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="javascript:void(0)" class="button btnsecondary top50">Get Started </a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60 wow fadeIn" data-wow-delay="450ms">
               <h3 class="bottom20 darkcolor">Business plan</h3>
               <p>If you are a small business and you are interested in small rebranding then this is a perfect plan for you</p>
               <div class="ammount">
                  <h2 class="defaultcolor"><i class="fa fa-dollar"></i> 29.99 <span class="dur">/ month</span></h2>
               </div>
               <ul class="top20">
                  <li><span>Designing a small brand</span></li>
                  <li><span>Redesign the company logo</span></li>
                  <li><span>New visual design of the website</span></li>
                  <li><span>Deploying a website</span></li>
                  <li><span>Studio and product photography</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="javascript:void(0)" class="button btnprimary top50">Get Started </a>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Pricing Tables ends-->  
  
  
<!-- Testimonials -->  
<!-- <section id="our-testimonial" class="padding testimonial-bg">
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-8">
            <div id="testimonial-quote" class="owl-carousel">
               <div class="item">
                  <div class="testimonial-quote whitecolor">
                    <h3 class="bottom30">If you are a small business and you are interested in small rebranding then this is a perfect plan for you</h3>
                     <h6>David Raleway</h6>
                     <small>CEO, XeOne</small>
                  </div>
               </div>
               <div class="item">
                  <div class="testimonial-quote whitecolor">
                    <h3 class="bottom30">If you are a small business and you are interested in small rebranding then this is a perfect plan for you</h3>
                     <h6>Jack Kaclics</h6>
                     <small>Designer, XeOne</small>
                  </div>
               </div>
               <div class="item">
                  <div class="testimonial-quote whitecolor">
                    <h3 class="bottom30">We show you our professional achievements in numbers, which show the acquired skills</h3>
                     <h6>Stephen Jhon</h6>
                     <small>CEO, XeOne</small>
                  </div>
               </div>
            </div>
            <div id="owl-thumbs" class="owl-dots">
               <div class="owl-dot active"><img src="http://www.themesindustry.com/html/xeone/images/testimonial-1.jpg" alt=""></div>
               <div class="owl-dot"><img src="http://www.themesindustry.com/html/xeone/images/testimonial-2.jpg" alt=""></div>
               <div class="owl-dot"><img src="http://www.themesindustry.com/html/xeone/images/testimonial-3.jpg" alt=""></div>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Testimonials Ends-->

<!-- Partners -->  
<!-- <section id="logos" class="padding">
   <div class="container">
     <h3 class="invisible">hidden</h3>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div id="partners-slider" class="owl-carousel">
               <div class="item">
                  <div class="logo-item"> <img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-1.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-2.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-3.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-4.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-5.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-1.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-2.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-3.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-4.png"></div>
               </div>
               <div class="item">
                  <div class="logo-item"><img alt="" src="http://www.themesindustry.com/html/xeone/images/logo-5.png"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Partners Ends-->


<!-- Our Blogs -->  
<!-- <section id="our-blog" class="half-section">
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-12">
            <div class="image hover-effect"><img src="http://www.themesindustry.com/html/xeone/images/split-blog.jpg" alt="our blog" class="equalheight"></div>
         </div>
         <div class="col-md-7 col-sm-12">
            <div class="split-box center-block equalheight container-padding">
               <div class="heading-title padding_half">
               <span class="wow fadeIn" data-wow-delay="300ms">Read Our News</span>
               <h2 class="darkcolor bottom25 wow fadeIn" data-wow-delay="350ms">Latest Blog Post</h2>
               <p class="bottom30 wow fadeInUp" data-wow-delay="400ms">Curabitur mollis bibendum luctus. Duis suscipit vitae dui sed suscipit. Vestibulum auctor nunc vitae diam eleifend, in maximus metus sollicitudin. Quisque vitae sodales lectus. Nam porttitor justo sed mi finibus, vel tristique risus faucibus. </p>
               <a href="http://www.themesindustry.com/html/xeone/blog.html" class="button btnsecondary wow fadeInUp" data-wow-delay="450ms">Read Full Story</a>
            </div>
            </div>
         </div>
      </div>
   </div>
</section> -->
<!--Our Blogs Ends-->
     
     
<!-- Contact US -->  
<section id="contactus" class="padding_top">
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
           <div class="heading-title heading_space wow fadeInUp" data-wow-delay="300ms">
               <span>Lets Get In Touch</span>
               <h2 class="darkcolor">Contact Us</h2>
            </div>
        </div>
         <div class="col-md-6 col-sm-12 margin_bottom wow fadeInUp" data-wow-delay="350ms">
            <p>West is not just about graphic design; it's more than that. We offer integral communication services, and we're responsible for our process and results. We thank each client and their projects.</p>
            <div class="row">
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Address</h5>
                  <p class="bottom15">1st Floor,Aditya Trade Center,<br>
Near Mythrivanam Hyderabad -<br>
500 084, Telangana </p>
                  <a class="pickus" href="#." data-text="Get Directions">Get Directions</a>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Phone</h5>
                  <p class="bottom15">Office Telephone : 040-48521656 <span class="block">
                     Mobile : +91 9505821934
                  </span> </p>
                  <a class="pickus" href="tel:9505821934" data-text="Call Us">Call Us</a>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Email</h5>
                  <p class="bottom15">Main Email : way2jesusorg@gmail.com <span class="block">Inquiries :  way2jesusorg@gmail.com</span> </p>
                  <a class="pickus" href="mailto:way2jesusorg@gmail.com" data-text="Send a Message">Send a Message</a>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Support</h5>
                  <p class="bottom15">Main Support : UBN QTech <span>help : support@ubnqtech</span> </p>
                  <a class="pickus" href="mailto:ubntech@gmail.com" data-text="Open a Ticket">Open a Ticket</a>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-12 margin_bottom">
            <form class="getin_form wow fadeInUp" data-wow-delay="400ms" onsubmit="return false;">
               <div class="row">

                  <div class="col-sm-12" id="result"></div>

                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="text" placeholder="First Name:" required id="first_name" name="first_name">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="text" placeholder="Last Name:" required id="last_name" name="last_name">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="email" placeholder="Email:" required id="email" name="email">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="tel" placeholder="Phone:" id="phone" name="phone">
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12">
                     <div class="form-group bottom35">
                        <textarea class="form-control" placeholder="Message" id="message" name="message"></textarea>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <button type="submit" class="button btnprimary" id="submit_btn">submit request</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   
   <!--Location Map here-->
<!-- <div id="map-container"></div>    -->
</section>
<!--Contact US Ends-->                    
      
<!--Site Footer Here-->
<footer id="site-footer" class="padding_half">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <ul class="social-icons bottom25 wow fadeInUp" data-wow-delay="300ms">
               <li><a href="javascript:void(0)" target="_blank"><i class="fa fa-facebook"></i> </a> </li>
               <li><a href="https://twitter.com/way2jesusorg" target="_blank"><i class="fa fa-twitter"></i> </a> </li>
               <li><a href="https://plus.google.com/u/0/108451439509347706392?tab=mX" target="_blank"><i class="fa fa-google-plus"></i> </a> </li>
               <li><a href="https://www.linkedin.com/feed/?trk=onboarding-landing" target="_blank"><i class="fa fa-linkedin"></i> </a> </li>
               <li><a href="https://plus.google.com/u/0/108451439509347706392?tab=mX" target="_blank"><i class="fa fa-instagram"></i> </a> </li>
               <li><a href="mailto:way2jesusorg@gmail.com" target="_blank"><i class="fa fa-envelope-o"></i> </a> </li>
            </ul>
            <p class="copyrights wow fadeInUp" data-wow-delay="350ms"> &copy; 2019 Way2Jesus. made with love by <a href="http://ubnqtech.com/" target="_blank">UBN QTech Solutions</a> </p>
         </div>
      </div>
   </div>
</footer>
<!--Footer ends-->   



<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url();?>front/js/jquery-3.1.1.min.js"></script>

<!--Bootstrap Core-->
<script src="<?php echo base_url();?>front/js/popper.min.js"></script>
<script src="<?php echo base_url();?>front/js/bootstrap.min.js"></script>

<!--to view items on reach-->
<script src="<?php echo base_url();?>front/js/jquery.appear.js"></script>

<!--Equal-Heights-->
<script src="<?php echo base_url();?>front/js/jquery.matchHeight-min.js"></script>

<!--Owl Slider-->
<script src="<?php echo base_url();?>front/js/owl.carousel.min.js"></script>

<!--number counters-->
<script src="<?php echo base_url();?>front/js/jquery-countTo.js"></script>
 
<!--Parallax Background-->
<script src="<?php echo base_url();?>front/js/parallaxie.js"></script>
  
<!--Cubefolio Gallery-->
<script src="<?php echo base_url();?>front/js/jquery.cubeportfolio.min.js"></script>

<!--FancyBox popup-->
<script src="<?php echo base_url();?>front/js/jquery.fancybox.min.js"></script>          

<!-- Video Background-->
<script src="<?php echo base_url();?>front/js/jquery.background-video.js"></script>

<!--TypeWriter-->
<script src="<?php echo base_url();?>front/js/typewriter.js"></script> 
      
<!--Particles-->
<script src="<?php echo base_url();?>front/js/particles.min.js"></script>            
        
<!--WOw animations-->
<script src="<?php echo base_url();?>front/js/wow.min.js"></script>
            
<!--Revolution SLider-->
<script src="<?php echo base_url();?>front/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>front/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="<?php echo base_url();?>front/js/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.carousel.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url();?>front/js/revolution.extension.video.min.js"></script>

<!--Google Map API-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJnKEvlwpyjXfS_h-J1Cne2fPMqeb44Mk"></script>
<script src="<?php echo base_url();?>front/js/functions.js"></script>

</body>


</html>