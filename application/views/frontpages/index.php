<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from kptkiran87.in/stromi/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Mar 2019 10:27:39 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible"  content="IE=edge">
<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> - <?php endif; ?><?php echo $this->settings->info->site_name ?></title>
<meta name="keywords" content="" />
<meta name="description" content="">
<meta name="author" content="">

<!-- Mobile view -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo base_url();?>frontassets/images/favicon.html">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontassets/js/bootstrap/bootstrap.min.css">

<!-- Google fonts  -->
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<!-- Template's stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/js/megamenu/stylesheets/screen.css">
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/js/megamenu/stylesheets/onepage.css">
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/js/loaders/stylesheets/screen.css">
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/css/default.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/css/corporate.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>frontassets/css/snow.css">
<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" type="text/css" href="fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="fonts/et-line-font/et-line-font.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontassets/js/revolution-slider/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontassets/js/revolution-slider/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontassets/js/revolution-slider/css/navigation.css">
<link href="<?php echo base_url();?>frontassets/js/accordion/css/smk-accordion.css" rel="stylesheet">
<link href="<?php echo base_url();?>frontassets/js/accordion2/css/accordion.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontassets/js/cubeportfolio/cubeportfolio.min.css">
<!-- Template's stylesheets END -->

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<style>
/*body {margin:0;height:2000px;}

.icon-bar {
  position: fixed;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}*/

/* Sticky Social Icons */
.sticky-container{ padding:0px; margin:0px; position:fixed; right:-130px;top:230px; width:210px; z-index: 1100;}
.sticky li{list-style-type:none;background-color:#fff;color:#efefef;height:43px;padding:0px;margin:0px 0px 1px 0px; -webkit-transition:all 0.25s ease-in-out;-moz-transition:all 0.25s ease-in-out;-o-transition:all 0.25s ease-in-out; transition:all 0.25s ease-in-out; cursor:pointer;}
.sticky li:hover{margin-left:-115px;}
.sticky li img{float:left;margin:5px 4px;margin-right:5px;}
.sticky li p{padding-top:5px;margin:0px;line-height:16px; font-size:11px;}
.sticky li p a{ text-decoration:none; color:#2C3539;}
.sticky li p a:hover{text-decoration:underline;}
/* Sticky Social Icons */
.navbar{
margin-bottom: 0px;
}
.navbar-inverse .navbar-nav > li > a{
  color: #fff;
}

.navbar-header{
  /*width: 200px !important;*/
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
</head>

<body>



<!--   <div class="icon-bar">
  <a href="#" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a> 
  <a href="https://twitter.com/way2jesusorg" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a> 
  <a href="https://www.instagram.com/way2jesusorg/" class="twitter" target="_blank"><i class="fa fa-instagram"></i></a> 
  <a href="https://plus.google.com/u/0/108451439509347706392?tab=mX" class="google" target="_blank"><i class="fa fa-google"></i></a> 
  <a href="https://www.linkedin.com/feed/?trk=onboarding-landing" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
  <a href="#" class="youtube" target="_blank"><i class="fa fa-youtube"></i></a> 
</div> -->


<div class="sticky-container">
    <ul class="sticky">

      <li>
        <img src="<?php echo base_url();?>frontassets/images/call.png" width="32" height="32">
        <p><a href="tel: 9505821934" target="_blank">Call Us<br>Mobile</a></p>
      </li>


      <li>
        <img src="<?php echo base_url();?>frontassets/images/mail.png" width="32" height="32">
        <p><a href="mailto:way2jesusorg@gmail.com" target="_blank">Email Us on<br>Gmail</a></p>
      </li>


      <li>
        <img src="<?php echo base_url();?>frontassets/images/facebook-circle.png" width="32" height="32">
        <p><a href="#" target="_blank">Like Us on<br>Facebook</a></p>
      </li>
      <li>
        <img src="<?php echo base_url();?>frontassets/images/twitter-circle.png" width="32" height="32">
        <p><a href="https://twitter.com/way2jesusorg" target="_blank">Follow Us on<br>Twitter</a></p>
      </li>
      <li>
        <img src="<?php echo base_url();?>frontassets/images/gplus-circle.png" width="32" height="32">
        <p><a href="https://plus.google.com/u/0/108451439509347706392?tab=mX" target="_blank">Follow Us on<br>Google+</a></p>
      </li>
      <li>
        <img src="<?php echo base_url();?>frontassets/images/instagram-logo-png-small.png" width="32" height="32">
        <p><a href="https://plus.google.com/u/0/108451439509347706392?tab=mX" target="_blank">Follow Us on<br>Instagram</a></p>
      </li>

      <li>
        <img src="<?php echo base_url();?>frontassets/images/linkedin-circle.png" width="32" height="32">
        <p><a href="https://www.linkedin.com/feed/?trk=onboarding-landing" target="_blank">Follow Us on<br>LinkedIn</a></p>
      </li>


      

     <!--  <li>
        <img src="<?php echo base_url();?>frontassets/images/youtube-circle.png" width="32" height="32">
        <p><a href="#" target="_blank">Subscribe on<br>YouYube</a></p>
      </li>
      <li>
        <img src="<?php echo base_url();?>frontassets/images/pin-circle.png" width="32" height="32">
        <p><a href="#" target="_blank">Follow Us on<br>Pinterest</a></p>
      </li> -->
    </ul>
  </div>


<div class="over-loader loader-live">
  <div class="loader">
    <div class="loader-item style7">
      <img src="<?php echo base_url();?>frontassets/images/cross-animated-gif-19.gif">
      <!-- <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div> -->
    </div>
  </div>
</div>
<!-- end loading-->
<div id="section1"></div>
<!-- menu scroll target link -->

<div class="site-wrapper">
  <div class="col-md-12 nopadding">
    <div class="header-section style1 no-bg fn-mar pin-style">
      <div class="container-filid">

              
              
<nav class="navbar navbar-inverse" style="background-color: #1abc9c;color:#fff !important">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>">
            <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>" width="50px" alt="" class="pull-left hidden-xs">
            <span style="font-size: 25px; color:#fff;"><?php echo $this->settings->info->site_name ?></span></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse main-nav" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right pt-serif" style="color: #fff !important">
            <li class="mega-menu"><a href="#section1" title="">Home</a></li>
                    <li><a href="#section2" title="">About</a></li>
                
                    <!-- <li><a href="#section4" title="">Gallery</a></li> -->
                    
                    <li><a href="#section6" title="">Slogans</a></li>
                    
                    <li><a href="#section9" title="">Contact</a></li>

                    <li><a href="<?php echo base_url();?>login" title=""><i class="fa fa-sign-in"></i> Login</a></li>

                    <li><a href="<?php echo base_url();?>register" title=""><i class="fa fa-user-plus"></i> Register</a></li>


            
          </ul>
          
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->


            <!-- <div class="mod-menu">
          <div class="row nopadding">
            <div class="col-sm-3"> 
              <a href="<?php echo base_url();?>" title="" class="logo style-2 mar-4"> <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>" width="50px" alt=""><span style="font-size: 35px"><?php echo $this->settings->info->site_name ?></span> </a>
              

               </div>
            <div class="col-sm-9">
              <div class="main-nav">
                <ul class="nav navbar-nav top-nav">
                  <li class="visible-xs menu-icon"> <a href="javascript:void(0)" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false"> <i aria-hidden="true" class="fa fa-bars"></i> </a> </li>
                </ul>
                <div id="menu" class="collapse">
                  <ul class="nav navbar-nav pt-serif">
                    <li class="mega-menu"><a href="#section1" title="">Home</a></li>
                    <li><a href="#section2" title="">About</a></li>
                    <li><a href="#section3" title="">Services</a></li>
                    <li><a href="#section4" title="">Team</a></li>
                    
                    <li><a href="#section6" title="">Portfolio</a></li>
                    
                    <li><a href="#section9" title="">Contact</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <!--end menu--> 
    
  </div>
  <!--end menu-->
  
  <div class="clearfix"></div>
  
  <!-- START REVOLUTION SLIDER 5.0 -->
  <div class="slide-tmargin">
    <div class="slidermaxwidth">
      <div class="rev_slider_wrapper"> 
        <!-- START REVOLUTION SLIDER 5.0 auto mode -->
        <div id="rev_slider" class="rev_slider"  data-version="5.0">
          <ul>
            
            <!-- SLIDE  -->
            <li data-index="rs-1" data-transition="slideright"> 
              <!-- MAIN IMAGE --> 
              <img src="<?php echo base_url();?>frontassets/images/slider2.jpg" alt="" data-bgfit="cover">
              <div class="banner_section">
                <div class="snow-container">
                  <div class="snow foreground"></div>
                  <div class="snow foreground layered"></div>
                  <div class="snow middleground"></div>
                  <div class="snow middleground layered"></div>
                  <div class="snow background"></div>
                  <div class="snow background layered"></div>
                </div>
                
                
                <!-- LAYER NR. 2 -->
                <div class="tp-caption Gym-Display fweight-9 white tp-resizeme" 
                     id="slide-1-layer-2" 
                     data-x="['left','left','left','left']" data-hoffset="['40','10','10','10']" 
                     data-y="['top','top','top','top']" data-voffset="['380','318','190','110']" 
                                data-fontsize="['55','50','40','27']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
         
                    data-type="text" 
                    data-responsive_offset="on" 
        
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                    data-textAlign="['left','left','left','left']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
        
                    style="z-index: 7; white-space: nowrap;"> To find God <span class="text-primary">Way2jesus </span> </div>
                
                <!-- LAYER NR. 3 -->
                <div class="tp-caption Gym-Display fweight-10 white tp-resizeme" 
                     id="slide-1-layer-3" 
                     data-x="['left','left','left','left']" data-hoffset="['40','10','10','10']" 
                     data-y="['top','top','top','top']" data-voffset="['440','358','228','145']"  
                                data-fontsize="['55','40','40','27']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
         
                    data-type="text" 
                    data-responsive_offset="on" 
        
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                    data-textAlign="['left','left','left','left']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
        
                    style="z-index: 7; white-space: nowrap;">Connecting People with  <span class="text-primary"> God’s  </span>love </div>
                
                <!-- LAYER NR. 4 -->
                <div class="tp-caption"
                    id="slide-1-layer-4"
                    data-x="['left','left','left','left']" data-hoffset="['50','50','10','10']" 
                    data-y="['top','top','top','top']" data-voffset="['550','440','310','220']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","speed":1000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":700,"to":"y:[175%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power2.easeOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
            data-textAlign="['left','left','left','left']"
                    
                    style="z-index: 6"> <a rel="nofollow" href="<?php echo base_url();?>login" class="animated-button thar-one uppercase font-weight-7 lspace-6">Login</a> </div>
                
                <!-- LAYER NR. 5 -->
                <div class="tp-caption"
                    id="slide-1-layer-5"
                    data-x="['left','left','left','left']" data-hoffset="['225','50','120','120']" 
                    data-y="['top','top','top','top']" data-voffset="['550','440','310','220']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","speed":1000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":700,"to":"y:[175%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"},{"frame":"hover","speed":"300","ease":"Power2.easeOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
            data-textAlign="['left','left','left','left']"
                    
                    style="z-index: 6"> <a rel="nofollow" href="<?php echo base_url();?>register" class="animated-button thar-two uppercase font-weight-7 lspace-6">Register</a> </div>
              </div>
            </li>
            
           
          </ul>
          <!-- END REVOLUTION SLIDER --> 
        </div>
      </div>
    </div>
    <!-- END REVOLUTION SLIDER WRAPPER --> 
  </div>
  <div class="clearfix"></div>
  <!-- END OF SLIDER WRAPPER -->
  
  <div id="section2"></div> <!-- menu scroll target link -->
  <div class="sec-paddingmore">
    <div class="container">
      <div class="row no-gutter">
        <div class="col-md-12 text-center">
          <h4 class="text-primary font-weight_06 nomargin">About us</h4>
          <h2 class="font-weight_09"> Who we are </h2>
          <div class="titel-line"></div>
        </div>
        <div class="clearfix margin-04"></div>
        <div class="col-md-6 col-sm-6">
          <div class="feature-box01 text-center">
            <img src="<?php echo base_url();?>frontassets/images/about-img.jpg">
          </div>
        </div>
        <!--end item-->
        
        <div class="col-md-6 col-sm-6">
          <div class="feature-box01 text-center">
           
            <h4 class="font-weight_06 tpadding-02"> Way To Jesus
              <small>
Connecting People with God’s love</small></h4>
<div>
            <p class="text-justify">Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. When a user sign-ups as the pastor he can create and post and share all the updates about his work, teachings etc in his pastor profile. </p>
            <p class="text-justify">So our way2jesus platform is giving an opportunity to the pastors and churches to promote and share their works on a dedicated platform for Christianity where a normal user can view all the information about the churches and pastors around him based on his or her search. </p>
            <p class="text-justify">Way2jesus brings and unites all the churches and pastors at a single platform where they can share their prayers, announcements and other information about them with the users. . So we can say our Way2jesus is the complete dedicated platform to spread the word of Christianity where pastors and churches can connect with the people. </p>
          </div>
          </div>
        </div>
        <!--end item-->
        
       
      </div>
    </div>
  </div>
  <div class="clearfix"></div>

  

  <div class=" bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-md-12 sec-padding_02 ">
          <div class="container">
            <div class="col-md-9 col-xs-12">
              <h1 class="text-white roboto nomargin font-weight_09">Use Way2jesus to make communities</h1>
              <h6 class="text-white font-weight_05"> Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. </h6>
            </div>
            <div class="col-md-3 col-xs-12">
              <div class="clearfix tmargin-03"></div>
              <a href="<?php echo base_url();?>register" class="but_02 font-weight_07 uppercase">Register Here</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  
  

  
  <div id="section6"></div> <!-- menu scroll target link -->
  
  <div class="sec-paddingmore-2">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h4 class="text-primary font-weight_06 nomargin"> JESUS SLOGANS </h4>
          <!-- <h2 class="font-weight_09"> Latest Projects </h2> -->
          <div class="titel-line"></div>
        </div>
        <div class="clearfix margin-04"></div>
        <div>
          <!-- <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter">
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> All
              <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".print" class="cbp-filter-item"> Graphic
              <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".web-design" class="cbp-filter-item"> Branding
              <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".graphic" class="cbp-filter-item"> Products
              <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".motion" class="cbp-filter-item"> Photography
              <div class="cbp-filter-counter"></div>
            </div>
          </div> -->
          <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">



<?php
$files = glob("frontassets/slogans/*.*");
// count($files)
for ($i=1; $i<=12; $i++)
{
    $num = $files[$i];
    // echo '<img src="'.$num.'" alt="random image">'."&nbsp;&nbsp;";

    ?>


   <div class="cbp-item web-design graphic zoomimg5" style="width: 25% !important; height:100px !important;">  <a href="<?php echo base_url();?><?php echo $num;?>" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>by Tiberiu Neamu">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo base_url();?><?php echo $num;?>" style=" height:250px !important;" class="zoomimg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                
              </div>
              </a> </div>

              <?php

}



?>

            


          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  
  
 
  
  <div id="section9"></div><!-- menu scroll target link -->
  
  <div class="sec-paddingmore-2 bg-dark">
    <div class="container">
      <div class="row">


         <div class="col-md-3 col-sm-6">
           <h4 class="uppercase footer-title text-white font-weight_06">About us</h4>
          <div class="fo-map">
            <!-- <div class="footer-logo"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>" width="50px" alt=""/></div> -->
           
           <p class="text-white"> Way2jesus is a unique platform where a user can get all the information and updates about churches and pastors around him at one place. When a user sign-ups as the pastor he can create and post and share all the updates about his work, teachings etc in his pastor profile. </p>
</div>
        </div>
        <!--end item-->



        <div class="col-md-3 col-sm-6">
          <h4 class="uppercase footer-title text-white font-weight_06">Address</h4>
          <div class="fo-map">
            <!-- <div class="footer-logo"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>" width="50px" alt=""/></div> -->
           
            <address class="text-white">
           
            1st Floor,Aditya Trade Center, <br>Near Mythrivanam
Hyderabad -<br> 500 084,
Telangana
</address>
</div>
        </div>
        <!--end item-->
        
        <div class="col-md-3 col-sm-6">
          <h4 class="uppercase footer-title text-white font-weight_06">Phone Number</h4>
          <div class="clearfix tmargin-03"></div>
          <p class="text-white">+91 9505821934, 040-48521656</p>
        </div>
        <!--end item-->
        

        
        <div class="col-md-3 col-sm-6">
          <h4 class="uppercase footer-title text-white font-weight_06">Email</h4>
          <div class="clearfix tmargin-03"></div>
          <p><a href="mailto:way2jesusorg@gmail.com" class="opacity-link-light text-white"><i class="glyphicon glyphicon-envelope"></i> way2jesusorg@gmail.com</a></p>
          


        </div>
        <!--end item--> 
        
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="copyrights ">
    <div class="container text-center"><span>Copyright © 2019<br/>
      Design & Developed by UBN Qtech</span></div>
  </div>
  <!--end copyrights-->
  <div class="clearfix"></div>
  <!-- end section --> 
  
  <a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page--> 
  
</div>

<!-- Scripts --> 
<script src="<?php echo base_url();?>frontassets/js/jquery/jquery.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/bootstrap/bootstrap.min.js"></script> 
<!-- Scripts END --> 

<!-- Template scripts --> 
<script src="<?php echo base_url();?>frontassets/js/megamenu/js/main.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/megamenu/js/onepage.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/accordion/js/smk-accordion.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/accordion/js/custom.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/accordion2/js/accordion.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/cubeportfolio/jquery.cubeportfolio.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/cubeportfolio/mosaic-flat.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/functions/functions.js"></script>

<!-- Scripts END --> 

<!-- REVOLUTION JS FILES --> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script> 
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
(Load Extensions only on Local File Systems ! 
The following part can be removed on Server for On Demand Loading) -->
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script> 
<script src="<?php echo base_url();?>frontassets/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script> 
<script>
	var tpj=jQuery;			
	var revapi4;
	tpj(document).ready(function() {
	if(tpj("#rev_slider").revolution == undefined){
	revslider_showDoubleJqueryError("#rev_slider");
	}else{
		revapi4 = tpj("#rev_slider").show().revolution({
		sliderType:"standard",
		jsFileLocation:"js/revolution-slider/js/",
		sliderLayout:"auto",
		dottedOverlay:"none",
		delay:9000,
		navigation: {
		keyboardNavigation:"off",
		keyboard_direction: "horizontal",
		mouseScrollNavigation:"off",
		onHoverStop:"off",
		arrows: {
		style:"uranus",
		enable:true,
		hide_onmobile:false,
		hide_under:100,
		hide_onleave:true,
		hide_delay:200,
		hide_delay_mobile:1200,
		tmp:'',
		left: {
		h_align:"left",
		v_align:"center",
		h_offset:35,
		v_offset:0
		},
		right: {
		h_align:"right",
		v_align:"center",
		h_offset:35,
		v_offset:0
		}
		}
		,
		touch:{
		touchenabled:"on",
		swipe_threshold: 75,
		swipe_min_touches: 1,
		swipe_direction: "horizontal",
		drag_block_vertical: false
	}
	,
										
										
										
	},
		viewPort: {
		enable:true,
		outof:"pause",
		visible_area:"80%"
	},
	
	responsiveLevels:[1240,1024,778,480],
	gridwidth:[1240,1024,778,480],
	 gridheight: [900,750,500,360],
	lazyType:"smart",
		parallax: {
		type:"mouse",
		origo:"slidercenter",
		speed:2000,
		levels:[2,3,4,5,6,7,12,16,10,50],
		},
	shadow:0,
	spinner:"off",
	stopLoop:"off",
	stopAfterLoops:-1,
	stopAtSlide:-1,
	shuffle:"off",
	autoHeight:"off",
	hideThumbsOnMobile:"off",
	hideSliderAtLimit:0,
	hideCaptionAtLimit:0,
	hideAllCaptionAtLilmit:0,
	disableProgressBar:"on",
	debugMode:false,
		fallbacks: {
		simplifyAll:"off",
		nextSlideOnWindowFocus:"off",
		disableFocusListener:false,
		}
	});
	}
	});	/*ready*/
</script> 

<script>
    $(window).load(function(){
      setTimeout(function(){

        $('.loader-live').fadeOut();
      },1000);
    })

</script> 
<script>
    $(window).load(function(){
      setTimeout(function(){

        $('.loader-live').fadeOut();
      },1000);
    })

</script> 
</body>

</html>
