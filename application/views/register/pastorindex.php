 <div class="col-md-3 register-left">
                        <a href="<?php echo base_url();?>">
                            <!-- <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>" alt=""/> -->
                            <img src="<?php echo base_url();?>front/images/icon.png" alt="" width="200px"/>
                        </a>
                        <h3><a href="<?php echo base_url();?>" class="text-white btn-link">Way2Jesus</a></h3>
                        <p><?php echo lang("ctn_212") ?> <?php echo $this->settings->info->site_name ?></p>
                      
                        <a href="<?php echo site_url("login") ?>" class="btn btn-warning form-control" ><?php echo lang("ctn_223") ?></a>
                        <br/>
                    </div>
                    <div class="col-md-9 register-right">

                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " id="home-tab"  href="<?php echo base_url();?>register/index"  >User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pastor</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <!--  <div class="tab-pane " id="home" role="tabpanel" aria-labelledby="home-tab">
                            	 <?php if(!empty($fail)) : ?>
					                <div class="alert alert-danger"><?php echo $fail ?></div>
					            <?php endif; ?>

                                <h3 class="register-heading">Register as a User</h3>
                                <?php echo form_open(site_url("register"), array("class" => "form-horizontal")) ?>
                                <input type="hidden" name="user_role" value="7">





                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email-in" name="email" value="<?php if(isset($email)) echo $email; ?>" placeholder="Your Email *">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="username" name="username" value="<?php if(isset($username)) echo $username; ?>" placeholder="Username *">
                            <div id="username_check"></div>
                                        </div>
                                        <div class="form-group">
                                            
                                             <input type="password" class="form-control" id="password-in" name="password" value="" placeholder="Password *">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="cpassword-in" name="password2" value="" placeholder="Confirm Password **">
                                        </div>

                                        <div class="form-group">
                                             <input type="text" class="form-control" id="name-in" name="first_name" value="<?php if(isset($first_name)) echo $first_name ?>" placeholder="First Name *">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name-in" name="last_name" value="<?php if(isset($last_name)) echo $last_name ?>" placeholder="Last Name *">
                                        </div>









                                        <?php foreach($fields->result() as $r) : ?>
                    <div class="form-group">

                        <label for="name-in" class="col-md-12 label-heading"><?php echo $r->name ?> <?php if($r->required) : ?>*<?php endif; ?></label>
                        <div class="col-md-12">
                            <?php if($r->type == 0) : ?>
                                <input type="text" class="form-control" id="name-in" name="cf_<?php echo $r->ID ?>" value="<?php if(isset($_POST['cf_'. $r->ID])) echo $_POST['cf_' . $r->ID] ?>">
                            <?php elseif($r->type == 1) : ?>
                                <textarea name="cf_<?php echo $r->ID ?>" rows="8" class="form-control"><?php if(isset($_POST['cf_'. $r->ID])) echo $_POST['cf_' . $r->ID] ?></textarea>
                            <?php elseif($r->type == 2) : ?>
                                 <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <?php foreach($options as $k=>$v) : ?>
                                    <div class="form-group"><input type="checkbox" name="cf_cb_<?php echo $r->ID ?>_<?php echo $k ?>" value="1" <?php if(isset($_POST['cf_cb_' . $r->ID . "_" . $k])) echo "checked" ?>> <?php echo $v ?></div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php elseif($r->type == 3) : ?>
                                <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <?php foreach($options as $k=>$v) : ?>
                                    <div class="form-group"><input type="radio" name="cf_radio_<?php echo $r->ID ?>" value="<?php echo $k ?>" <?php if(isset($_POST['cf_radio_' . $r->ID]) && $_POST['cf_radio_' . $r->ID] == $k) echo "checked" ?>> <?php echo $v ?></div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php elseif($r->type == 4) : ?>
                                <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <select name="cf_<?php echo $r->ID ?>" class="form-control">
                                    <?php foreach($options as $k=>$v) : ?>
                                    <option value="<?php echo $k ?>" <?php if(isset($_POST['cf_' . $r->ID]) && $_POST['cf_'.$r->ID] == $k) echo "selected" ?>><?php echo $v ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>
                            <?php endif; ?>
                            <span class="help-text"><?php echo $r->help_text ?></span>
                        </div>
                </div>
                <?php endforeach; ?>
                <p><?php echo lang("ctn_351") ?></p>

                <?php if(!$this->settings->info->disable_captcha) : ?>
                <div class="form-group">

                    <label for="name-in" class="col-md-3 label-heading"><?php echo lang("ctn_220") ?></label>
                    <div class="col-md-9">
                        <p><?php echo $cap['image'] ?></p>
                        <input type="text" class="form-control" id="captcha-in" name="captcha" placeholder="<?php echo lang("ctn_306") ?>" value="">
                    </div>
                </div>
                <?php endif; ?>
                <?php if($this->settings->info->google_recaptcha) : ?>
                    <div class="form-group">

                    <label for="name-in" class="col-md-3 label-heading"><?php echo lang("ctn_220") ?></label>
                    <div class="col-md-9">
                        <div class="g-recaptcha" data-sitekey="<?php echo $this->settings->info->google_recaptcha_key ?>"></div>
                    </div>
                </div>
                <?php endif ?>











                
                                        <input type="submit" name="s" class="btn btn-primary form-control" value="<?php echo lang("ctn_221") ?>" />


                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php echo lang("ctn_222") ?></p>

                          <?php if(!$this->settings->info->disable_social_login) : ?>
<div class="text-center decent-margin-top">

  <a href="<?php echo site_url("login/twitter_login") ?>" class="btn btn-success btn-block" >
    <img src="<?php echo base_url() ?>images/social/twitter.png" height="20" class='social-icon' />
   Twitter</a>



  <a href="<?php echo site_url("login/facebook_login") ?>" class="btn btn-info btn-block" >
    <img src="<?php echo base_url() ?>images/social/facebook.png" height="20" class='social-icon' />
   Facebook</a>



  <a href="<?php echo site_url("login/google_login") ?>" class="btn btn-warning btn-block" >
    <img src="<?php echo base_url() ?>images/social/google.png" height="20" class='social-icon' />
   Google</a>

</div>
<?php endif; ?>
<br>    
<a href="<?php echo site_url("login") ?>" class="btn btn-primary form-control" ><?php echo lang("ctn_223") ?></a>
                
                                        
                                    </div>
                                </div>
                                <?php echo form_close() ?>
                            </div> -->






















                            <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                     <?php if(!empty($fail)) : ?>
                                    <div class="alert alert-danger"><?php echo $fail ?></div>
                                <?php endif; ?>

                                <h3 class="register-heading">Register as a Pastor</h3>
                                <?php echo form_open(site_url("register/pastorindex"), array("class" => "form-horizontal")) ?>
                                <input type="hidden" name="user_role" value="5">





                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email-in" name="email" value="<?php if(isset($email)) echo $email; ?>" placeholder="Your Email *">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php if(isset($mobile)) echo $mobile; ?>" placeholder="Mobile Number *">
                                         <div id="mobile_check"></div>
                                     </div>


                                        <div class="form-group">
                                            <input type="text" class="form-control" id="username" name="username" value="<?php if(isset($username)) echo $username; ?>" placeholder="Username *">
                            <div id="username_check"></div>
                                        </div>
                                        <div class="form-group">
                                            
                                             <input type="password" class="form-control" id="password-in" name="password" value="" placeholder="Password *">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="cpassword-in" name="password2" value="" placeholder="Confirm Password **">
                                        </div>

                                        <div class="form-group">
                                             <input type="text" class="form-control" id="name-in" name="first_name" value="<?php if(isset($first_name)) echo $first_name ?>" placeholder="First Name *">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name-in" name="last_name" value="<?php if(isset($last_name)) echo $last_name ?>" placeholder="Last Name *">
                                        </div>









                                        <?php foreach($fields->result() as $r) : ?>
                    <div class="form-group">

                        <label for="name-in" class="col-md-12 label-heading"><?php echo $r->name ?> <?php if($r->required) : ?>*<?php endif; ?></label>
                        <div class="col-md-12">
                            <?php if($r->type == 0) : ?>
                                <input type="text" class="form-control" id="name-in" name="cf_<?php echo $r->ID ?>" value="<?php if(isset($_POST['cf_'. $r->ID])) echo $_POST['cf_' . $r->ID] ?>">
                            <?php elseif($r->type == 1) : ?>
                                <textarea name="cf_<?php echo $r->ID ?>" rows="8" class="form-control"><?php if(isset($_POST['cf_'. $r->ID])) echo $_POST['cf_' . $r->ID] ?></textarea>
                            <?php elseif($r->type == 2) : ?>
                                 <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <?php foreach($options as $k=>$v) : ?>
                                    <div class="form-group"><input type="checkbox" name="cf_cb_<?php echo $r->ID ?>_<?php echo $k ?>" value="1" <?php if(isset($_POST['cf_cb_' . $r->ID . "_" . $k])) echo "checked" ?>> <?php echo $v ?></div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php elseif($r->type == 3) : ?>
                                <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <?php foreach($options as $k=>$v) : ?>
                                    <div class="form-group"><input type="radio" name="cf_radio_<?php echo $r->ID ?>" value="<?php echo $k ?>" <?php if(isset($_POST['cf_radio_' . $r->ID]) && $_POST['cf_radio_' . $r->ID] == $k) echo "checked" ?>> <?php echo $v ?></div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php elseif($r->type == 4) : ?>
                                <?php $options = explode(",", $r->options); ?>
                                <?php if(count($options) > 0) : ?>
                                    <select name="cf_<?php echo $r->ID ?>" class="form-control">
                                    <?php foreach($options as $k=>$v) : ?>
                                    <option value="<?php echo $k ?>" <?php if(isset($_POST['cf_' . $r->ID]) && $_POST['cf_'.$r->ID] == $k) echo "selected" ?>><?php echo $v ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>
                            <?php endif; ?>
                            <span class="help-text"><?php echo $r->help_text ?></span>
                        </div>
                </div>
                <?php endforeach; ?>
                <p><?php echo lang("ctn_351") ?></p>

                <?php if(!$this->settings->info->disable_captcha) : ?>
                <div class="form-group">

                    <label for="name-in" class="col-md-3 label-heading"><?php echo lang("ctn_220") ?></label>
                    <div class="col-md-9">
                        <p><?php echo $cap['image'] ?></p>
                        <input type="text" class="form-control" id="captcha-in" name="captcha" placeholder="<?php echo lang("ctn_306") ?>" value="">
                    </div>
                </div>
                <?php endif; ?>
                <?php if($this->settings->info->google_recaptcha) : ?>
                    <div class="form-group">

                    <label for="name-in" class="col-md-3 label-heading"><?php echo lang("ctn_220") ?></label>
                    <div class="col-md-9">
                        <div class="g-recaptcha" data-sitekey="<?php echo $this->settings->info->google_recaptcha_key ?>"></div>
                    </div>
                </div>
                <?php endif ?>











                
                                        <input type="submit" name="s" class="btn btn-primary form-control" value="<?php echo lang("ctn_221") ?>" />


                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php echo lang("ctn_222") ?></p>

                          <!-- <?php if(!$this->settings->info->disable_social_login) : ?>
<div class="text-center decent-margin-top">

  <a href="<?php echo site_url("login/twitter_login") ?>" class="btn btn-success btn-block" >
    <img src="<?php echo base_url() ?>images/social/twitter.png" height="20" class='social-icon' />
   Twitter</a>



  <a href="<?php echo site_url("login/facebook_login") ?>" class="btn btn-info btn-block" >
    <img src="<?php echo base_url() ?>images/social/facebook.png" height="20" class='social-icon' />
   Facebook</a>



  <a href="<?php echo site_url("login/google_login") ?>" class="btn btn-warning btn-block" >
    <img src="<?php echo base_url() ?>images/social/google.png" height="20" class='social-icon' />
   Google</a>

</div>
<?php endif; ?> -->
<br>    
<a href="<?php echo site_url("login") ?>" class="btn btn-primary form-control" ><?php echo lang("ctn_223") ?></a>
                
                                        
                                    </div>
                                </div>
                                <?php echo form_close() ?>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function checkUsername() {
    var username = $('#username').val();
    if(username.length > 0) {
        $.ajax({
            url: global_base_url + "register/check_username",
            type: "get",
            data: {
                "username" : username
            },
            success: function(msg) {
                $('#username_check').html(msg);
            }
        });
    } else {
        $('#username_check').html('');
    }
}

$(document).ready(function() {
    $('#username').change(function() {
        checkUsername();
    });
});


function checkmobilenumber() {
    var mobile = $('#mobile').val();
    if(mobile.length > 0) {
        $.ajax({
            url: global_base_url + "register/check_mobile",
            type: "get",
            data: {
                "mobile" : mobile
            },
            success: function(msg) {
                $('#mobile_check').html(msg);
            }
        });
    } else {
        $('#mobile_check').html('');
    }
}

$(document).ready(function() {
    $('#mobile').change(function() {
        checkmobilenumber();
    });
});
                    </script>
