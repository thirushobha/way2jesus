

<section class="profile-account-setting">
			<div class="container">
				<div class="account-tabs-setting">
					<div class="row">
						<div class="col-lg-3">
							<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
							
						</div>
						<div class="col-lg-9">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
									<div class="acc-setting">
										<h3>Church Invitations</h3>

    
<table class="table table-bordered table-hover table-striped table-responsive">
<tr class="table-header">
	<th><?php echo lang("ctn_347") ?></th><th>Church Name</th><th>Invite <?php echo lang("ctn_608") ?></th><th><?php echo lang("ctn_627") ?></th><th><?php echo lang("ctn_52") ?></th>
</tr>

<?php foreach($invites->result() as $r) : ?>
  <?php
  if(!empty($r->slug)) {
    $slug = $r->slug;
  } else {
    $slug = $r->pageid;
  }
  ?>
<tr>
	<td>
		<a href="<?php echo site_url("pages/view/" . $slug) ?>">
			<img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->profile_avatar ?>" width="40">
		</a>
	</td>
	<td><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo $r->name ?></a></td>
	<td>
		<div class="noty-user-img">
                            <img src="<?php echo site_url();?>uploads/<?php echo $r->avatar;?>" alt="">
                          </div>
                          <div class="request-info">
                            <h3><?php echo $r->first_name.' '.$r->last_name;?>  @ <a href="<?php echo site_url();?>profile/<?php echo $r->username;?>">  <?php echo $r->username;?></a> </h3>
                            
                          </div>
		<?php 

		//echo $this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)) ?></td>
	<td><?php echo date($this->settings->info->date_format, $r->timestamp ) ?></td>
	<td><a href="<?php echo site_url("pages/join_page/" . $r->pageid . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-xs"><?php echo lang("ctn_628") ?></a> <a href="<?php echo site_url("user_settings/delete_page_invite/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-danger btn-xs"><?php echo lang("ctn_624") ?></a></td>

</tr>
<?php endforeach; ?>

</table>







									</div><!--acc-setting end-->
								</div>
							  	
							  	
							</div>
						</div>
					</div>
				</div><!--account-tabs-setting end-->
			</div>
		</section>