

<section class="profile-account-setting">
			<div class="container">
				<div class="account-tabs-setting">
					<div class="row">
						<div class="col-lg-3">
							<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
							
						</div>
						<div class="col-lg-9">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
									<div class="acc-setting">
										<h3>Change Password</h3>

    <?php echo form_open(site_url("user_settings/change_password_pro"), array("class" => "form-horizontal")) ?>
            <div class="cp-field">
                <h5 for="inputEmail3" ><?php echo lang("ctn_238") ?></h5>
                <div class="cpp-fiel">
                  <input type="password" class="form-control" name="current_password" placeholder="Current Password">
                  <i class="fa fa-lock"></i>
                </div>
            </div>
            <div class="cp-field">
                <h5 for="inputEmail3" ><?php echo lang("ctn_239") ?></h5>
                <div class="cpp-fiel">
                  <input type="password" class="form-control" name="new_pass1" placeholder="new password">
                  <i class="fa fa-lock"></i>
                </div>
            </div>
            <div class="cp-field mb-4">
                <h5 for="inputEmail3" ><?php echo lang("ctn_240") ?></h5>
                <div class="cpp-fiel">
                  <input type="password" class="form-control" name="new_pass2" placeholder="confirm password">
                  <i class="fa fa-lock"></i>
                </div>
            </div>

             <input type="submit" name="s" value="<?php echo lang("ctn_241") ?>" class="btn btn-primary form-control" />
    <?php echo form_close() ?>







									</div><!--acc-setting end-->
								</div>
							  	
							  	
							</div>
						</div>
					</div>
				</div><!--account-tabs-setting end-->
			</div>
		</section>