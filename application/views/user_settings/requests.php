
<section class="profile-account-setting">
			<div class="container">
				<div class="account-tabs-setting">
					<div class="row">
						<div class="col-lg-3">
							<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
							
						</div>
						<div class="col-lg-9">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
									<div class="acc-setting">
										<h3>Friend Requests</h3>
                    <div class="requests-list">

                      <?php foreach($requests->result() as $r) : ?>




<div class="request-details">
                          <div class="noty-user-img">
                            <img src="<?php echo site_url();?>uploads/<?php echo $r->avatar;?>" alt="">
                          </div>
                          <div class="request-info">
                            <h3><?php echo $r->first_name.' '.$r->last_name;?>  @ <a href="<?php echo site_url();?>profile/<?php echo $r->username;?>">  <?php echo $r->username;?></a> </h3>
                            <span><?php echo  date($this->settings->info->date_format, $r->timestamp )?></span>
                          </div>
                          <div class="accept-feat">
                            <ul>
                              
                              <a href="<?php echo site_url("user_settings/friend_request/1/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-xs"><?php echo lang("ctn_623") ?></a>
                            </li>
                            <li>
   <a href="<?php echo site_url("user_settings/friend_request/0/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-danger btn-xs"><?php echo lang("ctn_624") ?></a>
 </li>

                            </ul>
                          </div><!--accept-feat end-->
                        </div><!--request-detailse end-->


<?php endforeach; ?>




                        


                        
                      </div><!--requests-list end-->

 







									</div><!--acc-setting end-->
								</div>
							  	
							  	
							</div>
						</div>
					</div>
				</div><!--account-tabs-setting end-->
			</div>
		</section>