<!-- <div class="white-area-content">
<ul class="settings-sidebar">
	<li><span class="glyphicon glyphicon-cog"></span> <a href="<?php echo site_url("user_settings") ?>"><?php echo lang("ctn_156") ?></a></li>
	<li><span class="glyphicon glyphicon-lock"></span> <a href="<?php echo site_url("user_settings/change_password") ?>"><?php echo lang("ctn_225") ?></a></li>
	<li><span class="glyphicon glyphicon-eye-open"></span> <a href="<?php echo site_url("user_settings/privacy") ?>"><?php echo lang("ctn_629") ?></a></li>
	<li><span class="glyphicon glyphicon-glass"></span> <a href="<?php echo site_url("user_settings/social_networks") ?>"><?php echo lang("ctn_422") ?></a></li>
	<li><span class="glyphicon glyphicon-user"></span> <a href="<?php echo site_url("user_settings/friend_requests") ?>"><?php echo lang("ctn_640") ?></a></li>
	<li><span class="glyphicon glyphicon-file"></span> <a href="<?php echo site_url("user_settings/page_invites") ?>"><?php echo lang("ctn_626") ?></a></li>
</ul>
</div> -->

<div class="acc-leftbar">
								<div class="nav nav-tabs1"  >
								    <a class="nav-item nav-link " id="nav-acc-tab"  href="<?php echo site_url("user_settings") ?>"  aria-controls="nav-acc" aria-selected="true"><i class="la la-cogs"></i>Account Setting</a>
								    
								    <a class="nav-item nav-link" id="nav-password-tab"  href="<?php echo site_url("user_settings/change_password") ?>"  aria-controls="nav-password" aria-selected="false"><i class="fa fa-lock"></i>Change Password</a>

								    <a class="nav-item nav-link" id="nav-notification-tab"  href="<?php echo site_url("user_settings/privacy") ?>"  aria-controls="nav-notification" aria-selected="false"><i class="fa fa-flash"></i>Privacy Settings</a>

								    <a class="nav-item nav-link" id="nav-notification-tab"  href="<?php echo site_url("user_settings/social_networks") ?>"  aria-controls="nav-notification" aria-selected="false"><i class="fa fa-flash"></i>Social Networks</a>

								    <a class="nav-item nav-link" id="nav-requests-tab"  href="<?php echo site_url("user_settings/friend_requests") ?>"  aria-controls="nav-requests" aria-selected="false"><i class="fa fa-group"></i>Friend Requests</a>
								    <a class="nav-item nav-link" id="security-login"  href="<?php echo site_url("user_settings/page_invites") ?>"  aria-controls="security-login" aria-selected="false"><i class="fa fa-user-secret"></i>Page Invites</a>

								    
								  </div>
							</div><!--acc-leftbar end-->