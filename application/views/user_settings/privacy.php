
<section class="profile-account-setting">
			<div class="container">
				<div class="account-tabs-setting">
					<div class="row">
						<div class="col-lg-3">
							<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
							
						</div>
						<div class="col-lg-9">
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
									<div class="acc-setting">
										<h3>Privacy Settings</h3>


                    <?php echo form_open_multipart(site_url("user_settings/privacy_pro"), array("class" => "form-horizontal")) ?>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_631") ?></h5>
      <div class="cpp-fiel">
        <select name="profile_view" class="form-control">
          <option value="0"><?php echo lang("ctn_632") ?></option>
          <option value="1" <?php if($this->user->info->profile_view) echo "selected" ?>><?php echo lang("ctn_633") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_634") ?></h5>
      <div class="cpp-fiel">
        <select name="posts_view" class="form-control">
          <option value="0"><?php echo lang("ctn_632") ?></option>
          <option value="1" <?php if($this->user->info->posts_view) echo "selected" ?>><?php echo lang("ctn_633") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_635") ?></h5>
      <div class="cpp-fiel">
        <select name="post_profile" class="form-control">
          <option value="0"><?php echo lang("ctn_632") ?></option>
          <option value="1" <?php if($this->user->info->post_profile) echo "selected" ?>><?php echo lang("ctn_633") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_636") ?></h5>
      <div class="cpp-fiel">
        <select name="tag_user" class="form-control">
          <option value="0"><?php echo lang("ctn_632") ?></option>
          <option value="1" <?php if($this->user->info->tag_user) echo "selected" ?>><?php echo lang("ctn_633") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_637") ?></h5>
      <div class="cpp-fiel">
        <select name="allow_friends" class="form-control">
          <option value="0"><?php echo lang("ctn_53") ?></option>
          <option value="1" <?php if($this->user->info->allow_friends) echo "selected" ?>><?php echo lang("ctn_54") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field">
      <h5 for="inputEmail3" ><?php echo lang("ctn_638") ?></h5>
      <div class="cpp-fiel">
        <select name="allow_pages" class="form-control">
          <option value="0"><?php echo lang("ctn_53") ?></option>
          <option value="1" <?php if($this->user->info->allow_pages) echo "selected" ?>><?php echo lang("ctn_54") ?></option>
        </select>
      </div>
  </div>
  <div class="cp-field mb-5">
      <h5 for="inputEmail3" ><?php echo lang("ctn_639") ?></h5>
      <div class="cpp-fiel">
        <select name="chat_option" class="form-control">
          <option value="0"><?php echo lang("ctn_632") ?></option>
          <option value="1" <?php if($this->user->info->chat_option) echo "selected" ?>><?php echo lang("ctn_633") ?></option>
        </select>
      </div>
  </div>
  <input type="submit" name="s" value="<?php echo lang("ctn_236") ?>" class="btn btn-primary form-control" />
<?php echo form_close() ?>








									</div><!--acc-setting end-->
								</div>
							  	
							  	
							</div>
						</div>
					</div>
				</div><!--account-tabs-setting end-->
			</div>
		</section>