
    <section class="companies-info">
      <div class="container">

         <h4 class="page-header-title"> <span class="glyphicon glyphicon-file"></span>List of Pastors</h4>
         <br>

          <div class="db-header clearfix">
            
            <div class="db-header-extra form-inline"> 

                         <div class="form-group has-feedback no-margin">
                            <div class="input-group">
                            <input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
                            <div class="input-group-btn">
                                <input type="hidden" id="search_type" value="0">
                                    <!-- <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-search" aria-hidden="true"></span></button>
                                    <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
                                      <li><a href="#" onclick="change_search(0)"><span class="fa fa-check" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
                                      <li><a href="#" onclick="change_search(1)"><span class="fa fa-check d-none" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
                                      <li><a href="#" onclick="change_search(2)"><span class="fa fa-check d-none" id="name-exact"></span> <?php echo lang("ctn_81") ?></a></li>
                                      <li><a href="#" onclick="change_search(3)"><span class="fa fa-check d-none" id="cat-exact"></span> <?php echo lang("ctn_560") ?></a></li>
                                    </ul> -->
                                  </div><!-- /btn-group -->
                            </div>
                            </div>


                           
        </div>
        </div>
         <br><br>
        
        <table id="page-table" class="table table-bordered">
                <thead>
                <tr class="table-header"><td>Pastor Name</td><td>Profile Views</td><td>Posts Uploaded</td><td>Members</td><td>View Profile</td></tr>
                </thead>
                <tbody>
                </tbody>
                </table>


        
       
      </div>
    </section><!--companies-info end-->


 <script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#page-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 16,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        null,
        null,
        null,
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("pastors/your_pastors") ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
      "search-like", 
      "search-exact",
      "name-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>