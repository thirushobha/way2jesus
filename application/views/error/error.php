<style type="text/css">
    body {
		font-family: 'Varela Round', sans-serif;
	}
	.modal-confirm {		
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}	
	.modal-confirm .icon-box {
		color: #fff;		
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		background: #8a0000;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm .icon-box i {
		font-size: 58px;
		position: relative;
		top: 3px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 80px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #ce3434;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
	.modal-confirm .btn:hover, .modal-confirm .btn:focus {
		background: #8a0000;
		outline: none;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal({
        	backdrop: 'static',
    keyboard: false
        });
    });
</script>


<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="fa fa-close"></i>
				</div>				
				<h4 class="modal-title">Warning!</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center"><?php echo $message ?></p>
			</div>
			<div class="modal-footer">
				<a class="btn btn-danger btn-block text-white"onclick="window.history.back()"  >Go BACK</a>
			</div>
		</div>
	</div>
</div>     
                                                       
<!-- 
<div class="white-area-content">
<h4><?php echo lang("ctn_133") ?></h4>
<b><?php echo lang("ctn_134") ?></b>: <?php echo $message ?><br /><br />
<p><input type="button" value="<?php echo lang("ctn_135") ?>" onclick="window.history.back()" class="btn btn-default btn-sm" /> </p>

</div> -->