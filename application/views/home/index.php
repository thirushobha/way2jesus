<main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3 col-md-4 pd-left-none no-pd">
                <div class="main-left-sidebar no-margin">
                  <div class="user-data full-width">
                    <div class="user-profile">
                      <div class="username-dt">
                        <a href="<?php echo site_url("profile/" . $this->user->info->username) ?>">
                        <div class="usr-pic">
                          <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->avatar ?>" alt="">
                        </div>
                      </a>
                      </div><!--username-dt end-->
                      <div class="user-specs">
                        <h3><?php echo $this->user->info->first_name ?> <?php echo $this->user->info->last_name ?></h3>
                        <!-- <span>Graphic Designer at Self Employed</span> -->
                      </div>
                    </div><!--user-profile end-->
                     <!-- <ul class="user-fw-status">
                      <li><a href="<?php echo site_url("chat") ?>"><span class="glyphicon glyphicon-envelope sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_482") ?></a></li>

                     </ul> -->
                    <ul class="user-fw-status">
                      <!-- <li><a href="<?php echo site_url("chat") ?>"><span class="glyphicon glyphicon-envelope sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_482") ?></a></li> -->
                      
                        <li><a href="<?php echo site_url("profile/albums/" . $this->user->info->ID) ?>"><span class="glyphicon glyphicon-picture sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_483") ?></a></li>
        <li><a href="<?php echo site_url("pages/your") ?>"><span class="glyphicon glyphicon-duplicate sidebaricon" style="color: #4490f6"></span> My Churches</a></li>
        <li <?php if($type == 2) : ?>class="active"<?php endif; ?>><a href="<?php echo site_url("home/index/2") ?>"><span class="glyphicon glyphicon-list-alt sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_485") ?></a></li>

        <?php if($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
          
        <?php endif; ?>
       
        <?php if($this->common->has_permissions(array("admin", "post_admin"), $this->user)) : ?>
          <li <?php if($type == 4) : ?>class="active"<?php endif; ?>><a href="<?php echo site_url("home/index/4") ?>"><span class="glyphicon glyphicon-tower sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_486") ?></a></li>
        <?php endif; ?>
        <?php if($this->common->has_permissions(array("admin", "page_admin"), $this->user)) : ?>
          <li><a href="<?php echo site_url("pages/all") ?>"><span class="glyphicon glyphicon-tower sidebaricon" style="color: #4490f6"></span> All Churches</a></li>
        <?php endif; ?>
        <?php if($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
       
      <?php endif; ?>


                       <!--  <li>
                        <a href="<?php echo site_url("profile/" . $this->user->info->username) ?>" title="">View Profile</a>
                      </li> -->
                    </ul>
                  </div><!--user-data end-->




                  <div class="suggestions full-width">
                    <div class="sd-title">
                      <h3>Trending</h3>
                      <!-- <i class="la la-ellipsis-v"></i> -->
                    </div><!--sd-title end-->
                    <div class="tags-sec full-width">

                 <ul>     
          <?php foreach($hashtags->result() as $r) : ?>
            <li><a href="<?php echo site_url("home/index/1/" . $r->hashtag) ?>">#<?php echo $r->hashtag ?></a></li>
          <?php endforeach; ?>
        </ul>
        


                      
                      
                      <!-- <div class="view-more">
                        <a href="<?php echo site_url("profile/" . $r->username) ?>">View More</a>
                      </div> -->
                    </div><!--suggestions-list end-->
                  </div><!--suggestions end-->




                  
                  <!-- <div class="tags-sec full-width">
                    <ul>
                      <li><a href="#" title="">Help Center</a></li>
                      <li><a href="#" title="">About</a></li>
                      <li><a href="#" title="">Privacy Policy</a></li>
                      <li><a href="#" title="">Community Guidelines</a></li>
                      <li><a href="#" title="">Cookies Policy</a></li>
                      <li><a href="#" title="">Career</a></li>
                      <li><a href="#" title="">Language</a></li>
                      <li><a href="#" title="">Copyright Policy</a></li>
                    </ul>
                    <div class="cp-sec">
                      <img src="<?php echo base_url();?>assets/images/logo2.png" alt="">
                      <p><img src="<?php echo base_url();?>assets/images/cp.png" alt="">Copyright 2017</p>
                    </div>
                  </div> -->
                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-6 col-md-8 no-pd">
                <div class="main-ws-sec">
                  <?php if($this->common->has_permissions(array("admin", "page_creator", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>

                    <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval):
                    ?>
                <?php
                if($this->settings->info->global_premium && 
                  ($this->user->info->premium_time != -1 && 
                    $this->user->info->premium_time < time()) ) {

                  //$this->session->set_flashdata("globalmsg", lang("success_29"));
                  //redirect(site_url("funds/plans"));
                      ?>
                      <div class="alert alert-danger">
                        <strong>Alert!</strong> Please make payment to Continues process.
                        <br><br>
                        <a href="<?php echo base_url();?>funds/plans" class="btn btn-primary">Make Payment</a>
                      </div>

                      <?php
                }else{

    
                ?>
        
                   <?php include(APPPATH . "views/feed/editor.php"); ?>

                 <?php } ?>
                   <?php endif; ?>

                   <?php endif; ?>
                  <div class="posts-section" id="home_posts">
                   
                    
                    
                   
                    
                    
                </div><!--main-ws-sec end-->
              </div>
            </div>
              <div class="col-lg-3 pd-right-none no-pd">
                <div class="right-sidebar">




                  <?php if($this->common->has_permissions(array("admin",  "page_admin","page_creator"), $this->user)) : ?>
                  <div class="widget suggestions full-width">
                    <div class="sd-title">
                      <h4>Churches Created By You</h4>
                      <!-- <i class="la la-ellipsis-v"></i> -->
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                      <?php foreach($my_churches->result() as $r) : ?>
          <?php 
          if(!empty($r->slug)) {
            $slug = $r->slug;
          } else {
            $slug = $r->ID;
          } ?>


                      <div class="suggestion-usd" class="img img-thumbnail">
                       <a href="<?php echo site_url("pages/view/" . $slug) ?>"> <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->profile_avatar ?>" class="img img-thumbnail" width="70px"  >
                       </a>
                        <br>
                        <div class="sgt-text">
                          <h4><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo $r->name ?></a></h4>
                          <span><?php echo $r->members ?> Members</span>
                        </div>
                        
                      </div>


          
         <?php endforeach; ?>

                      
                      <div class="view-more">
                        <a href="<?php echo site_url("pages/your") ?>">View More</a>
                      </div>
                    </div><!--suggestions-list end-->
                  </div>

<?php endif; ?>


                  

                  <div class="suggestions full-width">
                    <div class="sd-title">
                      <h3>New Users</h3>
                      <!-- <i class="la la-ellipsis-v"></i> -->
                    </div><!--sd-title end-->
                    <div class="suggestions-list">

                      <?php foreach($users->result() as $r) : ?>

                        <div class="suggestion-usd">
                        <a href="<?php echo site_url("profile/" . $r->username) ?>">
                          <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>" width="40">
                        </a>
                        <div class="sgt-text">

                          <h4><a href="<?php echo site_url("profile/" . $r->username) ?>"><?php echo $r->first_name ?> <?php echo $r->last_name ?>
                        </a></h4>
                          <!-- <span>Graphic Designer</span> -->
                        </div>
                        <span><a href="<?php echo site_url("profile/" . $r->username) ?>"><i class="la la-plus"></i></a></span>
                      </div>


         
         <?php endforeach; ?>


                      
                      
                      <!-- <div class="view-more">
                        <a href="<?php echo site_url("profile/" . $r->username) ?>">View More</a>
                      </div> -->
                    </div><!--suggestions-list end-->
                  </div><!--suggestions end-->






                 
               
                  <div class="widget suggestions full-width">
                    <div class="sd-title">
                      <h3>New Churches</h3>
                      <!-- <i class="la la-ellipsis-v"></i> -->
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                      <?php foreach($pages->result() as $r) : ?>
          <?php 
          if(!empty($r->slug)) {
            $slug = $r->slug;
          } else {
            $slug = $r->ID;
          } ?>


                      <div class="suggestion-usd">
                        <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->profile_avatar ?>" width="40">
                        <div class="sgt-text">
                          <h4><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo $r->name ?></a></h4>
                          <span><?php echo $r->members ?> Members</span>
                        </div>
                        <span><a href="<?php echo site_url("pages/view/" . $slug) ?>"><i class="la la-plus"></i></a></span>
                      </div>

          
         <?php endforeach; ?>

                      
                      <div class="view-more">
                        <a href="<?php echo site_url("pages") ?>">View More</a>
                      </div>
                    </div><!--suggestions-list end-->
                  </div>



                </div><!--right-sidebar end-->
              </div>
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>

    



 <script type="text/javascript">
var global_page = 0;
var hide_prev = 0;



$(document).ready(function() {
  load_posts();

});

function load_posts_wrapper() 
{
  load_posts();
}

<?php if($type == 0) : ?>
function load_posts() 
{
  $.ajax({
    url: global_base_url + 'feed/load_home_posts',
    type: 'GET',
    data: {
    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
        nextSelector : '.load_next'
      });
     
    }
  })
}
<?php elseif($type == 1) : ?>
function load_posts() 
{
  $.ajax({
    url: global_base_url + 'feed/load_hashtag_posts',
    type: 'GET',
    data: {
      hashtag : "<?php echo $hashtag ?>",
    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
          nextSelector : '.load_next'
      });
    }
  })
}
<?php elseif($type == 2) : ?>
function load_posts() 
{
  $.ajax({
    url: global_base_url + 'feed/load_saved_posts',
    type: 'GET',
    data: {
    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
          nextSelector : '.load_next'
      });
    }
  })
}
<?php elseif($type == 3) : ?>
var commentid = <?php echo $commentid ?>;
var replyid = <?php echo $replyid ?>;
function load_posts() 
{
  $.ajax({
    url: global_base_url + 'feed/load_single_post/<?php echo $postid ?>',
    type: 'GET',
    data: {
    },
    success: function(msg) {
      $('#home_posts').html(msg);
      if(commentid > 0) {
        // Load comment up
        load_single_comment(<?php echo $postid ?>,commentid, replyid);
      }
    }
  })
}
<?php elseif($type == 4) : ?>
function load_posts() 
{
  $.ajax({
    url: global_base_url + 'feed/load_all_posts',
    type: 'GET',
    data: {
    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
          nextSelector : '.load_next'
      });
    }
  })
}
<?php endif; ?>
</script>