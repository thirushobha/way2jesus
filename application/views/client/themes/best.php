<!DOCTYPE html>
<?php if($enable_rtl) : ?>
<html dir="rtl">
<?php else : ?>
<html lang="en">
<?php endif; ?>

<!-- Mirrored from gambolthemes.net/workwise_demo/HTML/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Feb 2019 05:58:38 GMT -->
<head>
<meta charset="UTF-8">
<title><?php if(isset($page_title)) : ?><?php echo $page_title ?> - <?php endif; ?><?php echo $this->settings->info->site_name ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/line-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/line-awesome-font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/slick/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/cdn/jquery-ui.css">
<link href="<?php echo base_url();?>scripts/libraries/mention/jquery.mentions.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>styles/chat.css" rel="stylesheet" type="text/css">
<style type="text/css">
.navbar-brand{
  color: #ffffff;
}
.comment-section{
  margin-bottom: 20px;
}
.active-comment-like{
  color: #e44d3a !important ;
}
  .active-like{
    color: #e44d3a !important ;
  }
  .jscroll-inner { overflow: hidden; }
  .nodisplay{
    display: none;
  }
  
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>scripts/libraries/jquery.jscroll.js"></script> <!-- infinite scroll -->
<script src="<?php echo base_url();?>scripts/cdn/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>scripts/libraries/jquery.form.min.js"></script>
 <script src="<?php echo base_url();?>scripts/libraries/jquery.nicescroll.min.js"></script>
 <script src="<?php echo base_url() ?>scripts/libraries/select2.full.min.js"></script>
 <script src="<?php echo base_url() ?>scripts/libraries/jquery.geocomplete.min.js"></script>
<script src="<?php echo base_url();?>scripts/libraries/mention/jquery.mentions.js"></script> <!-- @mentions and #hastags -->
<!-- SCRIPTS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/cdn/datatables.min.css"/>
        <script type="text/javascript" src="<?php echo base_url();?>scripts/cdn/datatables.min.js"></script> <!-- datatables -->
        <script src="<?php echo base_url();?>scripts/cdn/ckeditor.js"></script> <!-- text editor -->

        <script src="<?php echo base_url();?>scripts/cdn/twemoji.min.js?2.3.0"></script> <!-- for emoji -->

        <script type="text/javascript">
        var global_base_url = "<?php echo site_url('/') ?>";
        var global_hash = "<?php echo $this->security->get_csrf_hash() ?>";
        </script>
        <?php if(isset($datatable_lang) && !empty($datatable_lang)) : ?>
        <script type="text/javascript">
            $(document).ready(function() {
              $.extend( true, $.fn.dataTable.defaults, {
              "language": {
                "url": "<?php echo $datatable_lang ?>"
            }
              });
          });
     
        </script>
        <?php endif; ?>


        <?php if(isset($fullcalendar_lang) && !empty($fullcalendar_lang)) : ?>
        <script src="<?php echo base_url() . $fullcalendar_lang ?>"></script>
        <?php endif; ?>
         <link href="<?php echo base_url();?>scripts/libraries/select2.min.css" rel="stylesheet" type="text/css">
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo0l7EcNxwWoTYIgE3-p3J0m5_W844_Pg&libraries=places"></script>
        <script src="<?php echo base_url() ?>scripts/libraries/jquery.geocomplete.min.js"></script>
        <script src="<?php echo base_url() ?>scripts/libraries/select2.full.min.js"></script>

        <?php if(!$this->settings->info->disable_chat) : ?>
          <!-- <script src="<?php echo base_url() ?>scripts/custom/chat.js" type="text/javascript"></script>
          <script type="text/javascript">
          time_to_update = 5000;
          </script> -->
        <?php endif; ?>
         <link  href="<?php echo base_url() ?>scripts/libraries/viewer/viewer.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>scripts/libraries/viewer/viewer.js"></script> 

        <!-- CODE INCLUDES -->
        <?php echo $cssincludes ?> 

        <!-- Favicon: http://realfavicongenerator.net -->
                <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url();?>images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url();?>images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
          body { padding-top: 60px; }

          
          @media screen and (min-width: 576px) and (max-width: 767px) {
              body { padding-top: 100px; }
          }

          @media screen and (min-width: 768px) {
              body { padding-top: 60px; }
          }
        </style>
</head>


<body>
  

  <div class="wrapper">
    


    <header class="fixed-top">
      <div class="container">
        <div class="header-data">
          <div class="logo">
            <!-- <a href="index.html" title=""><img src="<?php echo base_url();?>assets/images/logo.png" alt=""></a> -->
            <?php if($this->settings->info->logo_option) : ?>
          <a class="navbar-brand-two" href="<?php echo site_url() ?>" title="<?php echo $this->settings->info->site_name ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?>"  height="50px"></a>
        <?php else : ?>
          <a class="navbar-brand" href="<?php echo site_url() ?>" title="<?php echo $this->settings->info->site_name ?>"><?php echo $this->settings->info->site_name ?></a>
        <?php endif; ?>

          </div><!--logo end-->
          <div class="search-bar">
            <!-- <form>
              <input type="text" name="search" placeholder="Search...">
              <button type="submit"><i class="la la-search"></i></button>
            </form> -->
            <?php echo form_open(site_url(), array("class"=>"")) ?>
              
                <input type="text"   placeholder="<?php echo lang("ctn_76") ?> Churches,Pastors ..." id="search-complete">
                <button type="submit"><i class="la la-search"></i></button>
              
              <?php echo form_close() ?>

          </div><!--search-bar end-->
          <nav>
            <ul>
              <li>
                <a href="<?php echo site_url() ?>" title="">
                  <span><img src="<?php echo base_url();?>assets/images/icon1.png" alt=""></span>
                  Home
                </a>
              </li>
              <li>
              <a href="<?php echo site_url("pastors") ?>" title="">
                  <span class="fa fa-users"></span><br>
                  Pastors
                </a>
              </li>

              <li>
              <a href="<?php echo site_url("pages") ?>" title="">
                  <span><img src="<?php echo base_url();?>assets/images/icon2.png" alt=""></span>
                  Churches
                </a>
              </li>


               <?php if( ($this->common->has_permissions(array("admin", "page_admin", "page_creator"), $this->user)) ) : ?> 
                             
                  <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval):
                    ?>
                       <li> 
                        <a href="<?php echo site_url("pages/add") ?>" > <span class="fa fa-list"></span><br>
                        Create Church</a>
                       </li>
                       <?php endif; ?>

                      <?php endif; ?>

             


               
           <!--  <li><a href="#" data-target="#" onclick="load_chats()" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="email-menu-drop"><span><img src="<?php echo base_url();?>assets/images/icon6.png" alt=""></span>
                  Messages</a>

            <ul class="dropdown-menu" aria-labelledby="email-menu-drop">
            <li>
              <div class="notification-box-title">
                <?php echo lang("ctn_489") ?> - <a href="<?php echo site_url("chat") ?>"><?php echo lang("ctn_482") ?></a>
                </div>
                <div id="chat-scroll">
                  <div id="loading_spinner_email">
                    <span class="glyphicon glyphicon-refresh" id="ajspinner_email"></span>
                  </div>
                </div>
                <div class="notification-box-footer">
                <a href="#" id="chat-click-more" onclick="load_chat_page()"><?php echo lang("ctn_490") ?></a>
              </div>
            </li>
            </ul>

            </li>
 -->

             
            
              <li>
                <a href="#" title="" data-target="#" class="not-box-open"  onclick="load_notifications_unread()">
                  <span>
                    <img src="<?php echo base_url();?>assets/images/icon7.png" alt="">
                    <i class="label label-info badge bg-primary"><?php if($this->user->info->noti_count > 0) : ?><?php echo $this->user->info->noti_count ?><?php endif; ?></i>
                  
                  </span>
                  Notification 
                </a>
                <div class="notification-box">
                  <div class="nt-title">
                    <h4><?php echo lang("ctn_412") ?> <?php if($this->user->info->noti_count > 0) : ?><span class="badge click" id="noti-click-unread" onclick="load_notifications_unread()"></span><?php endif; ?></h4>
                    <a href="#" title=""><?php echo $this->user->info->noti_count ?></a>
                  </div>
                  <div class="nott-list">

                    <div id="notifications-scroll">
              <div id="loading_spinner_notification">
                <span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
              </div>
            </div>
            
                    
                     
                      <div class="view-all-nots">
                        <a href="<?php echo site_url("home/notifications") ?>" title="">View All Notification</a>
                        
                      </div>
                  </div><!--nott-list end-->
                </div><!--notification-box end-->
              </li>
            </ul>
          </nav><!--nav end-->
          <div class="menu-btn">
            <a href="#" title=""><i class="fa fa-bars"></i></a>
          </div><!--menu-btn end-->
          <div class="user-account">
            <div class="user-info">
              <!-- <img src="<?php echo base_url();?>assets/images/resources/user.png" alt=""> -->
              <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->avatar ?>" class="user_avatar" style="width: 30px;height: 30px;">
              <!-- <a href="#" title="">John</a> -->
              <a href="#" ><?php if($this->settings->info->user_display_type) : ?>
              <?php echo $this->user->info->first_name ?> <?php echo $this->user->info->last_name ?>
              <?php else : ?>
              <?php echo $this->user->info->username ?>
              <?php endif; ?></a> 
              <i class="la la-sort-down"></i>
            </div>
            <div class="user-account-settingss">
              <!-- <h3>Online Status</h3> -->
              <!-- <ul class="on-off-status">
                <li>
                  <div class="fgt-sec">
                    <input type="radio" name="cc" id="c5">
                    <label for="c5">
                      <span></span>
                    </label>
                    <small>Online</small>
                  </div>
                </li>
                <li>
                  <div class="fgt-sec">
                    <input type="radio" name="cc" id="c6">
                    <label for="c6">
                      <span></span>
                    </label>
                    <small>Offline</small>
                  </div>
                </li>
              </ul> -->
              <!-- <h3>Custom Status</h3>
              <div class="search_form">
                <form>
                  <input type="text" name="search">
                  <button type="submit">Ok</button>
                </form>
              </div> --><!--search_form end-->
              <!-- <h3>Setting</h3> -->
              <ul class="us-links">
              <!--   <li><a href="profile-account-setting.html" title="">Account Setting</a></li>
                <li><a href="#" title="">Privacy</a></li>
                <li><a href="#" title="">Faqs</a></li>
                <li><a href="#" title="">Terms & Conditions</a></li> -->
                 <li><a href="<?php echo site_url("profile/" . $this->user->info->username) ?>"><?php echo lang("ctn_491") ?></a></li>
              <!-- <li><a href="<?php echo site_url("pages/your") ?>"><?php echo lang("ctn_492") ?></a></li> -->
              <?php if( ($this->common->has_permissions(array("admin", "page_admin", "page_creator"), $this->user)) ) : ?> 
                             
                <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval):
                    ?>
                       <li> <a href="<?php echo site_url("pages/add") ?>" >Create Church</a>
                       </li>
                       <?php endif; ?>
                      <?php endif; ?>

              <li><a href="<?php echo site_url("profile/friends/" . $this->user->info->ID) ?>"><?php echo lang("ctn_493") ?></a></li>
              <li><a href="<?php echo site_url("user_settings") ?>">Settings</a></li>
               <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval){
                    ?>
                <?php
                if($this->settings->info->global_premium && 
     ($this->user->info->premium_time != -1 && 
       $this->user->info->premium_time > time()) ) {
     


                  //$this->session->set_flashdata("globalmsg", lang("success_29"));
                  //redirect(site_url("funds/plans"));

                      ?>
                   <li><a href="<?php echo site_url("funds/subscription") ?>">My Subscription</a></li>
                      

                      <?php
                    }

                    }
                  
                      ?>
              <li><a href="<?php echo site_url("user_settings/change_password") ?>">Change Password</a></li>
              

              </ul>
              <?php if($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings"), $this->user)) : ?>
                <!-- <li role="separator" class="divider"></li> -->
                <h3><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_157") ?></a></h3>
              <?php endif; ?>

              <h3 class="tc"><a href="<?php echo site_url("login/logout/" . $this->security->get_csrf_hash()) ?>"><?php echo lang("ctn_149") ?></a></h3>
            </div><!--user-account-settingss end-->
          </div>
        </div><!--header-data end-->
      </div>
    </header><!--header end-->  

        

     
   <?php if($this->settings->info->install) : ?>
          <section >
                        <div class="container">
                                <div class="alert alert-info alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
  
                                  <b>NOTICE</b> - <a href="<?php echo site_url("install") ?>">Great job on uploading all the files and setting up the site correctly! Let's now create the Admin account and set the default settings. Click here! This message will disappear once you have run the install process.</a>
                                </div>
                        </div>
                    </section>
        <?php endif; ?>
      <?php $gl = $this->session->flashdata('globalmsg'); ?>
        <?php if(!empty($gl)) :?>
                    <section class="alertsection">
                        <div class="container">
                                <div class="alert alert-success alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <b><span class="glyphicon glyphicon-ok"></span></b> <?php echo $this->session->flashdata('globalmsg') ?>
                                </div>
                        </div>
                    </section>

                    <script type="text/javascript">
                      $(document).ready(function() {
                        setTimeout(function() { 
                          $(".alertsection").slideUp('slow');
                           }, 3000);
                      });
                    </script>
        <?php endif; ?>

        
       <!--  <section>
          <div class="row">

        <div class="col-md-2">
           <?php if(isset($sidebar)) : ?>
          <?php echo $sidebar ?>
        <?php endif; ?>
          <?php include(APPPATH . "views/client/friends_bar.php") ?>
          <?php include(APPPATH . "views/client/chat.php"); ?>
        </div>
         <div class="col-md-10"> -->
        <?php echo $content ?>
        <!-- </div>
        
        </div>
        </section> -->

      



    <div class="post-popup pst-pj">
      <div class="post-project">
        <h3>Post a project</h3>
        <div class="post-project-fields">
          <form>
            <div class="row">
              <div class="col-lg-12">
                <input type="text" name="title" placeholder="Title">
              </div>
              <div class="col-lg-12">
                <div class="inp-field">
                  <select>
                    <option>Category</option>
                    <option>Category 1</option>
                    <option>Category 2</option>
                    <option>Category 3</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-12">
                <input type="text" name="skills" placeholder="Skills">
              </div>
              <div class="col-lg-12">
                <div class="price-sec">
                  <div class="price-br">
                    <input type="text" name="price1" placeholder="Price">
                    <i class="la la-dollar"></i>
                  </div>
                  <span>To</span>
                  <div class="price-br">
                    <input type="text" name="price1" placeholder="Price">
                    <i class="la la-dollar"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <textarea name="description" placeholder="Description"></textarea>
              </div>
              <div class="col-lg-12">
                <ul>
                  <li><button class="active" type="submit" value="post">Post</button></li>
                  <li><a href="#" title="">Cancel</a></li>
                </ul>
              </div>
            </div>
          </form>
        </div><!--post-project-fields end-->
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
      </div><!--post-project end-->
    </div><!--post-project-popup end-->

    <div class="post-popup job_post">
      <div class="post-project">
        <h3>Post a job</h3>
        <div class="post-project-fields">
          <form>
            <div class="row">
              <div class="col-lg-12">
                <input type="text" name="title" placeholder="Title">
              </div>
              <div class="col-lg-12">
                <div class="inp-field">
                  <select>
                    <option>Category</option>
                    <option>Category 1</option>
                    <option>Category 2</option>
                    <option>Category 3</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-12">
                <input type="text" name="skills" placeholder="Skills">
              </div>
              <div class="col-lg-6">
                <div class="price-br">
                  <input type="text" name="price1" placeholder="Price">
                  <i class="la la-dollar"></i>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="inp-field">
                  <select>
                    <option>Full Time</option>
                    <option>Half time</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-12">
                <textarea name="description" placeholder="Description"></textarea>
              </div>
              <div class="col-lg-12">
                <ul>
                  <li><button class="active" type="submit" value="post">Post</button></li>
                  <li><a href="#" title="">Cancel</a></li>
                </ul>
              </div>
            </div>
          </form>
        </div><!--post-project-fields end-->
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
      </div><!--post-project end-->
    </div><!--post-project-popup end-->



   

  </div><!--theme-layout end-->


<script src="<?php echo base_url();?>scripts/custom/global.js"></script>
<script src="<?php echo base_url();?>scripts/libraries/jquery.nicescroll.min.js"></script>
    <script type="text/javascript">
      $.widget.bridge('uitooltip', $.ui.tooltip);
    </script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/lib/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/scrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script type="text/javascript">
            $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php if(isset($datetimepicker) && !empty($datetimepicker)) : ?>
        <script type="text/javascript">
          jQuery.datetimepicker.setLocale('<?php echo $datetimepicker ?>');
        </script>
        <?php endif; ?>
    <script type="text/javascript">
     $(document).ready(function() {

        // Get sidebar height
       resize_layout();

        $('.nav-sidebar li').on('shown.bs.collapse', function () {
           $(this).find(".glyphicon-menu-right")
                 .removeClass("glyphicon-menu-right")
                 .addClass("glyphicon-menu-down");
            resize_layout();
        });
        $('.nav-sidebar li').on('hidden.bs.collapse', function () {
           $(this).find(".glyphicon-menu-down")
                 .removeClass("glyphicon-menu-down")
                 .addClass("glyphicon-menu-right");
            resize_layout();
        });

        function resize_layout() 
        {
          var sb_h = $('.sidebar').height();
          var mc_h = $('#main-content').height();
          var w_h = $(window).height();
          $('.sidebar').height($(window).height());
          if(sb_h > mc_h) {
            $('#main-content').css("min-height", sb_h+50 + "px");
          }
          if(w_h > mc_h) {
            $('#main-content').css("min-height", (w_h-(51+30)) +"px");
          }
        }
     });
    </script>
</body>

<!-- Mirrored from gambolthemes.net/workwise_demo/HTML/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Feb 2019 05:59:28 GMT -->
</html>