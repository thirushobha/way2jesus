        <link rel="stylesheet" href="<?php echo base_url();?>subscription/css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url();?>subscription/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url();?>subscription/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url();?>subscription/css/media-queries.css">
<!-- Coming Soon -->
        <div class="coming-soon">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                        	
                            <h2 class="wow fadeInLeftBig">Your Subscription Plan Expires at</h2>
                            <div class="timer wow fadeInUp">
                                <div class="days-wrapper">
                                    <span class="days"></span> <br>days
                                </div> 
                                <span class="slash">/</span> 
                                <div class="hours-wrapper">
                                    <span class="hours"></span> <br>hours
                                </div> 
                                <span class="slash">/</span> 
                                <div class="minutes-wrapper">
                                    <span class="minutes"></span> <br>minutes
                                </div> 
                                <span class="slash">/</span> 
                                <div class="seconds-wrapper">
                                    <span class="seconds"></span> <br>seconds
                                </div>
                            </div>
                            <div class="wow fadeInLeftBig text-white">
                            	<p class="text-white">
                            		We are working very hard on the new features of our site. 
                            		In the meantime sign up to our newsletter and you'll be notified when new Feature available: 
                            	</p>
                            </div>
                            <div class="subscribe wow fadeInUp text-center">
			                    <form class="form-inline text-center justify-content-center" action="#" method="post">
			                        <input type="text" name="email" placeholder="Enter your email..." class="subscribe-email">
			                        <button type="submit" class="btn">Subscribe</button>
			                    </form>
			                    <div class="success-message"></div>
			                    <div class="error-message"></div>
			                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

         <!-- Javascript -->

        <script src="<?php echo base_url();?>subscription/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url();?>subscription/js/jquery.countdown.min.js"></script>
        <script src="<?php echo base_url();?>subscription/js/wow.min.js"></script>

        <script type="text/javascript">
            // Set the date we're counting down to
    // var countDownDate = new Date("March 27, 2019 00:00:00").getTime();
    var countDownDate = <?php echo $userinfo->premium_time;?>.+'000';

    // alert(countDownDate);

// Update the count down every 1 second
    var x = setInterval(function() {

// Get todays date and time
    var now = new Date().getTime();

// Find the distance between now an the count down date
    var distance = countDownDate - now;

// Time calculations for days, hours, minutes and seconds
    var days =  Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours =  ("0" +Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2);
    var minutes = ("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2);
    var seconds =  ("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2);
 // Output the result in an element with id="demo"

     // $('.timer').countdown(countTo, function(event) {
    //     $(this).find('.days').text(event.offset.totalDays);
    //     $(this).find('.hours').text(event.offset.hours);
    //     $(this).find('.minutes').text(event.offset.minutes);
    //     $(this).find('.seconds').text(event.offset.seconds);
    // });
     if (distance > 0) {
    $('.days').text(days);
     $('.hours').text(hours);
      $('.minutes').text(minutes);
       $('.seconds').text(seconds);
     document.getElementById("countdown").innerHTML = '<div class="timer-wrapper"><div class="time">' + days + ':</div><span class="text">DAYS</span></div><div class="timer-wrapper"><div class="time">' + hours + ':</div><span class="text">HOURS</span></div><div class="timer-wrapper"><div class="time">' +
            minutes + ':</div><span class="text">MINUTES</span></div><div class="timer-wrapper"><div class="time">' + seconds + '</div><span class="text">SECONDS</span></div>';

        }

  // If the count down is over, write some text 
     if (distance < 0) {
        clearInterval(x);
         $('.days').text(0);
     $('.hours').text(0);
      $('.minutes').text(0);
       $('.seconds').text(0);
        document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
        </script>
        
        <script type="text/javascript">
            
jQuery(document).ready(function() {
    
    /*
        Fullscreen background
    */
    $.backstretch("../subscription/img/backgrounds/1.jpg");
    
    /*
        Wow
    */
    new WOW().init();
    
    /*
        Countdown initializer
    */
    // var now = new Date();
    // alert(now.valueOf());
    // var countTo = 25 * 24 * 60 * 60 * 1000 + now.valueOf();    
    //    var now = <?php echo $userinfo->premium_time;?>*1000;
    // var countTo = 25 * 24 * 60 * 60 * 1000 + now;
    // $('.timer').countdown(countTo, function(event) {
    //     $(this).find('.days').text(event.offset.totalDays);
    //     $(this).find('.hours').text(event.offset.hours);
    //     $(this).find('.minutes').text(event.offset.minutes);
    //     $(this).find('.seconds').text(event.offset.seconds);
    // });
    
    /*
        Subscription form
    */
    $('.success-message').hide();
    $('.error-message').hide();
    
    $('.subscribe form').submit(function(e) {
        e.preventDefault();
        var postdata = $('.subscribe form').serialize();
        $.ajax({
            type: 'POST',
            url: 'assets/subscribe.php',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                if(json.valid == 0) {
                    $('.success-message').hide();
                    $('.error-message').hide();
                    $('.error-message').html(json.message);
                    $('.error-message').fadeIn();
                }
                else {
                    $('.error-message').hide();
                    $('.success-message').hide();
                    $('.subscribe form').hide();
                    $('.success-message').html(json.message);
                    $('.success-message').fadeIn();
                }
            }
        });
    });
    
});


        </script>