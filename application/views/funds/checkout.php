<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/checkout.css">
<main class="page">
	 	<section class="shopping-cart dark">
	 		<div class="container">
		        <div class="block-heading">
		          <h2 class="font-weight-bold ">Checkout</h2>
		          <p>please check you chosen plan. you want to change plan click cancel button otherwise click checkout button.</p>
		        </div>
		        <div class="content">
	 				<div class="row">
	 					<div class="col-md-12 col-lg-8 bg-danger">
	 						<div class="items">
				 				<div class="product">
				 					<div class="row">
					 					<!-- <div class="col-md-3">
					 						<img class="img-fluid mx-auto d-block image" src="image.jpg">
					 					</div> -->
					 					

					 					<div class="col-md-12">
					 						<div class="info">
						 						<div class="row">
							 						<div class="col-md-6 product-name">
							 							<div class="product-name">
								 							<a href="#" class="text-white">PLAN NAME: <?php echo $plans->name;?></a>
								 							<div class="product-info text-white">
									 							<div>No Of Years: <span class="value"><?php echo $plans->days;?> years</span></div>
									 							
									 						</div>
									 					</div>
							 						</div>
							 						
							 						<div class="col-md-6 ">
							 							<div class="product-name">
								 							<a href="#" class="text-white">Amount </a>
								 							<div class="product-info text-white">
									 							<div> <span class="value">Rs.<?php echo $plans->cost;?>/-</span></div>
									 							
									 						</div>
									 					</div>
							 							
							 						</div>
							 					</div>
							 				</div>
					 					</div>

					 					


					 				</div>
					 				<div class="row mt-5">
					 						<div class="col-md-12">
					 							<a href="<?php echo base_url();?>funds/plans" class="btn btn-primary "><i class="fa fa-hand-o-left"></i> Back To Plans</a>
					 						</div>
					 					</div>
				 				</div>
				 				
				 				
				 			</div>
			 			</div>
			 			<div class="col-md-12 col-lg-4">
			 				<div class="summary">
			 					<h3>Summary</h3>
			 					<div class="summary-item"><span class="text">Subtotal</span><span class="price">&#8377; <?php echo $plans->cost;?>.00</span></div>
			 					<?php
			 					$subcost = $plans->cost;
			 					
			 					$payment_gateway_charges = number_format((float)($subcost*4)/100, 2, '.', '');
			 					$gst = number_format((float)($payment_gateway_charges*18)/100, 2, '.', '');
			 					$total_amount = $subcost+$gst+$payment_gateway_charges;
			 					number_format((float)$subcost, 2, '.', '');

			 					?>
			 					<div class="summary-item"><span class="text">18% GST</span><span class="price">&#8377;<?php echo $gst;?></span></div>
			 					<div class="summary-item"><span class="text">Payment Gateway Charges</span><span class="price">&#8377;<?php echo $payment_gateway_charges;?></span>
			 					</div>
			 					<br>
			 					<div class="summary-item"><span class="text">Total</span><span class="price">&#8377; <?php echo $total_amount;?></span></div>
			 					<br><br>
			 					<!-- <button type="button" class="btn btn-primary btn-lg btn-block">Checkout</button> -->
			 					<a href="<?php echo base_url();?>payment/index/<?php echo $total_amount;?>/<?php echo $plans->ID;?>"  class="btn btn-primary btn-lg btn-block">Checkout</a>
				 			</div>
			 			</div>
		 			</div> 
		 		</div>
	 		</div>
		</section>
	</main>