<style type="text/css">
          body { padding-top: 120px; }

          
          @media screen and (min-width: 576px) and (max-width: 767px) {
              body { padding-top: 100px; }
          }

          @media screen and (min-width: 768px) {
              body { padding-top: 60px; }
          }
        </style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/pricingtable.css">        
        <div class="demo11">
            <div class="container">
                <h4 style="padding-top:10px">Payment Plans</h4>
                <div class="row">
                    <?php

                    foreach ($plans->result() as $key => $value) {
                        ?>
                        
                    <div class="col-md-3 col-sm-6">
                        <div class="pricingTable11 <?php echo $value->bgcolor;?>">
                            <div class="pricingTable-header">
                                <i class="<?php echo $value->icon;?>"></i>
                                <div class="price-value"> &#8377;<?php echo $value->cost;?> <span class="month">per 
                                    <?php echo $value->days;?> year</span> </div>
                            </div>
                            <h3 class="heading"><?php echo $value->name;?></h3>
                            <div class="pricing-content">
                                <ul>
                                    <li><b>1</b>Church</li>
                                    <li><b>Unlimited</b> Members access</li>
                                    <li><b>Unlimited</b> Daily Posts</li>
                                    <li>Unlimited Videos</li>
                                    <li>Unlimited Audios</li>
                                </ul>
                            </div>
                            <div class="pricingTable-signup">
                                <!-- <a href="<?php echo base_url();?>payment/index/<?php echo $value->cost;?>/<?php echo $value->ID;?>" target="_blank">Subscribe</a> -->
                                <a href="<?php echo base_url();?>payment/checkout/<?php echo $value->cost;?>/<?php echo $value->ID;?>" >Subscribe</a>
                            </div>
                        </div>
                    </div>

                <?php } ?>


         
                    <!-- <div class="col-md-3 col-sm-6">
                        <div class="pricingTable11 green">
                            <div class="pricingTable-header">
                                <i class="fa fa-briefcase"></i>
                                <div class="price-value"> &#8377;4999.00 <span class="month">per 2 years</span> </div>
                            </div>
                            <h3 class="heading">SILVER</h3>
                            <div class="pricing-content">
                                <ul>
                                    <li><b>1</b>Church</li>
                                    <li><b>500</b> Members access</li>
                                    <li><b>100</b> Daily Posts</li>
                                    <li>Video albums</li>
                                    <li>Audio albums</li>
                                </ul>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Subscribe</a>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-3 col-sm-6">
                        <div class="pricingTable11 blue">
                            <div class="pricingTable-header">
                                <i class="fa fa-cube"></i>
                                <div class="price-value"> &#8377;5999.00 <span class="month">per 3 years</span> </div>
                            </div>
                            <h3 class="heading">Gold</h3>
                            <div class="pricing-content">
                                <ul>
                                    <li><b>1</b>Church</li>
                                    <li><b>500</b> Members access</li>
                                    <li><b>100</b> Daily Posts</li>
                                    <li>Video albums</li>
                                    <li>Audio albums</li>
                                </ul>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Subscribe</a>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-3 col-sm-6">
                        <div class="pricingTable11 red">
                            <div class="pricingTable-header">
                                <i class="fa fa-diamond"></i>
                                <div class="price-value"> &#8377;7999.00 <span class="month">per 4 years</span> </div>
                            </div>
                            <h3 class="heading">Diamond</h3>
                            <div class="pricing-content">
                                <ul>
                                    <li><b>1</b>Church</li>
                                    <li><b>500</b> Members access</li>
                                    <li><b>100</b> Daily Posts</li>
                                    <li>Video albums</li>
                                    <li>Audio albums</li>
                                </ul>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Subscribe</a>
                            </div>
                        </div>
                    </div> -->




                </div>
            </div>
        </div>
        <hr>
        
        