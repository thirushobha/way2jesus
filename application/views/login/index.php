 <div class="col-md-3 register-left">
                        <a href="<?php echo base_url();?>">
                          <!-- <?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->settings->info->site_logo ?> -->
                          <img src="<?php echo base_url();?>front/images/icon.png" alt="" width="200px"/>
                        </a>
                        <h3><a href="<?php echo base_url();?>" class="text-white btn-link"> Way2Jesus</a></h3>
                        <p><?php echo lang("ctn_304") ?> <?php echo $this->settings->info->site_name ?></p>
                      
                        <a href="<?php echo site_url("register") ?>" class="btn btn-success form-control" ><?php echo lang("ctn_305") ?></a>
                        <br/><br>
                       
                    </div>
                    <div class="col-md-9 register-right">
                        
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                               <?php if(!empty($fail)) : ?>
                          <div class="alert alert-danger"><?php echo $fail ?></div>
                      <?php endif; ?>

                                <h3 class="register-heading">User / Pastor Login</h3>
                                <?php if(isset($_GET['redirect'])) : ?>
        <?php echo form_open(site_url("login/pro/" . urlencode($_GET['redirect']))) ?>
        <?php else : ?>
        <?php echo form_open(site_url("login/pro")) ?>
        <?php endif; ?>





                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control" placeholder="<?php echo lang("ctn_303") ?>">
                                        </div>
                                        
                                        <div class="form-group">
                                            
                                             <input type="password" name="pass" class="form-control" placeholder="<?php echo lang("ctn_180") ?>">
                                        </div>
                                        









               










                
                                        <input type="submit" class="btn btn-primary form-control" value="<?php echo lang("ctn_184") ?>">


                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php //echo lang("ctn_222") ?></p>

                          <?php if(!$this->settings->info->disable_social_login) : ?>
<div class="text-center decent-margin-top">
<?php if(!empty($this->settings->info->twitter_consumer_key) && !empty($this->settings->info->twitter_consumer_secret)) : ?>
  <a href="<?php echo site_url("login/twitter_login") ?>" class="btn btn-success btn-block" >
    <img src="<?php echo base_url() ?>images/social/twitter.png" height="20" class='social-icon' />
   Twitter</a>
<?php endif; ?>

<?php if(!empty($this->settings->info->facebook_app_id) && !empty($this->settings->info->facebook_app_secret)) : ?>
  <a href="<?php echo site_url("login/facebook_login") ?>" class="btn btn-info btn-block" >
    <img src="<?php echo base_url() ?>images/social/facebook.png" height="20" class='social-icon' />
   Facebook</a>

<?php endif; ?>
<?php if(!empty($this->settings->info->google_client_id) && !empty($this->settings->info->google_client_secret)) : ?>

  <a href="<?php echo site_url("login/google_login") ?>" class="btn btn-warning btn-block" >
    <img src="<?php echo base_url() ?>images/social/google.png" height="20" class='social-icon' />
   Google</a>
   <?php endif; ?>

</div>
<?php endif; ?>
<br>    
<a href="<?php echo site_url("login/forgotpw") ?>" class="btn btn-warning"><?php echo lang("ctn_181") ?></a>
                
                                        
                                    </div>
                                </div>
                                <?php echo form_close() ?>
                            </div>






                        </div>
                    </div>
