 <script type="text/javascript">
$(document).ready(function() {
  load_posts(<?php echo $user->ID ?>);
});

function load_posts_wrapper() 
{
  load_posts(<?php echo $user->ID ?>);
}

function load_posts(userid) 
{
  $.ajax({
    url: global_base_url + 'feed/load_user_posts/' + userid,
    type: 'GET',
    data: {

    },
    success: function(msg) {
      $('#home_posts').html(msg);
      $('#home_posts').jscroll({
          nextSelector : '.load_next'
      });
    }
  })
}
 </script>


<section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $user->profile_header ?>" alt="">
      <?php if($this->user->info->ID==$user->ID) : ?>
      <a data-toggle="modal" data-target="#bannermodal" style="cursor: pointer;"><i class="fa fa-camera"></i> Change Image</a>
      <?php endif;?>
    </section>

  
  


 <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">
              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $user->avatar ?>" alt="" width="150px">
                     <?php if($this->user->info->ID==$user->ID) : ?>
                      <a data-toggle="modal" data-target="#myModal" class="text-white" style="cursor: pointer;"><i class="fa fa-camera"></i></a>
                      <?php endif;?>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        <!-- <li><a href="#" title="" class="flww"><i class="la la-plus"></i> Follow</a></li> -->
                        <!-- <li><a href="#" title="" class="hre">Hire</a></li> -->
                          <?php if($user->ID != $this->user->info->ID) : ?>
<?php if($friend_flag) : ?>
<button type="button" class="btn btn-success btn-sm" id="friend_button_<?php echo $user->ID ?>"><i class="fa fa-check" aria-hidden="true"></i> <?php echo lang("ctn_493") ?></button>
<?php else : ?>
<?php if($request_flag) : ?>
<button type="button" class="btn btn-success btn-sm disabled" id="friend_button_<?php echo $user->ID ?>"><?php echo lang("ctn_601") ?></button>
<?php else : ?> 
  <?php if(!$user->allow_friends) : ?>
  <button type="button" class="btn btn-success btn-sm" onclick="add_friend(<?php echo $user->ID ?>)" id="friend_button_<?php echo $user->ID ?>"><?php echo lang("ctn_602") ?></button>
  <?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><i class="fa fa-flag" aria-hidden="true"></i></button>

                      </ul>
                      <ul class="flw-status">
                        <li>
                          <span><?php echo lang("ctn_415") ?></span>
                          <b><?php echo number_format($user->profile_views) ?></b>
                        </li>
                        <li>
                          <span><?php echo lang("ctn_605") ?></span>
                          <b><?php echo $user->post_count ?></b>
                        </li>
                      </ul>
                    </div><!--user_pro_status end-->
                    


<!-- <div class="page-block-title">
  <span class="glyphicon glyphicon-globe"></span> <?php echo lang("ctn_603") ?>
  </div>
  <div class="page-block-intro">
  <?php echo $user->aboutme ?>
  </div>
  <hr> -->
                                        <ul class="social_links">
<?php if(isset($user->location_live) && !empty($user->location_live)) : ?>
    <li class="page-block-tidbit">
    <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo lang("ctn_607") ?> <?php echo $user->location_live ?>
    </li>
  <?php endif; ?>
  <?php if(isset($user->location_from) && !empty($user->location_from)) : ?>
    <li class="page-block-tidbit">
    <i class="fa fa-map-pin" aria-hidden="true"></i> <?php echo lang("ctn_608") ?> <?php echo $user->location_from ?>
    </li>
  <?php endif; ?>
  <?php foreach($fields->result() as $r) : ?>
    <?php if($r->value) : ?>
      <li class="page-block-tidbit">
      <strong><?php echo $r->name ?> : </strong> <?php echo $r->value ?>
      </li>
    <?php endif; ?>
  <?php endforeach; ?>

  <?php if($user->relationship_status != 0) : ?>
    <li class="page-block-tidbit">
      <?php if($user->relationship_status == 1) : ?>
      <span class="glyphicon glyphicon-heart-empty"></span> <?php echo lang("ctn_609") ?>
    <?php elseif($user->relationship_status == 2) : ?>
      <span class="glyphicon glyphicon-heart"></span> <?php echo lang("ctn_610") ?> <?php if($relationship_user != null) : ?><?php echo lang("ctn_611") ?> <a href="<?php echo site_url("profile/" . $relationship_user->username) ?>"><?php echo $relationship_user->first_name ?> <?php echo $relationship_user->last_name ?></a><?php endif; ?>
    <?php elseif($user->relationship_status == 2) : ?>
      <span class="glyphicon glyphicon-heart"></span> <?php echo lang("ctn_612") ?> <?php if($relationship_user != null) : ?><?php echo lang("ctn_613") ?> <a href="<?php echo site_url("profile/" . $relationship_user->username) ?>"><?php echo $relationship_user->first_name ?> <?php echo $relationship_user->last_name ?></a><?php endif; ?>
    <?php endif; ?>
    </li>
  <?php endif; ?>

                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>


                    <ul class="like-com m-3">

                      <?php if(isset($user_data) && $user_data->twitter) : ?>

 <li> <a href="https://twitter.com/<?php echo $this->security->xss_clean($user_data->twitter) ?>" >
    <i class="fa fa-twitter"></i></a>
  </li>

<?php endif; ?>

<?php if(isset($user_data) && $user_data->facebook) : ?>
<li>
  <a href="https://www.facebook.com/<?php echo $this->security->xss_clean($user_data->facebook) ?>" >
    <i class="fa fa-facebook-square"></i></a>
</li>
<?php endif; ?>

<?php if(isset($user_data) && $user_data->google) : ?>
<li>
  <a href="https://plus.google.com/<?php echo $this->security->xss_clean($user_data->google) ?>" >
    <i class="fa fa-google-plus-square"></i></a>
</li>
<?php endif; ?>

<?php if(isset($user_data) && $user_data->linkedin) : ?>
<li>
  <a href="https://www.linkedin.com/in/<?php echo $this->security->xss_clean($user_data->google) ?>" >
    <i class="fa fa-instagram"></i></a>
</li>
<?php endif; ?>

<?php if(isset($user_data) && $user_data->website) : ?>
<li>
  <a href="<?php echo $this->security->xss_clean($user_data->website) ?>" >
    <i class="la la-globe"></i></a>
</li>
<?php endif; ?>

                      
                      <!-- <li><a href="#" title=""><i class="fa fa-twitter"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-facebook-square"></i> </a></li>
                      
                      <li><a href="#" title=""><i class="fa fa-google-plus-square"></i> </a></li>
                      <li><a href="#" title=""><i class="fa fa-instagram"></i> </a></li>
                      <li><a href="#" title=""><i class="la la-globe"></i></a></li> -->
                      
                    </ul>


                  </div><!--user_profile end-->
                  <div class="suggestions full-width">
                    <div class="sd-title">
                      <h3>About Me</h3>
                      <i class="la la-ellipsis-v"></i>
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                      <div class="suggestion-usd">
                        <p><?php echo $user->aboutme;?></p>
                      </div>
                      
                      
                    
                    </div><!--suggestions-list end-->
                  </div><!--suggestions end-->


                  <div class="suggestions full-width">
                    <div class="sd-title">
                      <h3><a href="<?php echo site_url("profile/friends/" . $user->ID) ?>">My Friends</a></h3>
                      <i class="la la-ellipsis-v"></i>
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                     
                        <?php foreach($friends->result() as $r) : ?>


                        	<div class="suggestion-usd">
												<img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>" alt="" width="35px">
												<div class="sgt-text">
													<h4><?php echo $r->first_name ?> <?php echo $r->last_name ?></h4>
													<!-- <span>Graphic Designer</span> -->
												</div>
												<span><a href="<?php echo site_url("profile/" . $r->username) ?>"><i class="la la-plus"></i></a></span>
											</div>
        <?php endforeach; ?>
                    
                      
                      
                    
                    </div><!--suggestions-list end-->
                  </div><!--suggestions end-->







                </div><!--main-left-sidebar end-->
              </div>
              <div class="col-lg-6">
                <div class="main-ws-sec">
                  <div class="user-tab-sec">
                    <h3><?php echo $user->first_name ?> <?php echo $user->last_name ?></h3>
                    <div class="star-descp">
                      
                      <a href="#" title=""><?php if($user->online_timestamp > time() - (60*15)) : ?>
        <span class="profile-online text-success"><?php echo lang("ctn_139") ?></span>
      <?php else : ?>
        <span class="profile-offline text-secondary"><?php echo lang("ctn_335") ?></span>
      <?php endif; ?></a>
                    </div><!--star-descp end-->
                    <div class="st2">
                      <ul class="job-dt">
                            <!-- <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li> -->
                            <li><a href="<?php echo site_url("profile/" . $user->username) ?>" title=""><?php echo lang("ctn_200") ?></a></li>
                            <li><a href="<?php echo site_url("profile/friends/" . $user->ID) ?>" title=""><?php echo lang("ctn_493") ?></a></li>
                            <li><a href="<?php echo site_url("profile/albums/" . $user->ID) ?>" title=""><?php echo lang("ctn_483") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current" id="feed-dd">
                  	  <?php if( ($user->post_profile && ($this->user->info->ID == $user->ID || $friend_flag)) || !$user->post_profile) : ?>
					 	<?php 
					  $editor_placeholder = lang("ctn_579") . " " . $user->first_name . "'s ".lang("ctn_614")." ...";
					  $target_type = "user_profile";
					  $targetid = $user->ID;

					   ?>
             <?php if($this->common->has_permissions(array("admin", "page_creator", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
              <?php 
                    if($this->user->check_admin_permission($this->user->info->ID) || $this->settings->info->user_approval):
                    ?>

                     <?php
                if($this->settings->info->global_premium && 
                  ($this->user->info->premium_time != -1 && 
                    $this->user->info->premium_time < time()) ) {

                  //$this->session->set_flashdata("globalmsg", lang("success_29"));
                  //redirect(site_url("funds/plans"));
                      ?>
                      <div class="alert alert-danger">
                        <strong>Alert!</strong> Please make payment to Continues process.
                        <br><br>
                        <a href="<?php echo base_url();?>funds/plans" class="btn btn-primary">Make Payment</a>
                      </div>

                      <?php
                }else{

    
                ?>
          
					 <?php include(APPPATH . "views/feed/editor.php"); ?>

          <?php } ?>
           <?php endif; ?>
            <?php endif; ?>
					<?php endif; ?>

                    <div class="posts-section" id="home_posts">
                      
                    </div><!--posts-section end-->
                  </div><!--product-feed-tab end-->
                  
                 
                  
                  
                 
                </div><!--main-ws-sec end-->
              </div>
              <div class="col-lg-3">
                <div class="right-sidebar">
                 <!--  <div class="message-btn">
                    <a href="#" title=""><i class="fa fa-envelope"></i> Message</a>
                  </div> -->

                  <?php 
                  //echo $this->common->has_permissions(array(  "page_creator"), $this->user);
                  if($this->common->has_permissions(array("admin",  "page_admin","page_creator"), $this->user)) :
                   
                     if($this->user->check_admin_permission($user->ID) || $this->settings->info->user_approval):
                    if($my_churches->num_rows()>0):
                   ?>
                  <div class="widget suggestions full-width">
                    <div class="sd-title">
                      <h4>Churches List</h4>
                      <!-- <i class="la la-ellipsis-v"></i> -->
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                      <?php 

                      foreach($my_churches->result() as $r) : ?>
          <?php 
          if(!empty($r->slug)) {
            $slug = $r->slug;
          } else {
            $slug = $r->ID;
          } ?>


                      <div class="suggestion-usd" class="img img-thumbnail">
                       <a href="<?php echo site_url("pages/view/" . $slug) ?>"> <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->profile_avatar ?>" class="img img-thumbnail" width="70px"  >
                       </a>
                        <br>
                        <div class="sgt-text">
                          <h4><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo $r->name ?></a></h4>
                          <span><?php echo $r->members ?> Members</span>
                        </div>
                        
                      </div>


          
         <?php 
       endforeach; 

         ?>

                      
                      <div class="view-more">
                        <!-- <a href="<?php echo site_url("pages/your") ?>">View More</a> -->
                      </div>
                    </div><!--suggestions-list end-->
                  </div>

<?php 
endif; 
endif;
endif;
?>


                  <div class="widget widget-portfolio">
                    <div class="wd-heady">
                      <h3><a href="<?php echo site_url("profile/albums/" . $user->ID) ?>"><?php echo lang("ctn_483") ?></a></h3>
                      <img src="<?php echo base_url();?>assets/images/photo-icon.png" alt="">
                    </div>
                    <div class="pf-gallery">
                      <ul>
                      	<?php foreach($albums->result() as $r) : ?>
           
            <?php if(isset($r->file_name)) : ?>
             <li> <a href="<?php echo site_url("profile/view_album/" . $r->ID) ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>" width="50px" height="50px"></a>
             </li>
            <?php else : ?>
             <li> <a href="<?php echo site_url("profile/view_album/" . $r->ID) ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/default_album.png" width="50px" height="50px"></a>
             </li>
            <?php endif; ?>
            <!-- <li><p><a href="<?php echo site_url("profile/view_album/" . $r->ID) ?>"><?php echo $r->name ?></a></p>
            </li> -->
          <?php endforeach; ?>

                      
                      </ul>
                    </div><!--pf-gallery end-->
                  </div><!--widget-portfolio end-->
                </div><!--right-sidebar end-->
              </div>
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>







<!-- Button to Open the Modal -->
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    Open modal
  </button>
 -->
  <!-- The Modal -->



    <div class="modal" id="bannermodal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Banner Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <?php echo form_open_multipart(site_url("user_settings/pic_upload"), array("class" => "form-horizontal")) ?>

          <div class="cp-field">
        <h5 for="inputEmail3">Banner Image</h5>
         
        <div class="cpp-fiel">
        <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->profile_header ?>" width="100%" />
        <?php if($this->settings->info->avatar_upload) : ?>
            <input type="file" name="userfile_profile" /> 
         <?php endif; ?>
        </div>
    

         <input type="submit" name="s" value="Submit" class="btn btn-primary" />
    </div>
    <br>

         
<?php echo form_close() ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>


  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Profile Pic</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <?php echo form_open_multipart(site_url("user_settings/pic_upload"), array("class" => "form-horizontal")) ?>

          <div class="cp-field">
        <h5 for="inputEmail3">Profile Pic</h5>
        <div class="cpp-fiel">
        <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->avatar ?>" width="75px"/>
        <?php if($this->settings->info->avatar_upload) : ?>
            <input type="file" name="userfile" class="form-control" /> 
            <br>
         <?php endif; ?>
        </div>

         <input type="submit" name="s" value="Submit" class="btn btn-primary" />
    </div>
    <br>

         
<?php echo form_close() ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>










   <?php echo form_open(site_url("profile/report_profile/" . $user->ID)) ?>
 <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-flag"></span> Report <?php echo $user->first_name ?> <?php echo $user->last_name ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading">Reason for Reporting</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="reason">
                    </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="Report">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?> 