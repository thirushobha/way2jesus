<!--   <style>
.imagecontainer {
  position: relative;

}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #008CBA;
}

.imagecontainer:hover .overlay {
  opacity: 1;
}

.text {
  color: white !important;
  font-size: 50px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}
</style> -->

<style>
.imagecontainer {
  position: relative;
  width: 50%;
}

.image {
  opacity: 1;
  display: block;
  width: 80%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.imagecontainer:hover .image {
  opacity: 0.3;
}

.imagecontainer:hover .middle {
  opacity: 1;
}

.text {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}
</style>
  <section class="cover-sec">
      <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $user->profile_header ?>" alt="">
      <a href="#" title=""><i class="fa fa-camera"></i> Change Image</a>
    </section>



 <main>
      <div class="main-section">
        <div class="container">
          <div class="main-section-data">
            <div class="row">





























              <div class="col-lg-3">
                <div class="main-left-sidebar">
                  <div class="user_profile">
                    <div class="user-pro-img">
                      <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $user->avatar ?>" alt="" width="150px">
                      <a href="#" title=""><i class="fa fa-camera"></i></a>
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                      <ul class="flw-hr">
                        <!-- <li><a href="#" title="" class="flww"><i class="la la-plus"></i> Follow</a></li> -->
                        <!-- <li><a href="#" title="" class="hre">Hire</a></li> -->
                          <?php if($user->ID != $this->user->info->ID) : ?>
<?php if($friend_flag) : ?>
<button type="button" class="btn btn-success btn-sm" id="friend_button_<?php echo $user->ID ?>"><i class="fa fa-check" aria-hidden="true"></i> <?php echo lang("ctn_493") ?></button>
<?php else : ?>
<?php if($request_flag) : ?>
<button type="button" class="btn btn-success btn-sm disabled" id="friend_button_<?php echo $user->ID ?>"><?php echo lang("ctn_601") ?></button>
<?php else : ?> 
  <?php if(!$user->allow_friends) : ?>
  <button type="button" class="btn btn-success btn-sm" onclick="add_friend(<?php echo $user->ID ?>)" id="friend_button_<?php echo $user->ID ?>"><?php echo lang("ctn_602") ?></button>
  <?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><i class="fa fa-flag" aria-hidden="true"></i></button>

                      </ul>
                      


                      <ul class="user-fw-status">
                    



                            <li><a href="<?php echo site_url("profile/" . $user->username) ?>" title=""><?php echo lang("ctn_200") ?></a></li>
                            <li><a href="<?php echo site_url("profile/friends/" . $user->ID) ?>" title=""><?php echo lang("ctn_493") ?></a></li>
                            <li><a href="<?php echo site_url("profile/albums/" . $user->ID) ?>" title=""><?php echo lang("ctn_483") ?></a></li>
        
        
                    </ul>
                    </div><!--user_pro_status end-->
                    



                  </div><!--user_profile end-->
                  









                </div><!--main-left-sidebar end-->
              </div>



















              
              <div class="col-lg-9">
                <div class="main-ws-sec widget">
                  <div class="user-tab-sec container">
                    <h3><?php echo $user->first_name ?> <?php echo $user->last_name ?></h3>
                    <div class="star-descp">
                      
                      <a href="#" title=""><?php if($user->online_timestamp > time() - (60*15)) : ?>
        <span class="profile-online text-success"><?php echo lang("ctn_139") ?></span>
      <?php else : ?>
        <span class="profile-offline text-secondary"><?php echo lang("ctn_335") ?></span>
      <?php endif; ?></a>
                    </div><!--star-descp end-->
                    <div class="st2">
                      <ul class="job-dt">
                            <!-- <li data-tab="feed-dd" class="active"><a href="#" title="">Posts</a></li> -->
                            <li><a href="<?php echo site_url("profile/" . $user->username) ?>" title=""><?php echo lang("ctn_200") ?></a></li>
                            <li><a href="<?php echo site_url("profile/friends/" . $user->ID) ?>" title=""><?php echo lang("ctn_493") ?></a></li>
                            <li><a href="<?php echo site_url("profile/albums/" . $user->ID) ?>" title=""><?php echo lang("ctn_483") ?></a></li>
                            
                          </ul>

                      
                    </div><!-- tab-feed end-->
                  </div><!--user-tab-sec end-->
                  <div class="product-feed-tab current container" id="feed-dd">

                    <div class="row">
  
                    <div class="col-md-4 imagecontainer">
                      <div class="thumbnail ">
                       <a href="<?php echo site_url("profile/view_images/" . $user->ID) ?>">
                        <img src="<?php echo base_url();?>images/picture.png" class="image">
                      </a>
                      <a href="<?php echo site_url("profile/view_images/" . $user->ID) ?>">
                        <div class=" middle">
                           
                            <div class="text text-white">Pictures</div>
                         
                        </div>
                         </a>
                        
                      </div>

                    </div>
                    <div class="col-md-4 imagecontainer">
                      <div class="thumbnail ">
                        <a href="<?php echo site_url("profile/view_videos/" . $user->ID) ?>">
                          <img src="<?php echo base_url();?>images/video.png" class="image">
                        </a>
                         <a href="<?php echo site_url("profile/view_videos/" . $user->ID) ?>">
                        <div class=" middle">
                          <div class="text text-white">Videos</div>
                        </div>
                      </a>
                        
                      </div>
                    </div>
                    <div class="col-md-4 imagecontainer">
                      <div class="thumbnail ">
                        <a href="<?php echo site_url("profile/view_audios/" . $user->ID) ?>">
                        <img src="<?php echo base_url();?>images/music.png" class="image">
                      </a>
                       <a href="<?php echo site_url("profile/view_audios/" . $user->ID) ?>">
                        <div class=" middle">
                          <div class="text text-white">Audios</div>
                        </div>
                      </a>
                        
                      </div>
                    </div>

                    </div>
                      

                    


<!--    <h4 class="page-header-title"> <span class="glyphicon glyphicon-file"></span>List of Albums</h4>
         <br>

          <div class="db-header clearfix">
            
            <div class="db-header-extra form-inline"> 

                          <div class="form-group has-feedback no-margin">
<div class="input-group">
<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="fa fa-search" aria-hidden="true"></span></button>
        <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="fa fa-check" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
          <li><a href="#" onclick="change_search(1)"><span class="fa fa-check nodisplay" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
          <li><a href="#" onclick="change_search(2)"><span class="fa fa-check nodisplay" id="name-exact"></span> <?php echo lang("ctn_81") ?></a></li>
        </ul>
      </div>
</div>
</div>

<?php if($user->ID == $this->user->info->ID) : ?>
<input type="button" class="btn btn-primary btn-sm" value="<?php echo lang("ctn_555") ?>" data-toggle="modal" data-target="#addModal">
<?php endif; ?>
        </div>
        </div>
        
        <table id="album-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header"><td><?php echo lang("ctn_556") ?></td><td><?php echo lang("ctn_81") ?></td><td><?php echo lang("ctn_557") ?></td><td><?php echo lang("ctn_558") ?></td><td><?php echo lang("ctn_52") ?></td></tr>
</thead>
<tbody>
</tbody>
</table> -->






                  </div><!--product-feed-tab end-->
                  
                 
                  
                  
                 
                </div><!--main-ws-sec end-->
              </div>
              
            </div>
          </div><!-- main-section-data end-->
        </div> 
      </div>
    </main>













  <?php echo form_open(site_url("profile/add_album/0")) ?>
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-picture"></span> <?php echo lang("ctn_555") ?></h4>
      </div>
      <div class="modal-body ui-front form-horizontal">
          <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_81") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_271") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="description">
                    </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_559") ?>">
      </div>
    </div>
  </div>
</div>
<?php echo form_close() ?>

 <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="edit-album">
     
    </div>
  </div>
</div>
 <script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#album-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        { "orderable" : false },
        null,
        null,
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("profile/albums_page/" . $user->ID) ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
      "search-like", 
      "search-exact",
      "name-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>