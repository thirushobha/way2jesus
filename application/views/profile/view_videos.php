
    <section class="companies-info">
      <div class="container">
        <div class="company-title">
          <h3>All Videos<a href="<?php echo base_url();?>profile/albums/<?php echo $this->uri->segment(3);?>" class="pull-right"><i class="fa fa-hand-o-left"></i>Back To Album</a></h3>
        </div><!--company-title end-->
        <div class="companies-list">
          <div class="row">





            <?php foreach($images as $r) : ?>

   <?php
                if(empty($r->slug)) {
        $slug = $r->ID;
      } else {
        $slug = $r->slug;
      }
                ?>
                  

<div class="col-lg-3 col-md-4 col-sm-6">
              <div class="company_profile_info">
                <div class="">
                 
   
  
                  

                  
                  <!-- video start -->

                
                  <?php if(!empty($r->file_name)) : ?>
                     <video width="100%" controls>
                      <?php if($r->extension == ".mp4") : ?>
                        <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>" type="video/mp4">
                      <?php elseif($r->extension == ".ogg" || $r->extension == ".ogv") : ?>
                          <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>" type="video/ogg">
                      <?php elseif($r->extension == ".webm") : ?>
                          <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->file_name ?>" type="video/webm">
                      <?php endif; ?>
                      <?php echo lang("ctn_501") ?>
                     </video> 
                  <?php elseif(!empty($r->youtube_id)) : ?>
                  <p><iframe width="100%"  src="https://www.youtube.com/embed/<?php echo $r->youtube_id ?>" frameborder="0" allowfullscreen></iframe></p>
                  <?php endif; ?>
                

                <!-- video end -->
                 
                  
                  <!-- <ul>
                    <li><a href="#" title="" class="follow">Follow</a></li>
                    <li><a href="#" title="" class="message-us"><i class="fa fa-envelope"></i></a></li>
                  </ul> -->
                </div>
                
               
              </div><!--company_profile_info end-->
            </div>





<?php endforeach; ?>


            


            
           


          </div>
        </div><!--companies-list end-->
        <div class="process-comm">
          <?php echo $pagination; ?>
        </div>
      </div>
    </section><!--companies-info end-->