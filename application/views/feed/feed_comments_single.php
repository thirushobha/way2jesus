<?php if($post->comments > 5 && (isset($hide_prev) && !$hide_prev))  : ?>
<a href="javascript: void(0)" onclick="load_previous_comments(<?php echo $post->ID ?>, <?php echo $page+5 ?>, this)" class="small-text"><?php echo lang("ctn_514") ?></a>
<?php endif; ?>
<?php foreach($com as $r) : ?>
  <?php
$r->comment = $this->common->replace_user_tags($r->comment);
?>


                              <li>
                              <div class="comment-list" id="feed-comment-area-<?php echo $r->ID ?>">
                                <div class="bg-img">
                                  <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>" alt="" width="30px" height="30px">
                                </div>
                                <div class="comment">
                                  <h3><?php echo $r->first_name ?> <?php echo $r->last_name ?></h3>
                                  <span><i class="fa fa-clock-o"></i> <?php echo $this->common->get_time_string_simple($this->common->convert_simple_time($r->timestamp)) ?></span>
                                  <p><?php echo $r->comment ?></p>
                                  <!-- <a href="#" title="" class="active"><i class="fa fa-reply-all"></i>Reply</a> -->




                                  <div class="row">
                                    <div class="col-md-4" >
    <a href="javascript: void(0)" class="<?php if($r->commentlikeid) : ?>active-comment-like<?php endif; ?>" onclick="like_comment(<?php echo $r->ID ?>)" id="comment-like-link-<?php echo $r->ID ?>" style="display: contents;"><?php echo lang("ctn_337") ?> </a>
    <span class="" id="comment-like-<?php echo $r->ID ?>" style="display: inline-flex;"><?php if($r->likes > 0) : ?>- <span class="fa fa-thumbs-up" id=""></span> <?php echo $r->likes ?><?php endif; ?></span>
    </div>
      <div class="col-md-4">
      <a style="display: contents;" href="javascript: void(0)" onclick="load_comment_replies(<?php echo $r->ID ?>)"><?php echo lang("ctn_480") ?> </a>
      <span  style="display: inline-flex;" id="feed-reply-comments-<?php echo $r->ID ?>"><?php if($r->replies > 0) : ?>(<?php echo $r->replies ?>)<?php endif ?></span>

</div>
<div class="col-md-4">
      

      <?php if($r->userid == $this->user->info->ID || ($this->common->has_permissions(array("admin", "post_admin"), $this->user)) ) : ?>- [<a href="javascript: void(0)" onclick="delete_comment(<?php echo $r->ID ?>)">X</a>]<?php endif; ?>
</div>

     </div>



     

     <div id="feed-comment-reply-<?php echo $r->ID ?>" class="feed-comment-reply">
   <?php //echo lang("ctn_515") ?>
   </div>


                                </div>
                              </div><!--comment-list end-->
                              
                            </li>

<?php endforeach; ?>