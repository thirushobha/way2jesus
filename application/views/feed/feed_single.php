<?php
$r->content = $this->common->replace_user_tags($r->content);
$r->content = $this->common->replace_hashtags($r->content);
$r->content = $this->common->convert_smiles($r->content);
$script = '';

if($r->post_as == "page") {
	$r->avatar = $r->page_avatar;
	$r->first_name = $r->page_name;
	$r->last_name = "";
	if(!empty($r->page_slug)) {
		$slug = $r->page_slug;
	} else {
		$slug = $r->pageid;
	}
	$url = site_url("pages/view/" . $slug);
} else {
	$url = site_url("profile/" . $r->username);
}
?>









                      <div class="post-bar " id="feed-post-<?php echo $r->ID ?>">
                        <div class="post_topbar">
                          <div class="usy-dt">
                            <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>" alt="" style="width: 50px;height: 50px;">
                            <!-- <div class="usy-name">
                              <h3>John Doe</h3>
                              <span><img src="<?php echo base_url();?>assets/images/clock.png" alt="">3 min ago</span>
                            </div> -->


							<div class="usy-name">
							<?php if(isset($r->p_username)) : ?> 
							<?php // Posting to someone's profile ?>
							<h3><a href="<?php echo site_url("profile/" . $r->p_username) ?>"><?php echo $r->p_first_name ?> <?php echo $r->p_last_name ?></a> <i class="fa fa-arrow-right" aria-hidden="true"></i> <a href="<?php echo site_url("profile/" . $r->username) ?>"><?php echo $r->first_name ?> <?php echo $r->last_name ?></a></h3>

							<span><img src="<?php echo base_url();?>assets/images/clock.png" alt=""><?php echo $this->common->get_time_string_simple($this->common->convert_simple_time($r->timestamp)) ?> <?php if($r->location) : ?>- <?php echo lang("ctn_516") ?> <a href="https://www.google.com/maps/place/<?php echo urlencode($r->location) ?>"><span class="glyphicon glyphicon-map-marker"></span> <?php echo $r->location ?></a><?php endif; ?> 

								<?php if($r->user_flag) : ?> - <?php echo lang("ctn_517") ?> 
							<?php $users = $this->feed_model->get_feed_users($r->ID); ?>
							<?php $c = $users->num_rows(); $v=0; ?>

							<?php foreach($users->result() as $user) : ?>
								<?php $v++; ?>
							<a href="<?php echo site_url("profile/" . $user->username) ?>"><?php echo $user->first_name ?> <?php echo $user->last_name ?></a><?php if($v == ($c-1) && $c > 0) : ?> <?php echo lang("ctn_302") ?><?php elseif($c == $v) : ?><?php else : ?>, <?php endif; ?>
							<?php endforeach; ?>

							<?php endif; ?></span>

							<?php else : ?>
								<?php // User is posting on a page ?>
								<?php if(isset($r->page_name) && $r->post_as != "page") : ?>
									<h3><a href="<?php echo $url ?>"><?php echo $r->first_name ?> <?php echo $r->last_name ?></a> <i class="fa fa-arrow-right" aria-hidden="true"></i> <a href="<?php echo site_url("pages/view/" . $r->pageid) ?>"><?php echo $r->page_name ?></a></h3>

									<span><img src="<?php echo base_url();?>assets/images/clock.png" alt=""><?php echo $this->common->get_time_string_simple($this->common->convert_simple_time($r->timestamp)) ?>
										<?php if($r->location) : ?>- <?php echo lang("ctn_516") ?> <a href="https://www.google.com/maps/place/<?php echo urlencode($r->location) ?>"><span class="glyphicon glyphicon-map-marker"></span> <?php echo $r->location ?></a><?php endif; ?> 
										<?php if($r->user_flag) : ?> <?php echo lang("ctn_517") ?> 
										<?php $users = $this->feed_model->get_feed_users($r->ID); ?>
										<?php $c = $users->num_rows(); $v=0; ?>
										<?php foreach($users->result() as $user) : ?>
											<?php $v++; ?>
										<a href="<?php echo site_url("profile/" . $user->username) ?>"><?php echo $user->first_name ?> <?php echo $user->last_name ?></a><?php if($v == ($c-1) && $c > 0) : ?> <?php echo lang("ctn_302") ?><?php elseif($c == $v) : ?><?php else : ?>, <?php endif; ?>
										<?php endforeach; ?>
										<?php endif; ?>
									    </span>
								<?php else : ?>
									<?php // Normal post ?>
									<h3><a href="<?php echo $url ?>"><?php echo $r->first_name ?> <?php echo $r->last_name ?></a>
										<?php if($r->user_flag) : ?> <?php echo lang("ctn_517") ?> 
										<?php $users = $this->feed_model->get_feed_users($r->ID); ?>
										<?php $c = $users->num_rows(); $v=0; ?>
										<?php foreach($users->result() as $user) : ?>
											<?php $v++; ?>
										<a href="<?php echo site_url("profile/" . $user->username) ?>"><?php echo $user->first_name ?> <?php echo $user->last_name ?></a><?php if($v == ($c-1) && $c > 0) : ?> <?php echo lang("ctn_302") ?><?php elseif($c == $v) : ?><?php else : ?>, <?php endif; ?>
										<?php endforeach; ?>
										<?php endif; ?></h3>
										<span><img src="<?php echo base_url();?>assets/images/clock.png" alt=""><?php echo $this->common->get_time_string_simple($this->common->convert_simple_time($r->timestamp)) ?> <?php if($r->location) : ?>- <?php echo lang("ctn_516") ?> <a href="https://www.google.com/maps/place/<?php echo urlencode($r->location) ?>"><span class="glyphicon glyphicon-map-marker"></span> <?php echo $r->location ?></a><?php endif; ?> </span>
								<?php endif; ?>


							<?php endif; ?>
							</div>
                          </div>
                          <div class="ed-opts">
                            <a href="#" title="" class="ed-opts-open"><i class="la la-ellipsis-v"></i></a>
                            <ul class="ed-options">
                            	<li><a href="javascript:void(0)" onclick="save_post(<?php echo $r->ID ?>)" id="save_post_<?php echo $r->ID ?>"><?php if(!isset($r->savepostid)) : ?><?php echo lang("ctn_518") ?></a><?php else : ?><?php echo lang("ctn_519") ?><?php endif; ?></li>
							  	<li><a href="javascript:void(0)" onclick="subscribe_post(<?php echo $r->ID ?>)" id="subscribe_post_<?php echo $r->ID ?>"><?php if(!isset($r->subid)) : ?><?php echo lang("ctn_520") ?></a><?php else : ?><?php echo lang("ctn_521") ?><?php endif; ?></li>
							  	<?php if($r->userid == $this->user->info->ID || ($r->pageid > 0 && $r->post_as == "page" && isset($r->roleid) && $r->roleid == 1) || $this->common->has_permissions(array("admin", "post_admin"), $this->user)) : ?>
							    <li><a href="javascript:void(0)" onclick="delete_post(<?php echo $r->ID ?>)"><?php echo lang("ctn_522") ?></a></li>
							    <li><a href="javascript:void(0);" onclick="edit_post(<?php echo $r->ID ?>)"><?php echo lang("ctn_55") ?></a></li>
							    <?php endif; ?>
    
                             <!--  <li><a href="#" title="">Edit Post</a></li>
                              <li><a href="#" title="">Unsaved</a></li>
                              <li><a href="#" title="">Unbid</a></li>
                              <li><a href="#" title="">Close</a></li>
                              <li><a href="#" title="">Hide</a></li> -->
                            </ul>
                          </div>
                        </div>
                       
                        <div class="job_descp">
                          

                          <div class="mb-2"><?php echo $r->content ?></div>

							<?php if($r->site_flag) : ?>
								<?php $sites = $this->feed_model->get_feed_urls($r->ID); ?>
								<?php foreach($sites->result() as $site) : ?>
									<div class="feed-url-spot clearfix">
									<div class="pull-left feed-url-spot-image">
										<?php if($site->image) : ?>
										<img src="<?php echo $site->image ?>" width="100%">
										<?php endif; ?>
									</div>
									<p><a href="<?php echo $site->url ?>"><?php echo $site->title ?></a></p>
									<p><?php echo $site->description ?></p>
								</div>
								<?php endforeach; ?>
							<?php endif; ?>
								
							<?php if($r->template == "album") : ?>
								<?php
								// Display all images in post
								$images = $this->feed_model->get_feed_images($r->ID);
								$script .= '$(".album-images-'.$r->ID.'").viewer();';
								?>
								<div>
							  	<ul class="album-images album-images-<?php echo $r->ID ?>">
							  	<?php foreach($images->result() as $rr) : ?>
							  		<?php if(isset($rr->albumid)) : ?>
							  			<?php $r->albumid = $rr->albumid; $r->album_name = $rr->album_name; ?>
							  		<?php endif; ?>
								<li class="album-image">
								<?php if(isset($rr->file_name)) : ?>
								    <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $rr->file_name ?>" width="140" alt="<?php echo $rr->name . "<br>" . $rr->description ?>">
								  <?php else : ?>
								    <img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/default_album.png" width="140" alt="<?php echo $rr->name . "<br>" . $rr->description ?>">
								  <?php endif; ?>
								  <p><?php echo $rr->name ?></p>
								</li>
								<?php endforeach; ?>
							  </ul>
							  <?php if(isset($r->albumid)) : ?>
							  	<?php if($r->pageid > 0) {
							  		$url = site_url("pages/view_album/" . $r->albumid); 
							  	} else {
							  		$url = site_url("profile/view_album/" . $r->albumid);
							  	}
							  	?>
										<p class="small-text"><i><?php echo lang("ctn_523") ?>: <a href="<?php echo $url ?>"><?php echo $r->album_name ?></a></i></p>
									<?php endif; ?>
								</div>
							<?php elseif($r->template == "event") : ?>
								<div class="editor-event">
									<!-- <span class="fa fa-calendar"></span> -->
									
									<p><a href="<?php echo site_url("pages/view_event/" . $r->eventid) ?>"><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/Event-pic.jpg" width="100%"></a></p>
									<p><strong><a href="<?php echo site_url("pages/view_event/" . $r->eventid) ?>"><?php echo $r->event_title ?></a></strong></p>
									<p><?php echo $r->event_description ?></p>
									 <p><span class="glyphicon glyphicon-time"></span> <?php echo $r->event_start ?> ~ <?php echo $r->event_end ?> </p>
								</div>
							<?php else : ?>
								<?php if(isset($r->imageid)) : ?>
									<?php if(!empty($r->image_file_name)) : ?>
									<p><img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->image_file_name ?>" width="100%"></p>
									<?php else : ?>
									<p><img src="<?php echo $r->image_file_url ?>" width="100%"></p>
									<?php endif; ?>
									<?php if(isset($r->albumid)) : ?>
										<?php if($r->pageid > 0) {
							  		$url = site_url("pages/view_album/" . $r->albumid); 
							  	} else {
							  		$url = site_url("profile/view_album/" . $r->albumid);
							  	}
							  	?>
										<p class="small-text"><i><?php echo lang("ctn_523") ?>: <a href="<?php echo $url ?>"><?php echo $r->album_name ?></a></i></p>
									<?php endif; ?>
								<?php endif; ?>


								<!-- video start -->

								<?php if(isset($r->videoid)) : ?>
									<?php if(!empty($r->video_file_name)) : ?>
										 <video width="100%" controls>
										 	<?php if($r->video_extension == ".mp4") : ?>
											  <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->video_file_name ?>" type="video/mp4">
											<?php elseif($r->video_extension == ".ogg" || $r->video_extension == ".ogv") : ?>
										      <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->video_file_name ?>" type="video/ogg">
											<?php elseif($r->video_extension == ".webm") : ?>
										      <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->video_file_name ?>" type="video/webm">
											<?php endif; ?>
											<?php echo lang("ctn_501") ?>
										 </video> 
									<?php elseif(!empty($r->youtube_id)) : ?>
									<p><iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo $r->youtube_id ?>" frameborder="0" allowfullscreen></iframe></p>
									<?php endif; ?>
								<?php endif; ?>

								<!-- video end -->


								<!-- audio start -->
								<?php if(isset($r->audioid)) : ?>
									<?php if(!empty($r->audio_file_name)) : ?>
										 <audio width="100%" controls>
										 	<?php if($r->audio_extension == ".mp3") : ?>
											  <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->audio_file_name ?>" type="audio/mpeg">
											<?php elseif($r->audio_extension == ".ogg") : ?>
										      <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->audio_file_name ?>" type="audio/ogg">
											<?php elseif($r->audio_extension == ".wav") : ?>
										      <source src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->audio_file_name ?>" type="audio/wav">
											<?php endif; ?>
											<?php echo lang("ctn_501") ?>
										 </audio> 
									
									<?php endif; ?>
								<?php endif; ?>



								<!-- audio end -->







							<?php endif; ?>


                        </div>
                        <div class="job-status-bar">
                          <ul class="like-com">
                            <li>
                            	

                              <a  id="like-button-<?php echo $r->ID ?>"  onclick="like_feed_post(<?php echo $r->ID ?>)" class="<?php if(isset($r->likeid)) : ?>active-like<?php endif; ?>"><i class="la la-heart" id="like-button-like-<?php echo $r->ID ?>"></i> Like</a>
                              
                              
                               <!-- <span id="feed-likes-<?php echo $r->ID ?>"> <?php echo $r->likes ?></span> -->
                               <a href="#" onclick="get_post_likes(<?php echo $r->ID ?>)" class="feed-stat <?php if($r->likes <= 0) : ?>nodisplay<?php endif; ?>" id="likes-click-<?php echo $r->ID ?>">
                               	<!-- <img src="<?php echo base_url();?>assets/images/liked-img.png" alt=""> -->
                               	<span class="fa fa-thumbs-up"></span>
                               	 <span id="feed-likes-<?php echo $r->ID ?>" > <?php echo $r->likes ?></span>
                               	</a>
                          
                            </li> 
                            <li>
                            	

                            	<a    class="editor-button faded-icon" onclick="load_comments(<?php echo $r->ID ?>)" style="cursor: pointer;"><i class="la la-comment" ></i> Comment</a>

								<a href="javascript:void(0)" onclick="load_comments(<?php echo $r->ID ?>)" class="<?php if($r->comments <= 0) : ?>nodisplay<?php endif; ?>">
										<!-- <img src="<?php echo base_url();?>assets/images/liked-img.png" alt=""> -->
										<span class="fa fa-comment"></span>
										<!-- <span class="glyphicon glyphicon-comment"></span> -->
										 <span id="feed-comments-<?php echo $r->ID ?>" > <?php echo $r->comments ?></span></a>
                            </li>


                            <li> 

                            	<!-- <a href="#" class="dropdown-menu"><i class="fa fa-share"></i> Share </a> -->
                            	<div class="dropdown">
  <a   id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   <i class="fa fa-share"></i> Share 
  </a>
  <!-- http://www.facebook.com/sharer.php?u=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?> -->
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href=" http://www.facebook.com/sharer.php?u=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-facebook-f"></i> Facebook</a>

    <a class="dropdown-item" href="http://twitter.com/share?text=Way 2 Jesus&amp;url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>

    <a class="dropdown-item" href="https://plus.google.com/share?url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-google-plus-official"></i>Google +</a>

     <a class="dropdown-item" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-linkedin"></i>Linkedin</a>
      <a class="dropdown-item" href="http://pinterest.com/pin/create/button/?url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>&media={<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $r->avatar ?>}" target="_blank"><i class="fa fa-pinterest-p"></i>Pinterest</a>

       <a class="dropdown-item" href="http://www.stumbleupon.com/badge/?url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-stumbleupon"></i>Stumbleupon</a>

        <a class="dropdown-item" href="http://www.reddit.com/submit?url=<?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-reddit"></i>Reddit</a>
         <a class="dropdown-item" href="mailto:?subject=Way 2 Jesus&amp;body=Check out this site <?php echo base_url();?>home/index/3?postid=<?php echo $r->ID;?>" target="_blank"><i class="fa fa-envelope"></i>Email</a>

         <!-- http://localhost/socialnetwork/home/index/3?postid=85# -->

  </div>
</div>
	
                            </li>
                          </ul>
                          <!-- <a><i class="la la-eye"></i>Views 50</a> -->
                        </div>



<!-- <div class="feed-wrapper" id="feed-post-<?php echo $r->ID ?>">




<div class="feed-footer">


</div>
<div class="feed-comment-area" id="feed-comment-<?php echo $r->ID ?>">

</div>
</div> -->


<div class="comment-section nodisplay" id="feed-comment-<?php echo $r->ID ?>" >
												
											</div>
											

                      </div><!--post-bar end-->

                      
                      
          


















<?php if(!empty($script)) : ?>
<script type="text/javascript">
			$(document).ready(function() {
				<?php echo $script ?>
			});
</script>
<?php endif; ?>